# MULTIBAR_Analysis - Multibar project - Kohl Lab

## 1. Description
This is the code for analyzing the Multibar data from the Kohl lab 
at Oxford University collected by Dr. Mariangela Panniello.

## 2. Installation
The code itself can be obtained by cloning the [Bitbucket MULTIBAR repo](https://bitbucket.org/kohl-lab/multibar_analysis/src/master/).

However, to run the code, you should install [Anaconda](https://www.anaconda.com/) or [Miniconda](https://conda.io/miniconda.html), as well as [pip](https://pip.pypa.io/en/stable/).

Once these are installed, you can use the appropriate `.yml` 
file to create a conda environment. For example, if using Ubuntu or Mac OS, open a terminal, go to the repo directory, and enter:

`conda env create -f mult3.yml`

Conda should then handle installing most of the necessary dependencies for you.

The code **also requires** downloading and compiling [OASIS](https://github.com/j-friedrich/OASIS) within the environment.

The code is written in `Python 3`. 

## 3. Use
Once installed, you simply activate the environment:

`source activate mult3`

All of the appropriate libraries should then be loaded, and the modules can be imported for use in ipython, python scripts, or jupyter notebooks, for example.

**Scripts and modules**:

* `run_roi_analysis.py`: run and plot specific analyses on ROI data
* **`analysis`**: Sessarea object as well as session/area data analysis module
* **`mult_util`**: multibar specific utilities module
* **`plot_fcts`**: module with functions to plot analysis results from saved dictionaries or dataframes
* **`examples`**: example notebook for using SessArea/Plane objects
* **`reference_code`**: analysis code from Mariangela and Friedemann used for this project, but not integrated into this codebase

## 4. Authors
This code was written by:

* Colleen Gillon (colleen.gillon@mail.utoronto.ca)
* Mariangela Panniello (mariangela.panniello@iit.it)
* Friedemann Zenke (friedemann.zenke@fmi.ch)
