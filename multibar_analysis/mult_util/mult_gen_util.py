"""
mult_gen_util.py

This module contains general functions for navigating data from 
the MULTIBAR project (Kohl lab, Dr. Mariangela Panniello).

Authors: Colleen Gillon

Date: September, 2019

Note: this code uses python 3.7.

"""

import os

import numpy as np
import pandas as pd

from analysis import sessarea
from util import gen_util, file_util


DEPTH_DICT = {'layers' : ['L2', 'L3', 'L3', 'L3', 'L4'],
              'levels' : ['mid', 'hypershallow', 'shallow', 'deep', 'mid'],
              'base_vals': [130, 190, 270, 320, 400]
              }


#############################################
def depth_from_lay_lev(layer='any', level='any'):
    """
    depth_from_lay_lev()

    Returns theoretical depths corresponding to specified layer(s) and level(s).
    Raises an error if none correspond.

    Optional args:
        - layer (str or list): layer(s) of interest ('any' for all)
                               default: 'any'
        - level (str or list): level(s) of interest ('any' for all)
                               default: 'any'
    
    Returns:
        - depth (list): ordered list of depths
    """

    keys = ['layers', 'levels']
    vals = [layer, level]
    
    sets = []
    for key, val in zip(keys, vals):
        if val in ['any', 'all']:
            val = list(set(DEPTH_DICT[key]))
        else:
            val = gen_util.list_if_not(val)
        idx = [i for i, key in enumerate(DEPTH_DICT[key]) if key in val]
        sets.append(set(idx))
    
    idx = list(sets[0].intersection(sets[1]))
    depth = list(set([DEPTH_DICT['base_vals'][i] for i in idx]))

    if len(depth) == 0:
        raise ValueError('No depth values correspond to criteria.')

    return depth


#############################################
def lay_lev_from_depth(depth='any'):
    """
    lay_lev_from_depth()

    Returns layers and levels corresponding to specified depth(s).
    Raises an error if none correspond.

    Optional args:
        - depth (str or int or list): depth(s) of interest ('any' for all)
                                      default: 'any' 
    
    Returns:
        - layer (list): ordered list of layer(s)
        - level (list): ordered list of level(s)
    """

    if depth in ['any', 'all']:
        depth = list(set(DEPTH_DICT['base_vals']))
    else:
        depth = gen_util.list_if_not(depth)

    idx = [i for i, key in enumerate(DEPTH_DICT['base_vals']) if key in depth]

    layer = list(set([DEPTH_DICT['layers'][i] for i in idx]))
    level = list(set([DEPTH_DICT['levels'][i] for i in idx]))

    if len(layer) == 0:
        raise ValueError('No depth values correspond to criteria.')

    return layer, level


#############################################
def depth_vals(exact_depth):
    """
    depth_vals(exact_depth)

    Returns theoretical depth, layer and level closest to the specified 
    exact depth.
    
    Required args:
        - exact_depth (int): exact depth of interest 
    
    Returns:
        - depth (int): corresponding theoretical depth
        - layer (str): corresponding layer
        - level (str): corresponding level
    """

    if np.isnan(exact_depth):
        depth = np.nan
        layer = np.nan
        level = np.nan
    else:
        layers    = DEPTH_DICT['layers']
        levels    = DEPTH_DICT['levels']
        base_vals = np.asarray(DEPTH_DICT['base_vals'])
        
        close_idx = np.argmin(np.absolute(base_vals - exact_depth))
        depth     = base_vals[close_idx]
        layer     = layers[close_idx]
        level     = levels[close_idx]
    
    return depth, layer, level


#############################################
def add_depth_info(df, savename='mouse_df', fulldir='', overwrite=False):
    """
    add_depth_info(df)

    Adds theoretical depth, layer and level to the dataframe for each actual
    depth and saves the file.
    
    Required args:
        - df (pd DataFrame or str): dataframe or path to dataframe. Expects the
                                    following column, 'plane_x' for each 
                                    plane x, containing the actual depth of
                                    recording

    Optional args:
        - savename (str)  : name under which to save df as csv, can include the 
                            whole directory name and extension
                            default: 'mouse_df'
        - fulldir (str)   : directory in which to save df
                            default: ''
        - overwrite (bool): if False, file name is modified if needed to prevent 
                            overwriting    
    """
 
    if isinstance(df, str):
        df = file_util.loadfile(df)

    plane_ns = [col.replace('plane_', '') for col in df.columns 
                                             if 'plane' in col]
    if len(plane_ns) == 0:
        raise ValueError('No plane columns found.')
    
    for n in plane_ns:
        for row in range(len(df)):
            act_depth = df.loc[row, 'plane_{}'.format(n)]
            depth, layer, level = depth_vals(act_depth)
            df.loc[row, 'depth_{}'.format(n)] = depth
            df.loc[row, 'layer_{}'.format(n)] = layer
            df.loc[row, 'level_{}'.format(n)] = level
    
    file_util.saveinfo(df, savename, fulldir, save_as='csv', 
                       overwrite=overwrite)


#############################################
def get_go_nogo(task='pd_2', pos='go'):
    """
    get_go_nogo()

    Returns requested value.

    Optional args:
        - task (str): task
                      default: 'pd_2'
        - pos (str) : requested position
                      default: 'go'

    Return:
        - val (int): position value
    """

    if task == 'pd_2':
        if pos == 'go':
            val = 70
        elif pos == 'nogo':
            val = 49
        else:
            gen_util.accepted_values_error('pos', pos, ['go', 'nogo'])
    elif task == 'switch':
        if pos == 'go':
            val = 49
        elif pos == 'nogo':
            val = 70
        else:
            gen_util.accepted_values_error('pos', pos, ['go', 'nogo'])
    elif task == 'sens_stim':
        val = None
    else:
        gen_util.accepted_values_error(
            'task', task, ['sens_stim', 'pd_2', 'switch'])

    return val


#############################################
def get_outcomes(outc='all'):
    """
    get_outcomes()

    Returns the requested outcome split.

    Optional args:
        - outc (str): outcome split, i.e. 
                         'all'       : all outcomes separate
                         'corr_wrong': outcomes grouped by correct v wrong
                         'go'        : only go-related outcomes included
                         'nogo'      : only nogo-related outcomes included
                         'corr'      : only correct outcomes included
                         'wrong'     : only wrong outcomes included

    Return:
        - outcomes (list): list of outcomes, possibly by group (in the case of 
                           'corr_wrong')
    """

    if outc in ['all', 'any']:
        outcomes = ['corr_acc', 'corr_rej', 'missed', 'false_pos']
    elif outc == 'corr_wrong':
        outcomes = [['corr_acc', 'corr_rej'], ['missed', 'false_pos']]
    elif outc == 'go':
        outcomes = ['corr_acc', 'missed']
    elif outc == 'nogo':
        outcomes = ['corr_rej', 'false_pos']
    elif outc == 'corr':
        outcomes = ['corr_acc', 'corr_rej']
    elif outc == 'wrong':
        outcomes = ['missed', 'false_pos']
    else:
        gen_util.accepted_values_error(
            'outc', outc, ['all', 'corr_wrong', 'go', 'nogo'])

    return outcomes


#############################################
def get_sess_info(sessions, resp_rois=False):
    """
    get_sess_info(sessions)

    Puts information from all sessions into a dictionary.

    Required args:
        - sessions (list): ordered list of Session objects

    Returns:
        - sess_info (dict): dictionary containing information from each
                            session 
            ['mouse_ns'] (list)   : mouse numbers
            ['mouseids'] (list)   : mouse ids
            ['sess_ns'] (list)    : session numbers
            ['dates'] (list)      : dates  
            ['areas'] (list)      : areas
            ['tasks'] (list)      : tasks
            ['planes'] (list)     : planes
            ['layers'] (list)     : imaging layers
            ['levels'] (list)     : levels in imaging layers
            ['depths'] (list)     : imaging depths aimed at
            ['depths_act'] (list) : actual imaging depths 
            ['nrois'] (list)      : number of ROIs in session
            ['twop_fps'] (list)   : 2p frames per second
            ['prop_corr'] (list)  : proportion correct
            ['stim_leng'] (float) : average length of stimulus presentations
            
            and if resp_rois:
            ['n_resp_rois'] (list)     : number of responsive ROIs
            ['n_outc_resp_rois'] (list): number of outcome responsive ROIs
    """

    sess_info = dict()
    keys = ['mouse_ns', 'mouseids', 'sess_ns', 'dates', 'tasks', 'areas', 
        'planes', 'layers', 'levels', 'depths', 'depths_act', 'stim_leng', 
        'nrois', 'twop_fps', 'prop_corr']

    if resp_rois:
        keys.extend(['n_resp_rois', 'n_outc_resp_rois'])
        
    for key in keys:
        sess_info[key] = []

    sessions = gen_util.list_if_not(sessions)

    for sess in sessions:
        if sess is None:
            for key in keys:
                sess_info[key].append(None)
            continue
        plane_n = sess.plane_oi
        plane = sess.get_plane(plane_n)        
        sess_info['mouse_ns'].append(sess.mouse_n)
        sess_info['mouseids'].append(sess.mouseid)
        sess_info['sess_ns'].append(sess.sess_n)
        sess_info['dates'].append(sess.date)
        sess_info['tasks'].append(sess.task)
        sess_info['areas'].append(sess.area)
        sess_info['planes'].append(plane_n)
        sess_info['layers'].append(plane.layer)
        sess_info['levels'].append(plane.level)
        sess_info['depths'].append(plane.depth)
        sess_info['depths_act'].append(plane.depth_act)
        sess_info['stim_leng'].append(plane.stim_leng)
        sess_info['nrois'].append(plane.nrois)
        sess_info['twop_fps'].append(plane.twop_fps)
        sess_info['prop_corr'].append(sess.prop_corr)
        if resp_rois:
            sess_info['n_resp_rois'].append(plane.get_n_resp_rois())
            if sess_info['tasks'][-1] == 'sens_stim':
                sess_info['n_outc_resp_rois'].append(None)
            else:
                sess_info['n_outc_resp_rois'].append(
                    plane.get_n_resp_rois(trial_type='outcome'))
            
    return sess_info


#############################################
def limit_by_trial_outcome(df_rows, min_corr=-1, min_wrong=-1):
    """
    limit_by_trial_outcome(df_rows)

    Returns a subset of the input dataframe limited by the minimum number of
    correct/wrong trials per category 

    Required args:
        - df_rows (pd DataFrame): dataframe, with columns 'corr_rej', 
                                  'corr_acc', 'false_pos', 'missed' 

    Optional args:
        - min_corr (int or str) : min number of correct trials 
                                  (ignored if negative), optionally limited 
                                  to go/nogo trials (e.g. '1go', '5each')
                                  default: -1
        - min_wrong (int or str): min number of wrong trials, optionally 
                                  limited to go/nogo trials
                                  (ignored if negative) (e.g. '1go', '5each')
                                  default: -1
    
    Returns:
        - df_rows (pd DataFrame): dataframe corresponding to the criteria
    """

    # limit the min_corr, min_wrong criteria based if needed
    extr_strs  = ['nogo', 'go', 'each'] # keep order to check nogo first!
    corr_labs  = ['corr_rej', 'corr_acc']
    wrong_labs = ['false_pos', 'missed']
    min_strs = [min_corr, min_wrong]
    all_labs = [corr_labs, wrong_labs]
    eachs = [False, False]

    try:
        for e in range(len(extr_strs)):
            for l in range(len(min_strs)):
                if extr_strs[e] in str(min_strs[l]):
                    if extr_strs[e] == 'each':
                        eachs[l] = True
                    else:
                        all_labs[l] = [all_labs[l][e]]
                    # strip string and keep int only
                    min_strs[l] = min_strs[l].replace(extr_strs[e], '')
        min_ints = [int(min_val) for min_val in min_strs]
    except Exception as err:
        print(('`min_corr` and `min_wrong` can only include an int and '
               'either `go`, `nogo` or `each`.'))
        raise err

    # limit based on criteria
    for each, min_val, labs in zip(eachs, min_ints, all_labs):
        if min_val > 0:
            if each: # restrict rows sequentially
                for lab in labs:
                    df_rows = df_rows.loc[(df_rows[lab] >= min_val)]
            else: # restrict rows together
                row_sums = df_rows[labs[0]].astype(float) # get first value
                for lab in labs[1:]: # add subsequent
                    row_sums += df_rows[lab].astype(float)
                df_rows = df_rows.loc[(row_sums >= min_val)] # apply criterion

    return df_rows


#############################################
def get_df_vals(mouse_df, returnlab, mouse_n, sess_n, task, area, layer, 
                level, incl=1, min_rois=1, min_corr=-1, min_wrong=-1, 
                unique=False, sort=False, plane=1):
    """
    get_df_vals(mouse_df, returnlab, mouse_n, sessid, sess_n, runtype, depth)

    Returns values from mouse dataframe under a specified label that fit the 
    criteria.

    Required args:
        - mouse_df (pandas df)      : dataframe containing parameters for each 
                                      session.
        - returnlab (list or str)   : label(s) from which to return values
        - mouse_n (int, str or list): mouse id value(s) of interest  
        - sess_n (int, str or list) : session number(s) of interest
        - task (int, str, or list)  : task(s) of interest
        - area (int, str or list)   : area number(s) of interest
        - layer (str or list)       : layer value(s) of interest ('L2', 'L3')
        - level (str or list)       : level value(s) of interest 
                                      ('shallow', 'mid', 'deep')

    Optional args:
        - incl (str)            : which sessions to include (1, 0, 'any')
                                  default: 1
        - min_rois (int)        : min number of ROIs (ignored if negative)
                                  default: 1
        - min_corr (int or str) : min number of correct trials 
                                  (ignored if negative or task includes 
                                  sens_stim), optionally limited 
                                  to go/nogo trials
                                  default: -1
        - min_wrong (int or str): min number of wrong trials, optionally 
                                  limited to go/nogo trials
                                  (ignored if negative or task includes 
                                  sens_stim)
                                  default: -1
        - unique (bool)         : whether to return a list of values without 
                                  duplicates
                                  default: False 
        - sort (bool)           : whether to sort output values
                                  default: False
        - plane (int)           : plane to use for criteria
                                  default: 1
                                    
    Returns:
        - df_vals (list): list of values under the specified label that fit
                          the criteria (or lists for each label if multiple 
                          labels)
    """

    if isinstance(mouse_df, str):
        mouse_df = file_util.loadfile(mouse_df)

    # make sure all of these labels are lists
    all_labs = [mouse_n, sess_n, task, area, layer, level, incl]
    for i in range(len(all_labs)):
        all_labs[i] = gen_util.list_if_not(all_labs[i])

    [mouse_n, sess_n, task, area, layer, level, incl] = all_labs

    df_rows = mouse_df.loc[
        (mouse_df['mouse_n'].isin(mouse_n)) & 
        (mouse_df['sess_n'].isin(sess_n)) &
        (mouse_df['task'].isin(task)) &
        (mouse_df['area'].isin(area)) &
        (mouse_df['layer_{}'.format(plane)].isin(layer)) &
        (mouse_df['level_{}'.format(plane)].isin(level)) &
        (mouse_df['incl_{}'.format(plane)].isin(incl))]
    if min_rois > 0:
        df_rows = df_rows.loc[
            (df_rows['nrois_{}'.format(plane)].astype(float) >= min_rois)]
    
    if 'sens_stim' in task:
        if min_corr != -1 or min_wrong != -1:
            print(('Not including min correct and min wrong criteria as '
                'tasks include sensory stimulation.'))
    else:
        df_rows = limit_by_trial_outcome(df_rows, min_corr, min_wrong)

    returnlab = gen_util.list_if_not(returnlab)
    if len(returnlab) != 1 and (sort or unique):
        print(('WARNING: Sorted and unique will be set to False as multiple '
            'labels are requested.'))
        sort   = False
        unique = False

    df_vals = []
    for lab in returnlab:
        if lab in ['layer', 'level', 'usable', 'incl', 'nrois']:
            lab = '{}_{}'.format(lab, plane)
        if lab == 'plane':
            vals = [plane] * len(df_rows) # add the planes in
        else:
            vals = df_rows[lab].tolist()    
        if unique:
            vals = list(set(vals))
        if sort:
            vals = sorted(vals)
        df_vals.append(vals)

    df_vals = gen_util.delist_if_not(df_vals)
    
    return df_vals


#############################################
def get_sess_vals(mouse_df, returnlab, mouse_n='any', sess_n='any', task='any',
                  layer='any', level='any', incl='all', min_rois=1, 
                  min_corr=-1, min_wrong=-1, omit_mice=[], unique=False, 
                  sort=False, split_planes=True):

    """
    get_sess_vals(mouse_df, returnlab)

    Returns list of values under the specified label that fit the specified
    criteria.

    Required args:
        - mouse_df (str)         : path name of dataframe containing 
                                   information on each session
        - returnlab (str or list): label(s) from which to return values

    Optional args:
        - mouse_n (int, str or list): mouse number(s) of interest
                                      default: 'any'
        - sess_n (int, str or list) : session number(s) of interest
                                      default: 'any'
        - task (str or list)        : task(s) of interest
                                      default: 'any'
        - layer (str or list)       : layer value(s) of interest ('L2', 'L3')
                                      default: 'any'
        - level (str or list)       : level value(s) of interest 
                                      ('shallow', 'mid', 'deep')
        - incl (str)                : which sessions to include (1, 0, 'any')
                                      default: 1
        - min_rois (int)            : min number of ROIs (ignored if negative)
                                      default: 1
        - min_corr (int or str)     : min number of correct trials 
                                      (ignored if negative or task includes 
                                      sens_stim), optionally limited 
                                      to go/nogo trials
                                      default: -1
        - min_wrong (int or str)    : min number of wrong trials, optionally 
                                      limited to go/nogo trials
                                      (ignored if negative or task includes 
                                      sens_stim)
                                      default: -1
        - omit_mice (list)          : mice to omit
                                      default: []
        - unique (bool)             : whether to return a list of values 
                                      without duplicates
                                      default: False 
        - sort (bool)               : whether to sort output values
                                      default: False
        - split_planes (bool)       : if True, planes are kept separate
                                      default: True
     
    Returns:
        - sess_vals (list): values from dataframe for sessions that correspond 
                            to criteria, split by planes if split_planes
                            and split by labels if several returnlab are 
                            provided: (planes x) (labels x) vals
    """

    if isinstance(mouse_df, str):
        mouse_df = file_util.loadfile(mouse_df)

    plane_ns = [col.replace('plane_', '') for col in mouse_df.columns 
        if 'plane' in col]

    returnlab = gen_util.list_if_not(returnlab)
    if len(returnlab) != 1 and (sort or unique):
        print(('WARNING: Sorted and unique will be set to False as multiple '
            'labels are requested.'))
        sort, unique = False, False
    
    sess_vals = []
    for n in plane_ns:
        params      = [mouse_n, sess_n, task, layer, level, incl, 'any']
        param_names = ['mouse_n', 'sess_n', 'task', 'layer_{}'.format(n), 
            'level_{}'.format(n), 'incl_{}'.format(n), 'area']
    
        # for each label, collect values in a list
        for i in range(len(params)):
            params[i] = gen_util.get_df_label_vals(
                mouse_df, param_names[i], params[i])   
        [mouse_n, sess_n, task, layer, level, incl, area] = params
        # collect all mouse IDs and remove omitted mice
        mouse_n = gen_util.remove_if(mouse_n, omit_mice)

        sess_vals.append(get_df_vals(
            mouse_df, returnlab, mouse_n, sess_n, task, area=area, layer=layer, 
            level=level, incl=incl, min_rois=min_rois, min_corr=min_corr, 
            min_wrong=min_wrong, unique=unique, sort=sort, plane=n))
        
    if not split_planes:
        sess_vals_both = []
        if len(returnlab) == 1:
            sess_vals = [[vals] for vals in sess_vals]
        for i in range(len(sess_vals[0])):
            all_vals = [vals for pl_vals in sess_vals for vals in pl_vals[i]]
            if unique:
                all_vals = list(set(all_vals))
            if sort:
                all_vals = sorted(all_vals)
            sess_vals_both.append(all_vals)
        sess_vals = sess_vals_both
        sess_vals = gen_util.delist_if_not(sess_vals)
    
    return sess_vals


#############################################
def get_idx_pre_post(sess_ns, plane_ns, min_exp=3, diff_exp=3, miss=True):
    """
    get_idx_pre_post(sess_ns, plane_ns)

    Returns the indices of the 2 sessions selected pre and post learning.

    Required args:
        - sess_ns (list) : session numbers
        - plane_ns (list): corresponding plane numbers

    Optional args:
        - min_exp (int) : the largest expected number for first session
                          default: 3
        - diff_exp (int): the minimum difference in session number between the 
                          first and last session
                          default: 3
        - miss (bool)   : if True, allows for up to 50% of sessions in series
                          to be missing
                          default: True

    Returns:
        - sess_idx (list): list of session indices (None for missing sessions)
        - no_sess (bool) : if True, no sessions are returned
    """

    sess_idx = []
    no_sess = False
    if len(sess_ns) == 0:
        no_sess = True
    else:
        arg_sort = np.argsort(sess_ns)
        sort_sess_ns  = np.asarray(sess_ns)[arg_sort]
        sort_plane_ns = np.asarray(plane_ns, dtype=int)[arg_sort]
        learn_idx = np.where(np.diff(sort_sess_ns) > diff_exp)[0]
        
        if len(learn_idx) == 0:
            if not miss:
                no_sess = True
                pass
            else:
                idx = -1
                if np.min(sort_sess_ns) > min_exp:
                    idx = 0
        else:
            idx = learn_idx[0] + 1

        # favour plane 1s by penalizing plane 2s (+2 dist)
        min_seq = sort_sess_ns[: idx] # min
        if len(min_seq) == 0:
            min_idx_orig = None
        else:
            min_planes = sort_plane_ns[: idx]
            min_idx = np.argmin(min_seq + 2 * (min_planes - 1))
            min_idx_orig = arg_sort[min_idx]

        max_seq = sort_sess_ns[idx :]
        if len(max_seq) == 0: # max
            max_idx_orig = None
        else:
            max_planes = sort_plane_ns[idx :]
            max_idx  = np.argmax(max_seq - 2 * (max_planes - 1))
            max_idx_orig = arg_sort[max_idx + idx]
        sess_idx = [min_idx_orig, max_idx_orig]
    
    return sess_idx, no_sess


#############################################
def get_idx_learn(sess_ns, plane_ns, corrs, thrs, miss=True):
    """
    get_idx_learn(sess_ns, plane_ns, corrs, thrs)

    Returns the indices of the sessions selected across learning.

    Required args:
        - sess_ns (list) : session numbers
        - plane_ns (list): corresponding plane numbers
        - corrs (list)   : corresponding correct proportions
        - thrs (list)    : threshold ranges for each session

    Optional args:
        - miss (bool): if True, allows for up to 50% of sessions in series to 
                       be missing
                       default: True

    Returns:
        - sess_idx (list): list of session indices (None for missing sessions)
        - no_sess (bool) : if True, no sessions are returned
    """

    no_sess = False
    
    n = len(thrs)
    thr_sess = []
    sess_min, sess_max = float(min(sess_ns)), float(max(sess_ns) + 1)
    buff_prop = 0.1 # buffer around session numbers
    b = np.around(sess_max * buff_prop).astype(int)
    incr = sess_max/n
    
    for i in range(n):
        thr_sess.append(
            [np.max([sess_min, np.around(i*incr)-b]).astype(int),
            np.min([sess_max, np.around((i+1)*incr) + b]).astype(int)])

    lower = sess_min
    sess_idx = []
    none_count = 0
    for t, (th, th_s) in enumerate(zip(thrs, thr_sess)):
        lower = max(th_s[0], lower)
        wei = [1-t/n, t/n]
        targ = sum([s*w for s, w in zip(th_s, wei)])  # target session number
        idx  = [i for i, (s, c) in enumerate(zip(sess_ns, corrs)) 
            if (s >= lower and s < th_s[1] and c >= th[0] and c < th[1])]
        planes = [int(plane_ns[i]) for i in idx] # retain corresponding planes
        if len(idx) == 0:
            none_count += 1
            if not miss or none_count > n*0.5:
                no_sess = True
                break
            else:
                sess_idx.append(None)   
        else:
            sess_dist = [np.absolute(targ - sess_ns[i]) for i in idx]
            # favour plane 1s by penalizing plane 2s (+2 dist)
            sess_dist_pen = [sd + 2 * (p - 1) for sd, p 
                in zip(sess_dist, planes)]
            closest = np.argmin(sess_dist_pen)
            sess_idx.append(idx[closest])
            lower = sess_ns[sess_idx[-1]] + 1
    
    return sess_idx, no_sess


#############################################
def get_ordered_vals(mouse_df, returnlab, thrs, mouse_n='any', task='any',
                     layer='any', level='any', incl='all', min_rois=1, 
                     min_corr=-1, min_wrong=-1, omit_mice=[], miss=True, 
                     bylab=False, rev=False):

    """
    get_ordered_vals(mouse_df, returnlab, thrs)

    Returns list of values under the specified label that fit the specified
    criteria. If no session series fits the criteria, None is returned.

    Required args:
        - mouse_df (str)         : path name of dataframe containing 
                                   information on each session
        - returnlab (str or list): label(s) from which to return values
        - thrs (list)            : list of threshold ranges to use for 
                                   correct response rate

    Optional args:
        - mouse_n (int, str or list): mouse number(s) of interest
                                      default: 'any'
        - task (str or list)        : task(s) of interest
                                      default: 'any'
        - layer (str or list)       : layer value(s) of interest ('L2', 'L3')
                                      default: 'any'
        - level (str or list)       : level value(s) of interest 
                                      ('shallow', 'mid', 'deep')
        - incl (str)                : which sessions to include (1, 0, 'any')
                                      default: 1
        - min_rois (int)            : min number of ROIs (ignored if negative)
                                      default: 1
        - min_corr (int or str)     : min number of correct trials 
                                      (ignored if negative or task includes 
                                      sens_stim), optionally limited 
                                      to go/nogo trials
                                      default: -1
        - min_wrong (int or str)    : min number of wrong trials, optionally 
                                      limited to go/nogo trials
                                      (ignored if negative or task includes 
                                      sens_stim)
                                      default: -1     
        - omit_mice (list)          : mice to omit
                                      default: []
        - unique (bool)             : whether to return a list of values 
                                      without duplicates
                                      default: False 
        - sort (bool)               : whether to sort output values
                                      default: False
        - miss (bool)               : if True, allows for up to 50% of sessions 
                                      in series to be missing
                                      default: True
        - bylab (bool)              : if True, values are returned by label. If 
                                      False, values are returned by session
                                      default: False
        - rev (bool)                : if True, sessions are returned in reverse
                                      order (descending)
                                      default: False

    Returns:
        - sess_vals (list): values from dataframe for sessions that correspond 
                            to criteria, split by labels if several returnlab 
                            are provided: 
                                if bylab : (labels x) vals
                                otherwise: vals (x labels)
                            Values replaced with None for missing sessions.
                            None returned if no appropriatesessions are found.                             
    """

    if isinstance(mouse_df, str):
        mouse_df = file_util.loadfile(mouse_df)

    task_name = gen_util.list_if_not(task)
    if task_name == ['sens_stim']:
        if thrs is not None:
            print('Settings thresholds to None.')
            thrs = None
    elif task_name != ['pd_2']:
        raise ValueError(('Function not implemented for {} tasks '
            'together.'.format(', '.join(task))))

    returnlab = gen_util.list_if_not(returnlab)
    
    # get all possible sessions numbers for the task
    sess_ns = get_sess_vals(
        mouse_df, 'sess_n', mouse_n, task=task, omit_mice=omit_mice, 
        split_planes=False, unique=True, sort=True)
    if len(sess_ns) == 0:
        no_sess = True
    else:
        labs_need = ['mouse_n', 'sess_n', 'area', 'task', 'layer', 'level', 
            'incl', 'plane', 'prop_corr']
        sess_vals = get_sess_vals(
            mouse_df, labs_need, mouse_n, 'any', task, layer, level, incl, 
            min_rois, min_corr, min_wrong, omit_mice, split_planes=False)
        [mouse_n, sess_n, area, task, layer, level, incl, plane, corrs] = \
            sess_vals
        if task_name == ['sens_stim']: # get pre and post learning
            sess_idx, no_sess = get_idx_pre_post(sess_n, plane)
        else:
            sess_idx, no_sess = get_idx_learn(sess_n, plane, corrs, thrs, miss)

    if no_sess:
        sess_vals = None
    else:
        sess_vals = []
        for i in sess_idx:
            if i is None:
                sess_vals.append([None] * len(returnlab))
            else:
                add_vals = get_df_vals(
                    mouse_df, returnlab, mouse_n[i], sess_n[i], task[i], 
                    area[i], layer[i], level[i], incl[i], min_rois, min_corr, 
                    plane=plane[i])
                if len(add_vals[0]) != 1:
                    raise ValueError(('Check code. Exactly one matching '
                        'session should be found.'))
                add_vals = [val[0] for val in add_vals]
                sess_vals.append(add_vals)
        if rev:
            sess_vals = sess_vals[::-1]
        if bylab:
            sess_vals = [list(val) for val in zip(*sess_vals)]

        sess_vals = gen_util.delist_if_not(sess_vals)

    return sess_vals


#############################################
def choose_sess(sess_vals, plane_1=True):
    """
    choose_sess(sess_vals)

    Selects a single set of session values, typically the first.

    Required args:
        - sess_vals (nested list): list of session values, organized as 
                                   labels x sess, where the last label must be
                                   the plane number

    Optional args:
        - plane_1 (bool): if True the first plane 1 session is retained

    Returns:
        - sess_vals (list): list of values for one session
    """

    idx = 0
    n_sess = len(sess_vals[0])
    if n_sess != 1:
        planes = np.asarray(sess_vals[-1]).astype(int)
        idx_1  = np.where(planes == 1)[0]
        if plane_1 and len(idx_1) > 0:
            idx = idx_1[idx]
            print(('\nWARNING: {} session/areas/planes match. Retaining only '
                'first plane 1 match.').format(n_sess))
        else:
            print(('\nWARNING: {} session/areas/planes match. Retaining only'
                ' first match.').format(n_sess)) 

    sess_vals = [vals[idx] for vals in sess_vals]

    return sess_vals


#############################################
def sess_per_mouse(mouse_df, mouse_n='any', sess_n=1, task='any', layer='any', 
                   level='any', incl=1, min_rois=1, min_corr=-1, min_wrong=-1, 
                   omit_mice=[], closest=False, bylab=False):
    """
    sess_per_mouse(mouse_df)
    
    Returns session path (up to 1 per mouse) that fit the specified
    criteria.

    Required args:
        - mouse_df (str): path name of dataframe containing information 
                          on each session
        
    Optional args:
        - mouse_n (int or str)  : mouse number(s) of interest
                                  default: 'any'
        - sess_n (int or str)   : session number(s) of interest
                                  (1, 2, 3, ... or 'first', or 'last')
                                  default: 1
        - task (str or list)    : task(s) of interest
                                  default: 'any'
        - layer (str or list)   : layer value(s) of interest (1, 2, 3)
                                  default: 'any'
        - level (str or list)   : level value(s) of interest (1, 2, 3)
                                  default: 'any'
        - incl (str)            : which sessions to include (1, 0, 'any')
                                  default: 1
        - min_rois (int)        : min number of ROIs (ignored if negative)
                                  default: 1
        - min_corr (int or str) : min number of correct trials 
                                  (ignored if negative or task includes 
                                  sens_stim), optionally limited 
                                  to go/nogo trials
                                  default: -1
        - min_wrong (int or str): min number of wrong trials, optionally 
                                  limited to go/nogo trials
                                  (ignored if negative or task includes 
                                  sens_stim)
                                  default: -1
        - omit_mice (list)      : mice to omit
                                  default: []
        - closest (bool)        : if False, only exact session number is 
                                  retained, otherwise the closest
                                  default: False
        - bylab (bool)          : if True, values are returned by label. If 
                                  False, values are returned by session

    Returns:
        - all_sess_vals (list): values for each session to analyse 
                                (1 per mouse), 
                                if bylab, structured as 
                                    val (mouse_n, sess_n, area, plane) x session
                                otherwise, structured as
                                    session x val (mouse_n, sess_n, area, plane)
                                 
    """

    mouse_df = file_util.loadfile(mouse_df)

    orig_sess_n = int(sess_n)
    if closest or sess_n in ['last', 'first']:
        sess_n = gen_util.get_df_label_vals(mouse_df, 'sess_n', 'any')

    # get list of mice that fit the criteria
    mouse_ns = get_sess_vals(
        mouse_df, 'mouse_n', mouse_n, sess_n, task, layer, level, incl, 
        min_rois, min_corr, min_wrong, omit_mice, unique=True, sort=True, 
        split_planes=False)

    # get session ID each mouse based on criteria 
    all_sess_vals = []
    returnlabs = ['mouse_n', 'sess_n', 'area', 'plane']
    for i in sorted(mouse_ns):
        sess_ns = get_sess_vals(
            mouse_df, 'sess_n', i, sess_n, task, layer, level, incl, min_rois, 
            min_corr, min_wrong, omit_mice, unique=True, sort=True, 
            split_planes=False)
        # skip mouse if no sessions meet criteria
        if len(sess_ns) == 0:
            continue
        # if only exact sess n is accepted (not closest)
        elif str(orig_sess_n) == 'first' or not closest:
            n = sess_ns[0]
        elif str(orig_sess_n) in ['last', '-1']:
            n = sess_ns[-1]
        # find closest sess number among possible sessions
        else:
            n = sess_ns[np.argmin(np.absolute(
                [x - orig_sess_n for x in sess_ns]))]
        sess_vals = get_sess_vals(
            mouse_df, returnlabs, i, n, task, layer, level, incl, min_rois, 
            min_corr, min_wrong, omit_mice, split_planes=False)
        # choose first plane 1 session, if possible
        all_sess_vals.append(choose_sess(sess_vals, plane_1=True))
    
    if len(all_sess_vals) == 0:
        raise ValueError('No sessions meet the criteria.')
    if bylab:
        all_sess_vals = [list(val) for val in zip(*all_sess_vals)]

    return all_sess_vals


#############################################
def sess_comb_per_mouse(mouse_df, mouse_n='any', sess_n='1vlast', 
                        task='pd_2', layer='L2', level='shallow', incl=1, 
                        min_rois=1, min_corr=-1, min_wrong=-1, omit_mice=[], 
                        closest=False, bylab=False):
    """
    sess_comb_per_mouse(mouse_df)
    
    Returns list of session ID combinations (2 per mouse) that fit the 
    specified criteria.

    Required args:
        - mouse_df (str): path name of dataframe containing information 
                          on each session
        
    Optional args:
        - mouse_n (int or str)  : mouse number(s) of interest
                                  default: 'any'
        - sess_n (str)          : session numbers of interest to compare
                                  default: '1vlast'
        - task (str or list)    : task(s) of interest
                                  default: 'pd_2'
        - layer (str or list)   : layer value(s) of interest (1, 2, 3)
                                  default: 'any'
        - level (str or list)   : level value(s) of interest (1, 2, 3)
                                  default: 'any'
        - incl (str)            : which sessions to include (1, 0, 'any')
                                  default: 1
        - min_rois (int)        : min number of ROIs (ignored if negative)
                                  default: 1
        - min_corr (int or str) : min number of correct trials 
                                  (ignored if negative or task includes 
                                  sens_stim), optionally limited 
                                  to go/nogo trials
                                  default: -1
        - min_wrong (int or str): min number of wrong trials, optionally 
                                  limited to go/nogo trials
                                  (ignored if negative or task includes 
                                  sens_stim)
                                  default: -1
        - omit_mice (list)      : mice to omit
                                  default: []
        - closest (bool)        : if False, only exact session number is 
                                  retained, otherwise the closest
                                  default: False
        - bylab (bool)          : if True, values are returned by label. If 
                                  False, values are returned by session
     
    Returns:
        - all_sess_vals (list): values for each session combination to analyse 
                                (1 per mouse), structured as
                                if bylab:
                                    mouse x val (mouse_n, sess_n, area, plane)
                                          x session
                                otherwise: 
                                    mouse x session 
                                          x val (mouse_n, sess_n, area, plane) 
    """

    if closest:
        print(('WARNING: Session comparisons not implemented using the '
            '`closest` parameter. Setting to False.'))
        closest = False

    if 'v' not in str(sess_n):
        raise ValueError('sess_n must be of a format like `1v3`.')

    sess_n = [n for n in sess_n.split('v')]
    for i in range(len(sess_n)):
        if sess_n[i] not in ['first', 'last']:
            sess_n[i] = int(sess_n[i])

    # get list of mice that fit the criteria
    mouse_ns = []
    for n in sess_n:
        if str(n) in ['first', 'last', '-1']:
            n = 'any'
        ns = get_sess_vals(
            mouse_df, 'mouse_n', mouse_n, n, task, layer, level, incl, 
            min_rois, min_corr, min_wrong, omit_mice, unique=True, sort=True, 
            split_planes=False)
        mouse_ns.append(ns)
    
    mouse_ns = set(mouse_ns[0]).intersection(set(mouse_ns[1]))
    # get session vals each mouse based on criteria 
    all_sess_vals = []
    returnlabs = ['mouse_n', 'sess_n', 'area', 'plane']
    for i in mouse_ns:
        mouse_sess_vals = []
        for j, n in enumerate(sess_n):
            if str(n) in ['first', 'last', '-1']:
                ns = get_sess_vals(
                    mouse_df, 'sess_n', i, 'any', task, layer, level, incl, 
                    min_rois, min_corr, min_wrong, omit_mice, 
                    split_planes=False, sort=True)
                if len(ns) == 0 or ns[-1] == sess_n[1-j]:
                    break # mouse omitted
                if n == 'first':
                    n = ns[0]
                else:
                    n = ns[-1]
            sess_vals = get_sess_vals(
                mouse_df, returnlabs, i, n, task, layer, level, incl, min_rois, 
                min_corr, min_wrong, omit_mice, split_planes=False)
            mouse_sess_vals.append(choose_sess(sess_vals, plane_1=True))
        
        if bylab:
            mouse_sess_vals = [list(val) for val in zip(*mouse_sess_vals)]

        all_sess_vals.append(mouse_sess_vals)
    
    if len(all_sess_vals) == 0:
        raise ValueError('No sessions meet the criteria.')

    return all_sess_vals


#############################################
def get_sess_ranges(sess_n='asc4', task='pd_2', ret='threshs'):
    """
    get_sess_ranges()
    
    Returns either the proportion correct threshold ranges or the range names
    for the specified session series.

    Optional args:
        - sess_n (str): string indicating number of sessions to order (2 is 
                        task is sens_stim) and whether to order them in 
                        ascending or reverse (e.g., 'rev3', 'asc4')
                        default: 'asc4'
        - task (str)  : task
                        default: 'pd_2'
        - ret (str)   : if 'threshs', proportion correct thresholds are 
                        returned for each session. If names, the names of each 
                        session group are returned.
                        default: 'threshs'

    Returns:
        if ret == 'threshs' or 'both':
        - threshs (nested list): list of proportion correct thresholds, 
                                     structured as range x [min, max]
                                 None if task is sens_stim
        if ret == 'names' or 'both':
        - names (list)         : list of names for each range
    """
    
    asc, rev = [lett in str(sess_n) for lett in ['asc', 'rev']]
    if not (asc or rev):
        raise ValueError(('`sess_n` must include `asc` or `rev` and the '
            'number of sessions to order.'))

    if task == 'pd_2':
        n = int(sess_n.replace('asc', '').replace('rev', ''))
        if n < 2:
            raise ValueError('Must have at least 2 sessions to order.')
        elif n == 2:
            names = ['naive', 'expert']
            threshs = [[0.00, 0.55], [0.75, 1.00]]
        elif n == 3:
            names = ['naive', 'learner', 'expert']
            threshs = [[0.00, 0.55], [0.55, 0.75], [0.75, 1.00]]
        elif n == 4:
            names = ['naive', 'early learner', 'late learner', 'expert']
            threshs = [[0.00, 0.55], [0.55, 0.65], [0.65, 0.75], [0.75, 1.00]]
        else:
            raise ValueError('Thresholds not set for more than 4 sessions.')
    elif task == 'sens_stim':
        n = 2
        names = ['pre', 'post']
        threshs = None
    else:
        raise ValueError('Function not implemented for {} tasks.'.format(
            ', '.join(task)))

    if ret == 'threshs':
        return threshs
    elif ret == 'names':
        return names
    elif ret == 'both':
        return threshs, names
    else:
        raise gen_util.accepted_values_error(
            'ret', ret, ['threshs', 'names', 'both']) 


#############################################
def sess_ord_per_mouse(mouse_df, mouse_n='any', sess_n='asc4', 
                       task='pd_2', layer='L2', level='shallow', incl=1, 
                       min_rois=1, min_corr=-1, min_wrong=-1, omit_mice=[], 
                       closest=False, bylab=False, miss=True):
    """
    sess_ord_per_mouse(mouse_df)
    
    Returns list of session ID combinations in ascending order that fit the 
    specified criteria.

    Required args:
        - mouse_df (str): path name of dataframe containing information 
                          on each session
        
    Optional args:
        - mouse_n (int or str)  : mouse number(s) of interest
                                  default: 'any'
        - sess_n (str)          : string indicating number of sessions to 
                                  order and whether to order them in 
                                  ascending or reverse (e.g., 'rev3', 'asc4')
                                  default: 'asc4'
        - task (str or list)    : task(s) of interest
                                  default: 'pd_2'
        - layer (str or list)   : layer value(s) of interest (1, 2, 3)
                                  default: 'any'
        - level (str or list)   : level value(s) of interest (1, 2, 3)
                                  default: 'any'
        - incl (str)            : which sessions to include (1, 0, 'any')
                                  default: 1
        - min_rois (int)        : min number of ROIs (ignored if negative)
                                  default: 1
        - min_corr (int or str) : min number of correct trials 
                                  (ignored if negative or task includes 
                                  sens_stim), optionally limited 
                                  to go/nogo trials
                                  default: -1
        - min_wrong (int or str): min number of wrong trials, optionally 
                                  limited to go/nogo trials
                                  (ignored if negative or task includes 
                                  sens_stim)
                                  default: -1
        - omit_mice (list)      : mice to omit
                                  default: []
        - closest (bool)        : if False, only exact session number is 
                                  retained, otherwise the closest
                                  default: False
        - bylab (bool)          : if True, values are returned by label. If 
                                  False, values are returned by session
                                  default: False
        - miss (bool)           : if True, allows for up to 50% of sessions 
                                  in series to be missing
                                  default: True
        - rev (bool)            : if True, sessions are returned in reverse
                                  order (descending)
                                  default: False

    Returns:
        - all_sess_vals (list): values for each session combination to analyse 
                                (1 per mouse), structured as
                                if bylab:
                                    mouse x val (mouse_n, sess_n, area, plane)
                                          x session
                                otherwise: 
                                    mouse x session 
                                          x val (mouse_n, sess_n, area, plane) 
    """
    if closest:
        print(('WARNING: Session comparisons not implemented using the '
            '`closest` parameter. Setting to False.'))
        closest = False

    threshs = get_sess_ranges(sess_n, task, ret='threshs')
    rev = 'rev' in str(sess_n)

    # get list of mice that fit the criteria
    mouse_ns = get_sess_vals(
        mouse_df, 'mouse_n', mouse_n, 'any', task, layer, level, incl, 
        min_rois, min_corr, min_wrong, omit_mice, unique=True, sort=True, 
        split_planes=False)

    # get session vals each mouse based on criteria 
    all_sess_vals = []
    returnlabs = ['mouse_n', 'sess_n', 'area', 'plane']
    for i in mouse_ns:
        mouse_vals = get_ordered_vals(
            mouse_df, returnlabs, threshs, i, task, layer, level, incl, 
            min_rois, min_corr, min_wrong, omit_mice, miss=miss, 
            bylab=bylab, rev=rev)
        if mouse_vals is not None:
            all_sess_vals.append(mouse_vals)
    
    if len(all_sess_vals) == 0:
        raise ValueError('No sessions meet the criteria.')

    return all_sess_vals


#############################################
def mouse_all_sess_vals(mouse_df, mouse_n=1, task='pd_2', layer='L2', 
                        level='shallow', incl=1, min_rois=1, min_corr=-1, 
                        min_wrong=-1, omit_mice=[], closest=False, 
                        bylab=False):
    """
    mouse_all_sess_vals(mouse_df)
    
    Returns list of session ID combinations in ascending order for a mouse
    that fit the specified criteria.

    Required args:
        - mouse_df (str): path name of dataframe containing information 
                          on each session
        
    Optional args:
        - mouse_n (int or str)  : mouse number of interest
                                  default: 1
        - task (str or list)    : task(s) of interest
                                  default: 'pd_2'
        - layer (str or list)   : layer value(s) of interest (1, 2, 3)
                                  default: 'any'
        - level (str or list)   : level value(s) of interest (1, 2, 3)
                                  default: 'any'
        - incl (str)            : which sessions to include (1, 0, 'any')
                                  default: 1
        - min_rois (int)        : min number of ROIs (ignored if negative)
                                  default: 1
        - min_corr (int or str) : min number of correct trials 
                                  (ignored if negative or task includes 
                                  sens_stim), optionally limited 
                                  to go/nogo trials
                                  default: -1
        - min_wrong (int or str): min number of wrong trials, optionally 
                                  limited to go/nogo trials
                                  (ignored if negative or task includes 
                                  sens_stim)
                                  default: -1
        - omit_mice (list)      : mice to omit
                                  default: []
        - closest (bool)        : if False, only exact session number is 
                                  retained, otherwise the closest
                                  default: False
        - bylab (bool)          : if True, values are returned by label. If 
                                  False, values are returned by session
                                  default: False


    Returns:
        - sess_vals (list): values for each session combination to analyse, 
                            structured as
                                if bylab:
                                    val (mouse_n, sess_n, area, plane) x sess
                                otherwise: 
                                    sess x val (mouse_n, sess_n, area, plane) 
    """
    if closest:
        print(('WARNING: Session comparisons not implemented using the '
            '`closest` parameter. Setting to False.'))
        closest = False

    # get list of mice that fit the criteria
    returnlabs = ['mouse_n', 'sess_n', 'area', 'plane']
    sess_vals = get_sess_vals(
        mouse_df, returnlabs, mouse_n, 'any', task, layer, level, incl, 
        min_rois, min_corr, min_wrong, omit_mice, unique=True, sort=True, 
        split_planes=False)
    
    # sort values by mice, then planes
    sort_idx  = np.asarray(range(len(sess_vals[0])))
    pl_sort   = np.argsort(sess_vals[-1])
    m_sort    = np.argsort(np.asarray(sess_vals[1])[pl_sort])
    sess_vals = [[vals[i] for i in sort_idx[pl_sort][m_sort]] 
        for vals in sess_vals]
    
    if len(sess_vals) == 0:
        raise ValueError('No sessions meet the criteria.')

    if bylab:
        sess_vals = list(zip(*sess_vals))

    return sess_vals


#############################################
def init_sessions(mouse_ns, sess_ns, areas, planes, datadir, mouse_df):
    """
    init_sessions(mouse_ns, sess_ns, areas, planes, datadir, mouse_df)

    Creates list of SessArea objects for each session values passed.

    Required args:
        - mouse_ns (list): mouse IDs for session areas
        - sess_ns (list) : session numbers for session areas
        - areas (list)   : area numbers for session areas
        - planes (list)  : plane numbers of interest for session areas
        - datadir (str)  : directory where sessions are stored
        - mouse_df (str) : path name of dataframe containing information 
                           on each session

    Returns:
        - sessions (list): list of Session objects
    """

    vals = [mouse_ns, sess_ns, areas, planes]
    vals = [gen_util.list_if_not(val) for val in vals]
    mouse_ns, sess_ns, areas, planes = vals
    
    sessions = []
    for mouse_n, sess_n, area, plane in zip(mouse_ns, sess_ns, areas, planes):
        if mouse_n is None:
            sess = None
            print('None (session skipped)')
        else:
            # creates a session object to work with
            sess = sessarea.SessArea(
                datadir, area, mouse_n=mouse_n, sess_n=sess_n, 
                mouse_df=mouse_df, plane_oi=plane) 

            print('Creating mouse {} ({}) session from {} ({})...'.format(
                sess.mouseid, sess.mouse_n, sess.date, sess.sess_n))

        sessions.append(sess)

    return sessions

