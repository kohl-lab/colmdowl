"""
mult_plot_util.py

This module contains basic functions for plotting with pyplot data generated 
from the MULTIBAR project (Kohl lab, Dr. Mariangela Panniello).

Authors: Colleen Gillon

Date: September, 2019

Note: this code uses python 3.7.

"""

import os

import numpy as np
from matplotlib import pyplot as plt

from util import gen_util, plot_util
from mult_util import mult_str_util


#############################################
def init_figpar(ncols=4, sharex=False, sharey=True, subplot_hei=7.5, 
                subplot_wid=7.5, datetime=True, use_dt=None, fig_ext='jpg', 
                overwrite=False, output='', plt_bkend=None, fontdir=None):
    
    """
    Returns a dictionary containing figure parameter dictionaries for 
    initializing a figure, saving a figure, and extra save directory 
    parameters.

    Required args: 
        - ncols (int)      : number of columns in the figure
                             default: 4 
        - sharex (bool)    : if True, x axis lims are shared across subplots
                             default: False 
        - sharey (bool)    : if True, y axis lims are shared across subplots
                             default: True
        - subplot_hei (num): height of each subplot (inches)
                             default: 7.5
        - subplot_wid (num): width of each subplot (inches)
                             default: 7.5
        - datetime (bool)  : if True, figures are saved in a subfolder named 
                             based on the date and time.
        - use_dt (str)     : datetime folder to use
                             default: None
        - fig_ext (str)    : figure extension
                             default: 'jpg'
        - overwrite (bool) : if False, overwriting existing figures is 
                             prevented by adding suffix numbers.
                             default: False
        - output (str)     : general directory in which to save output
                             default: ''

    Returns:
        - figpar (dict): dictionary containing figure parameters:
            ['init'] : dictionary containing the following inputs as
                       attributes:
                           ncols, sharex, sharey, subplot_hei, subplot_wid
            ['save'] : dictionary containing the following inputs as
                       attributes:
                           datetime, use_dt, fig_ext, overwrite
            ['dirs']: dictionary containing the following attributes:
                ['figdir'] (str)   : main folder in which to save figures
                ['roi'] (str)      : subdirectory name for ROI analyses
                ['run'] (str)      : subdirectory name for running analyses
                ['all_tr'] (str)   : subdirectory name for all traces
                ['denoised'] (str) : subdirectory name for denoising plots
                ['svm_learn'] (str): subdirectory name for SVM accuracy across 
                                     learning
            ['mng']: dictionary containing the following attributes:
                ['plt_bkend'] (str): mpl backend to use
                ['fontdir'] (str)  : path to directory containing additional 
                                     fonts
    """

    fig_init = {'ncols'      : ncols,
                'sharex'     : sharex,
                'sharey'     : sharey, 
                'subplot_hei': subplot_hei,
                'subplot_wid': subplot_wid
                }

    fig_save = {'datetime' : datetime,
                'use_dt'   : use_dt,
                'fig_ext'  : fig_ext,
                'overwrite': overwrite
                }
    
    fig_mng = {'plt_bkend': plt_bkend,
               'fontdir'  : fontdir,
                }

    figdir = os.path.join(output, 'results', 'figures')

    fig_dirs = {'figdir'   : figdir,
                'roi'      : os.path.join(figdir, 'roi'),
                'run'      : os.path.join(figdir, 'run'),
                'denoised' : 'denoised',
                'svm_learn': 'svm_learn',
                'all_tr'   : 'all_tr'
               }

    figpar = {'init' : fig_init,
              'save' : fig_save,
              'dirs' : fig_dirs,
              'mng'  : fig_mng
              }
    
    return figpar


#############################################
def get_stim_outc_cols(col_type='stim'):
    """
    get_stim_outc_cols()

    Optional args:
        - col_type (str): type of values and colors to return, 
                          i.e. 'stim' for stimulus position values and colors 
                               or 'outc' for outcome values and colors

    Returns:
        - col_dict (dict): dictionary of color values for each stimulus 
                           position or outcome
    """

    col_dict = dict()
    if col_type == 'stim':
        vals = [35, 56, 42, 63, 49, 70]
        cols = ['purple', 'pink', 'blue']
    elif col_type == 'outc':
        vals = ['corr_rej', 'corr_acc', 'false_pos', 'missed', 'too_soon']
        cols = ['green', 'red']
        col_dict['too_soon'] = plot_util.get_color_range(n=1, col='grey')[0]
    else:
        gen_util.accepted_values_error('col_type', col_type, ['stim', 'outc'])
    
    for c, col in enumerate(cols):
        cols = plot_util.get_color_range(n=4, col=col)[1:3]
        col_dict[vals[2 * c]] = cols[0]
        col_dict[vals[2 * c + 1]] = cols[1]

    return col_dict
    
    
#############################################
def plot_events(events, fps, cols=None, y_offset=1.0, y_scale=0.1):
    """
    plot_events(events, fps)

    ADD AT SOME POINT, IF USED    
    """

    def_cols = ['black', 'gray', 'darkblue', 'darkred', 'darkgreen', 'magenta', 
        'cyan']

    n_ev = len(events.keys())
    if cols is None:
        if n_ev < len(def_cols) + 1:
            cols = def_cols
    else:
        if len(cols) < n_ev:
            raise ValueError('If passing colors, must pass at least as '
                'many colors as events.')

    for i, key in enumerate(events.keys()):
        t_event = events[key]/fps
        y = (y_offset + y_scale * i) * np.ones(t_event.shape)
        plt.scatter(t_event, y, c=cols[i], label=key)


