"""
fz_util.py

This script contains utilities to work with data from the MULTIBAR 
project (Kohl lab, Dr. Mariangela Panniello).

Authors: Friedemann Zenke

Date: September, 2019

Note: this code uses python 3.7.

"""

import numpy as np


#############################################
def extract_dense_spike_raster(data, maxtime=100, binsize=1):
    """
    extract_dense_spike_raster(data)

    Returns 3D binary array indicating for each trial x neuron x time bin
    whether or not at least one spike occurred.

    Note: Jimmy's data structure may need to be transposed, if it is structured
    as neuron x trial.
    
    Required args:
        - data (data array): The data structure from Jimmy's MATLAB struct,
                             containing the spike times (in frames) for each
                             trial and each neuron, structured as 
                                 trials x neurons x [spike frame nbrs]
    
    Optional args:
        - maxtime (int)  : The maximum time dimension in frames. Needs to be 
                           larger or equal to maximum frame number otherwise
                           otherwise an error will be thrown.
                           default: 100
        - binsize (int)  : Number of frames to bin together. 1 corresponds to 
                           no binning.
                           default: 1
                
    Returns:
        - A (3D array): 3D binary array indicating for each trial x neuron x 
                        time bin whether or not at least one spike occurred, 
                        structured as trials x neurons x time bin
        
    """
    shape = data.shape + (maxtime//binsize, )
    A = np.zeros(shape)
    for i, trial in enumerate(data):
        for k, neuron in enumerate(trial):
            if not isinstance(neuron, list):
                neuron = [neuron]
            times = np.array(neuron, dtype=int)
            if np.max(times) > maxtime:
                raise ValueError('Times greater than maxtime found in data.')
            A[i][k][times//binsize] = 1.0 
    return A

