"""
mult_str_util.py

This module contains basic math functions for getting strings to print or save
files for analyses of data generated from the MULTIBAR project (Kohl lab, 
Dr. Mariangela Panniello).

Authors: Colleen Gillon

Date: September, 2019

Note: this code uses python 3.7.

"""

import datetime

import numpy as np

from util import gen_util
from mult_util import mult_gen_util


#############################################
def fluor_par_str(fluor='dff', str_type='file'):
    """
    fluor_par_str()

    Returns a string from the fluorescence parameter to print or for a 
    filename.

    Optional args:
        - fluor (str)   : whether 'raw', corrected fluorescence traces 'dff', 
                          'denoised' corrected traces or 'spike's are used  
                          default: 'dff'
        - str_type (str): 'print' for a printable string and 'file' for a
                          string usable in a filename.
                          default: 'file'
    
    Returns:
        - fluor_str (str): fluorescence parameter string
    """

    if fluor == 'raw':
        if str_type == 'print':
            fluor_str = 'raw fluorescence intensity'
        elif str_type == 'file':
            fluor_str = 'raw'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif fluor == 'dff':
        if str_type == 'print':
            delta = u'\u0394'
            fluor_str = u'{}F/F'.format(delta)
        elif str_type == 'file':
            fluor_str = 'dff'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif fluor == 'denoised':
        if str_type == 'print':
            delta = u'\u0394'
            fluor_str = u'denoised {}F/F'.format(delta)
        elif str_type == 'file':
            fluor_str = 'den_dff'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif fluor == 'spike':
        if str_type == 'print':
            fluor_str = 'spiking'
        elif str_type == 'file':
            fluor_str = 'spike'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    else:
        gen_util.accepted_values_error(
            'fluor', fluor, ['raw', 'dff', 'denoised', 'spike'])

    return fluor_str
    

#############################################
def stat_par_str(stats='mean', error='sem', str_type='file'):
    """
    stat_par_str()

    Returns a string from statistical analysis parameters to print or for a 
    filename.

    Optional args:
        - stats (str)   : 'mean' or 'median'
                          default: 'mean'
        - error (str)   : 'std' (for std or quartiles) or 'sem' (for SEM or MAD)
                          or 'None' is no error
                          default: 'sem'
        - str_type (str): use of output str, i.e., for a filename ('file') or
                          to print the info to console or for title ('print')
                          default: 'file'
    
    Returns:
        - stat_str (str): statistics combo string
    """
    if error in ['None', 'none']:
        stat_str = stats
    else:
        if str_type == 'print':
            sep = u'\u00B1' # +- symbol
        elif str_type == 'file':
            sep = '_'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])

        if stats == 'mean':
            stat_str = u'{}{}{}'.format(stats, sep, error)
        elif stats == 'median':
            if error == 'std':
                stat_str = u'{}{}qu'.format(stats, sep)
            elif error == 'sem':
                stat_str = u'{}{}mad'.format(stats, sep)
            else:
                gen_util.accepted_values_error(
                    'error', error, ['std', 'sem', 'None', 'none'])
        else:
            gen_util.accepted_values_error('stats', stats, ['mean', 'median'])
    return stat_str


#############################################
def layer_par_str(layer='any', str_type='file'):
    """
    layer_par_str()

    Returns a string from the layer parameter for a filename, or to print or 
    use in a title.

    Optional args:
        - layer (str or list): recording layer(s)
                               default: 'any'
        - str_type (str)     : use of output str, i.e., for a filename 
                               ('file') or to print the info to console 
                               ('print')
                               default: 'file'
    Returns:
        - lay_str (list): string containing info on layer(s)
    """


    if not isinstance(layer, str) and np.isnan(layer):
        if str_type == 'file':
            lay_str = 'unk_lay'
        elif str_type == 'print':
            lay_str = 'unknown layer'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif layer == 'any':
        if str_type == 'file':
            lay_str = 'all_lay'
        elif str_type == 'print':
            lay_str = 'all layers'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])

    else:
        layer = list(set(gen_util.list_if_not(layer)))
        
        n_lay = len(layer)
        if str_type == 'file':
            joiner = '-'
            layer = [lay for lay in layer]
        elif str_type == 'print':
            joiner = '/'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])

        if n_lay == 3:
            lay_str = ''
        elif n_lay in [1, 2]:
            lay_str = joiner.join(layer)
        else:
            raise ValueError(
                ('Unexpected number of layers found: {}').format(n_lay))

    return lay_str
 

#############################################
def level_par_str(level='any', str_type='file'):
    """
    level_par_str()

    Returns a string from the level parameter for a filename, or to print or 
    use in a title.

    Optional args:
        - level (str or list): recording level(s)
                               default: 'any'
        - str_type (str)     : use of output str, i.e., for a filename 
                               ('file') or to print the info to console 
                               ('print')
                               default: 'file'
    Returns:
        - lev_str (list): string containing info on level(s)
    """
    
    if not isinstance(level, str) and np.isnan(level):
        if str_type == 'file':
            lev_str = 'unk_lev'
        elif str_type == 'print':
            lev_str = 'unknown level'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif level == 'any':
        if str_type == 'file':
            lev_str = 'all_lev'
        elif str_type == 'print':
            lev_str = 'all levels'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])

    else:
        level = list(set(gen_util.list_if_not(level)))
        
        n_lev = len(level)
        if str_type == 'file':
            joiner = '-'
            level = [lev[:4] for lev in level]
        elif str_type == 'print':
            joiner = '/'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])

        if n_lev == 3:
            lev_str = ''
        elif n_lev in [1, 2]:
            lev_str = joiner.join(level)
        else:
            raise ValueError(
                ('Unexpected number of levels found: {}').format(n_lev))
    
    return lev_str


#############################################
def depth_par_str(depth='any', str_type='file'):
    """
    depth_par_str()

    Returns a string from the depth parameter for a filename, or to print or 
    use in a title.

    Required args:
        - depth (int): recording depth(s)

    Optional args:
        - str_type (str): use of output str, i.e., for a filename 
                          ('file') or to print the info to console 
                          ('print')
                          default: 'file'
    Returns:
        - dep_str (list): string containing info on depth(s) (layer and level)
    """

    if np.isnan(depth):
        if str_type == 'file':
            dep_str = 'unk_dep'
        elif str_type == 'print':
            dep_str = 'unknown depth'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    if depth == 'any':
        if str_type == 'file':
            dep_str = 'all_dep'
        elif str_type == 'print':
            dep_str = 'all depths'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    else:
        depth = gen_util.list_if_not(depth)
        layers = []
        levels = []
        for dep in depth:
            _, layer, level = mult_gen_util.depth_vals(dep)
            layers.append(layer)
            levels.append(level)
        
        lay_str = layer_par_str(layers, str_type)
        lev_str = level_par_str(levels, str_type)

        if str_type == 'file':
            btw = '_'
        elif str_type == 'print':
            btw = ' '
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
        
        dep_str = '{}{}{}'.format(lev_str, btw, lay_str)

    return dep_str


###############################################################
def task_par_str(task='pd_2', str_type='file'):
    """
    task_par_str()

    Returns a string from the task parameter for a filename, or to print or 
    use in a title.

    Optional args:
        - task (str)    : task (e.g., 'pd_2', 'sens_stim', 'switch' 
                          or 'any')
                          default: 'pd_2'
        - str_type (str): use of output str, i.e., for a filename 
                          ('file') or to print the info to console 
                          ('print')
                          default: 'file'
    Returns:
        - task_str (list): string containing info on task
    """

    if task == 'pd_2':
        if str_type == 'file':
            task_str = 'discr'
        elif str_type == 'print':
            task_str = 'discrimination task'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif task == 'switch':
        if str_type == 'file':
            task_str = 'switch'
        elif str_type == 'print':
            task_str = 'switched discrimination task'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif task == 'sens_stim':
        if str_type == 'file':
            task_str = 'stim'
        elif str_type == 'print':
            task_str = 'sensory stimulation task'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    elif task == 'any':
        if str_type == 'file':
            task_str = 'alltask'
        elif str_type == 'print':
            task_str = 'All tasks'
        else:
            gen_util.accepted_values_error(
                'str_type', str_type, ['print', 'file'])
    else:
        gen_util.accepted_values_error(
            'task', task, ['pd_2', 'switch', 'any', 'sens_stim'])

    return task_str
    

###########################
def laylev_par_str(layer='any', level='any', str_type='file'):
    """
    laylev_par_str()

    Returns a string from the layer and level parameters for a filename, or to 
    print or use in a title.

    Optional args:
        - level (str or list): recording level(s)
                               default: 'any'
        - layer (str or list): recording layer(s)
                               default: 'any'
        - str_type (str)     : use of output str, i.e., for a filename 
                               ('file') or to print the info to console 
                               ('print')
                               default: 'file'
    Returns:
        - laylev_str (list): string containing info on layer(s) and level(s)
    """

    lay_str  = layer_par_str(layer, str_type)
    lev_str  = level_par_str(level, str_type)

    if str_type == 'file':
        laylev_str = '{}_{}'.format(lev_str, lay_str)
    elif str_type == 'print':
        laylev_str = '{} {}'.format(lev_str, lay_str)
    else:
        gen_util.accepted_values_error(
            'str_type', str_type, ['print', 'file'])

    return laylev_str
    

###########################
def sess_par_str(sess_n, layer, level, task='pd_2', str_type='file'):
    """
    sess_par_str(sess_n)

    Returns a string from session and stimulus parameters for a filename, 
    or to print or use in a title.

    Required args:
        - sess_n (int): session number aimed for
        - layer (str) : recording layer
        - level (str) : recordig level

    Optional args:
        - task (str)    : session task ('sens_stim', 'pd_2', 'switch' or 'any')
                          default: 'pd_2'
        - str_type (str): use of output str, i.e., for a filename 
                          ('file') or to print the info to console 
                          ('print')
                          default: 'file'
    Returns:
        - sess_str (list): string containing info on session, task and depth 
                           parameters
    """

    laylev_str = laylev_par_str(layer, level, str_type)
    task_str = task_par_str(task, str_type)

    if str_type == 'file':
        sess_str = 'sess{}_{}_{}'.format(sess_n, task_str, laylev_str)
    elif str_type == 'print':
        sess_str = 'session {} ({}) in {}'.format(sess_n, task_str, laylev_str)
    else:
        gen_util.accepted_values_error(
            'str_type', str_type, ['print', 'file'])

    return sess_str
    

#############################################
def datatype_par_str(datatype='roi'):
    """
    datatype_par_str()

    Returns a string for the datatype.

    Optional args:
        - datatype (str): type of data, i.e. 'run' or 'roi'
                          default: 'roi'
    Returns:
        - data_str (list): string containing datatype info
    """

    if datatype == 'run':
        data_str = 'running'
    elif datatype == 'roi':
        data_str = 'ROI'
    else:
        gen_util.accepted_values_error('datatype', datatype, ['roi', 'run'])
    
    return data_str
    

#############################################
def datatype_dim_str(datatype='roi'):
    """
    datatype_dim_str()

    Returns a string for the dimension along which error is calculated for 
    the specified datatype.

    Optional args:
        - datatype (str): type of data, i.e. 'run' or 'roi'
                          default: 'roi'
    Returns:
        - dim_str (list): string containing dimension info
    """

    if datatype == 'run':
        dim_str = 'trials'
    elif datatype == 'roi':
        dim_str = 'ROIs'
    else:
        gen_util.accepted_values_error('datatype', datatype, ['roi', 'run'])
    
    return dim_str
    

#############################################
def outc_par_str(outc='all', case='v', str_type='file'):
    """
    outc_par_str()

    Returns a string for the outcome split.

    Optional args:
        - outc (str)    : outcome split, 
                            i.e. all outcomes separate ('all'), 
                            outcomes grouped by correct v wrong ('corr_wrong'), 
                            only go-related outcomes ('go'), 
                            only nogo-related outcomes ('nogo')
                            only correct outcomes ('corr')
                            only wrong outcomes ('wrong')
        - case (str)    : use case, either 'v' for 
        - str_type (str): use of output str, i.e., for a filename 
                          ('file') or to print the info to console 
                          ('print')
                          default: 'file'
    Return:
        - outc_str (list): string containing outcome split info
    """

    if str_type not in ['file', 'print']:
        gen_util.accepted_values_error('str_type', str_type, ['print', 'file'])

    if case not in ['v', '&']:
        gen_util.accepted_values_error('case', case, ['v', '&'])

    if case == '&' and outc == 'corr_wrong':
        outc = 'all'

    if outc in ['all', 'any']:
        if str_type == 'file':
            outc_str = 'all-outc'
        elif str_type == 'print':
            if case == 'v':
                outc_str = 'corr acc v corr reg v missed v false pos'
            elif case == '&':
                outc_str = 'all trials (except too soon)'
    elif outc == 'corr_wrong':
        if str_type == 'file':
            outc_str = 'corr-wrong'
        elif str_type == 'print':
            if case == 'v':
                outc_str = 'correct v wrong'
    elif outc == 'go':
        if str_type == 'file':
            outc_str = 'go-outc'
        elif str_type == 'print':
            if case == 'v':
                outc_str = 'correct accept v missed'
            elif case == '&':
                outc_str = 'go trials'
    elif outc == 'nogo':
        if str_type == 'file':
            outc_str = 'nogo-outc'
        elif str_type == 'print':
            if case == 'v':
                outc_str = 'correct rejection v false positive'
            elif case == '&':
                outc_str = 'nogo trials'
    elif outc == 'corr':
        if str_type == 'file':
            outc_str = 'corr'
        elif str_type == 'print':
            if case == 'v':
                outc_str = 'correct accept v rejected'
            elif case == '&':
                outc_str = 'correct trials'
    elif outc == 'wrong':
        if str_type == 'file':
            outc_str = 'wrong'
        elif str_type == 'print':
            if case == 'v':
                outc_str = 'missed v false positive'
            elif case == '&':
                outc_str = 'wrong trials'
    else:
        gen_util.accepted_values_error(
            'outc', outc, ['all', 'corr_wrong', 'go', 'nogo', 'corr', 'wrong'])

    return outc_str


#############################################
def prepost_par_str(pre, post, diff=False, str_type='file'):
    """
    prepost_par_str(pre, post)

    Returns a string for the pre and post values.

    Required args:
        - pre (num) : pre value (in seconds)
        - post (num): post value (in seconds) 

    Optional args:
        - diff (bool)   : if True, weighted difference was used
                          default: False
        - str_type (str): use of output str, i.e., for a filename 
                          ('file') or to print the info to console 
                          ('print')
                          default: 'file'
    Return:
        - prepost_str (str): string containing pre-post info
    """

    vals = [pre, post]

    # convert to int if equivalent
    for i in range(len(vals)):
        if int(vals[i]) == float(vals[i]):
            vals[i] = int(vals[i])
    
    if str_type == 'file':
        # replace . by -
        prepost_str = '{}pre-{}post'.format(*vals).replace('.', '-')
        if diff:
            prepost_str = '{}_diff'.format(prepost_str)
    elif str_type == 'print':
        if vals[0] != 0:
            vals[0] = -vals[0]
        prepost_str = '{}-{}s'.format(*vals)
        if diff:
            prepost_str = '{} diff'.format(prepost_str)
    else:
        gen_util.accepted_values_error('str_type', str_type, ['print', 'file'])

    return prepost_str


#############################################
def roi_par_str(roi='denoised', resp_rois=True, str_type='file'):
    """
    roi_par_str()

    Returns a string for the ROI trace type.

    Optional args:
        - roi (str)       : type of ROI trace (e.g., 'denoised', 'raw', 'corr', 
                            'nmf', 'pca')
                            default: 'denoised'
        - resp_rois (bool): if True, only responsive ROIs included in analysis
                            default: True
        - str_type (str)  : use of output str, i.e., for a filename 
                            ('file') or to print the info to console 
                            ('print')
                            default: 'file'
    Return:
        - roi_str (str): string containing ROI trace type info
    """


    if str_type not in ['file', 'print']:
        gen_util.accepted_values_error('str_type', str_type, ['print', 'file'])

    if roi == 'denoised':
        if str_type == 'file':
            roi_str = 'den-tr'
        elif str_type == 'print':
            roi_str = 'denoised ROIs'
    elif roi == 'raw':
        if str_type == 'file':
            roi_str = 'raw-tr'
        elif str_type == 'print':
            roi_str = 'raw ROIs'
    elif roi == 'corr':
        if str_type == 'file':
            roi_str = 'corr-tr'
        elif str_type == 'print':
            roi_str = 'corrected ROIs'
    elif roi == 'nmf':
        if str_type == 'file':
            roi_str = 'nmf-tr'
        elif str_type == 'print':
            roi_str = 'NMF traces'
    elif roi == 'pca':
        if str_type == 'file':
            roi_str = 'pca-tr'
        elif str_type == 'print':
            roi_str = 'PCA traces'
    else:
        gen_util.accepted_values_error(
            'roi', roi, ['denoised', 'raw', 'corr', 'nmf', 'pca'])

    if resp_rois:
        if str_type == 'file':
            roi_str = '{}-resp'.format(roi_str)
        elif str_type == 'print':
            roi_str = '{} (resp)'.format(roi_str)

    return roi_str

