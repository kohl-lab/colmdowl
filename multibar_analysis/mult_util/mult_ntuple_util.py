"""
mult_ntuple_util.py

This module contains functions to initialize namedtuples for analyses on data 
from the MULTIBAR project (Kohl lab, Dr. Mariangela Panniello).

Authors: Colleen Gillon

Date: October, 2018

Note: this code uses python 3.7.
"""

from collections import namedtuple

from util import gen_util


#############################################
def init_analyspar(stats='mean', error='sem', scale=None):
    """
    Returns a AnalysPar namedtuple with the inputs arguments as named 
    attributes.

    Optional args:
        - stats (str)        : statistic parameter ('mean' or 'median')
                               default: 'mean'
        - error (str)        : error statistic parameter, ('std' or 'sem')
                               default: 'sem'
        - scale (str or bool): if scaling is used or type of scaling used 
                               (e.g., 'roi', 'all', 'none')
                               default: None

    Returns:
        - analyspar (AnalysPar namedtuple): AnalysPar with input arguments as 
                                            attributes
    """

    analys_pars = [stats, error, scale]
    analys_keys = ['stats', 'error', 'scale']
    AnalysPar   = namedtuple('AnalysPar', analys_keys)
    analyspar   = AnalysPar(*analys_pars)
    return analyspar


#############################################
def init_sesspar(sess_n, closest=False, layer='any', level='any', task='any', 
                 min_rois=1, min_corr=-1, min_wrong=-1, incl=1, mouse_n='any'):
    """
    Returns a SessPar namedtuple with the inputs arguments as named 
    attributes.

    Required args:
        - sess_n (int): session number aimed for

    Optional args:
        - closest (bool)            : if False, only exact session number is 
                                      retained, otherwise the closest.
                                      default: False
        - layer (str or list)       : layer
                                      default: 'any'
        - level (str or list)       : level
                                      default: 'any'
        - task (str or list)        : task
                                      default: 'any'
        - min_rois (int)            : min number of ROIs
                                      default: 1
        - min_corr (int)            : min correct accepted trials
                                      default: -1
        - min_wrong (int)           : min wrong trials
                                      default: -1
        - incl (str or list)        : incl values of interest (0, 1, 
                                      'all')
                                      default: 1
        - mouse_n (str, int or list): mouse number
                                      default: 'any'
    
    Returns:
        - sesspar (SessPar namedtuple): SessPar with input arguments as 
                                        attributes
    """

    sess_pars = [sess_n, closest, layer, level, task, min_rois, min_corr, 
        min_wrong, incl, mouse_n]
    sess_keys = ['sess_n', 'closest', 'layer', 'level', 'task', 'min_rois', 
        'min_corr', 'min_wrong', 'incl', 'mouse_n']
    SessPar   = namedtuple('SessPar', sess_keys)
    sesspar   = SessPar(*sess_pars)
    return sesspar


#############################################
def init_trialpar(pre=0, post=1.5, diff=False):
    """
    Returns a TrialPar namedtuple with the inputs arguments as named 
    attributes.

    Optional args:
        - pre (num)  : range of frames to include before each
                       reference frame (in s)
                       default: 0
        - post (num) : range of frames to include after each 
                       reference frame (in s)
                       default: 1.5
        - diff (bool): if True, the weighted difference between post and pre is 
                       taken for the SVM
                       default: False
    
    Returns:
        - trialpar (TrialPar namedtuple): TrialPar with input arguments as 
                                          attributes
    """

    trial_keys = ['pre', 'post', 'diff']
    trial_pars = [pre, post, diff]
    TrialPar   = namedtuple('TrialPar', trial_keys)
    trialpar   = TrialPar(*trial_pars)
    return trialpar


#############################################
def init_svmpar(cv=5, outc_cl='all', stim_cl='all', ctrl=False, n_shuff=20, 
                sub=5):
    """
    Returns a SVMPar namedtuple with the inputs arguments as named 
    attributes.

    Optional args:
        - cv (int)     : number of cross-validation folds (at least 3)
                         default: 5
        - outc_cl (str): the outcomes to classify
            'all'       : 'corr_acc' v 'corr_rej' v 'missed' v 'false_pos'
            'corr_wrong': 'corr_acc' or 'corr_rej' v 'missed' or 'false_pos'
            'go'        : 'corr_acc' v 'missed'
            'nogo'      : 'corr_rej' v 'false_pos'
            'corr'      : 'corr_acc' v 'corr_rej'
            'wrong'     : 'missed' v 'false_pos'
                         default: 'all'
        - stim_cl (str): the outcomes to include for stim position
            'all'       : 'corr_acc', 'corr_rej', 'missed' and 'false_pos'
            'corr'      : 'corr_acc' and 'corr_rej'
            'wrong'     : 'missed' and 'false_pos'
                         default: 'all'
        - ctrl (bool)  : if True, control positions are used (sens_stim only)
                         default: False
        - n_shuff (int): number of shuffled SVMs to run
                         default: 20
        - sub (int)    : number of ROIs to subsample (or percentage if < 1)
                         default: 5
    Returns:
        - svmpar (SVMPar namedtuple): SVMPar with input arguments as 
                                      attributes
    """

    svm_keys = ['cv', 'outc_cl', 'stim_cl', 'ctrl', 'n_shuff', 'sub']
    svm_pars = [cv, outc_cl, stim_cl, ctrl, n_shuff, sub]
    SVMPar   = namedtuple('SVMPar', svm_keys)
    svmpar   = SVMPar(*svm_pars)
    return svmpar


#############################################
def init_roipar(trace_type='denoised', resp_rois=False):
    """
    Returns an ROIPar namedtuple with the inputs arguments as named 
    attributes.

    Optional args:
        - trace_type (str): ROI signals to use (e.g., 'raw', 'denoised', 'corr', 
                           'nmf', 'pca')
                            default: 'denoised'
        - resp_rois (bool): if True, only responsive ROIs are used
                            default: False

    Returns:
        - roipar (ROIPar namedtuple): ROIPar with input arguments as 
                                      attributes
    """

    roi_keys = ['trace_type', 'resp_rois']
    roi_pars = [trace_type, resp_rois]
    ROIPar   = namedtuple('ROIPar', roi_keys)
    roipar   = ROIPar(*roi_pars)
    return roipar


#############################################
def get_modif_ntuple(ntuple, keys, key_vals):
    """
    get_modif_ntuple(ntuple, keys, key_vals)
    
    Returns a ntuple with certain attributes modified.

    Required args:
        - ntuple (named tuple): specific type of named tuple
        - keys (list)         : list of keys to modify
        - key_vals (list)     : list of values to assign to keys

    Returns:
        - ntuple (named tuple): same namedtuple type, but modified
    """

    ntuple_dict = ntuple._asdict()

    ntuple_name = type(ntuple).__name__

    ntuple_types = ['AnalysPar', 'SessPar', 'TrialPar', 'SVMPar']
    
    init_fcts = [init_analyspar, init_sesspar, init_trialpar, init_svmpar]

    if not isinstance(keys, list):
        keys = [keys]
        key_vals = [key_vals]

    for key, val in zip(keys, key_vals):
        if key not in ntuple_dict.keys():
            raise ValueError('{} not in ntuple dictionary keys.'.format(key))
        ntuple_dict[key] = val

    if ntuple_name in ntuple_types:
        type_idx = ntuple_types.index(ntuple_name)
        fct = init_fcts[type_idx]
        ntuple_new = fct(**ntuple_dict)
    else:
        raise ValueError(
            ('ntuple of type {} not recognized.').format(ntuple_name))

    return ntuple_new
