"""
oasis_nmf_analysis.py

This script analyses ROI trace data from the MULTIBAR project 
(Kohl lab, Dr. Mariangela Panniello)..

Authors: Colleen Gillon, Friedemann Zenke, Mariangela Panniello

Date: September, 2019

Note: this code uses python 3.7.

"""

import argparse
import copy
import inspect
import importlib  
import os
import sys

import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import scipy.io

from analysis import roi_analys, sessarea
from mult_util import mult_fz_util, mult_gen_util, mult_mat_util, \
                      mult_ntuple_util, mult_plot_util
from util import gen_util, plot_util
from plot_fcts import plot_from_dicts_tool as plot_dicts

DEFAULT_DATADIR = os.path.join('..', 'data', 'KohlLab_multibar', '2p_data', 
    'df-structures', 'imaging')
DEFAULT_MOUSE_DF_PATH = 'mouse_df.csv'
DEFAULT_FONTDIR = os.path.join('..', 'tools', 'fonts')


#############################################
def reformat_args(args):
    """
    reformat_args(args)

    Returns args with an added cycler argument with layer and level 
    combinations.
    Checks whether stim_cl parameter is appropriate.

    Required args:
        - args (Argument parser): parser with the following attributes: 
            layer (str)  : layer(s) to analyse
            level (str)  : level(s) to analyse
            stim_cl (str): outcomes to include for the stim position SVM, 
                           e.g. 'all', 'corr', 'wrong'
    
    Returns:
        - args (Argument parser): input parser, with the following attributes
                                  added:
            - analysis (str): analysis letters
            - cycler (list) : list of layer, level combinations
            - seed (int)    : seed number
    """
    
    args = copy.deepcopy(args)

    layers, levels = args.layer, args.level
    all_layers, all_levels = mult_gen_util.lay_lev_from_depth('any')
    
    cycle = 0
    if layers == 'cycle':
        cycle += 1
        layers = all_layers
    if levels == 'cycle':
        levels = all_levels + ['any']
        cycle += 1

    layers = gen_util.list_if_not(layers)
    levels = gen_util.list_if_not(levels)

    args.cycler = [[lay, lev] for lay in layers for lev in levels]
    
    if cycle == 2:
        args.cycler.append(['any', 'any'])

    if args.stim_cl in ['go', 'nogo']:
        gen_util.accepted_values_error(
            'args.stim_cl', args.stim_cl, ['all', 'corr', 'wrong'])

    # chose a seed if none is provided (i.e., args.seed=-1), but seed later
    args.seed = gen_util.seed_all(
        args.seed, 'cpu', print_seed=False, seed_now=False)

    # collect analysis letters
    all_analyses = ''.join(get_analysis_fcts().keys())
    if 'all' in args.analyses:
        if '_' in args.analyses:
            excl = args.analyses.split('_')[1]
            args.analyses, _ = gen_util.remove_lett(all_analyses, excl)
        else:
            args.analyses = all_analyses
    elif '_' in args.analyses:
        raise ValueError('Use `_` in args.analyses only with `all`.')

    return args


#############################################
def init_param_cont(args, sess_n):
    """
    init_param_cont(args, sess_n)

    Returns args:
        - in the following nametuples: analyspar, sesspar
        - in the following dictionary: figpar 

    Required args:
        - args (Argument parser): parser with the following attributes:
            cv (int)               : number of cross-validation splits for SVM
            error (str)            : error statistic parameter ('std' or 'sem')
            fontdir (str)          : path to directory containing additional 
                                     fonts
            incl (str)             : sessions to include (0, 1, 'all') 
            layer (str)            : layer ('L2', 'L3', 'any')
            level (str)            : level to analyse ('shallow', 'mid', 
                                     'deep', 'any')
            min_corr (int)         : minimum number of correct trials
            min_wrong (int)        : minimum number of wrong trials
            n_shuff (int)          : number of shuffle SVMs to run
            ncols (int)            : number of columns
            no_datetime (bool)     : if True, figures are not saved in a 
                                     subfolder named based on the date and time.
            not_closest (bool)     : if True, only exact session number is 
                                     retained, otherwise the closest.
            outc_cl (str)          : how to split outcomes for the SVM, 
                                     e.g., 'all', 'corr_wrong', 'go', nogo'
            output (str)           : general directory in which to save output
            overwrite (bool)       : if False, overwriting existing figures 
                                     is prevented by adding suffix numbers.
            plt_bkend (str)        : mpl backend to use
            post (num)             : range of frames to include after each 
                                     reference frame (in s)
            pre (num)              : range of frames to include before each 
                                     reference frame (in s)
            resp_rois (bool)       : if True, responsive ROIs are used
            sess_n (int)           : session number
            stats (str)            : statistic parameter ('mean' or 'median')
            stim_cl (str)          : outcomes to include for the stim position 
                                     SVM, e.g. 'all', 'corr', 'wrong'
            task (str)             : task to analyse
            trace_type (str)       : ROI signals to use (e.g., 'denoised', 
                                     'corr', 'nmf', 'pca')
        - sess_n (int or str)    : session number (different from args if 
                                   args.sess_n is 'all')

    Returns:
        - analysis_dict (dict): dictionary of analysis parameters
            ['analyspar'] (AnalysPar)    : named tuple of analysis parameters
            ['sesspar'] (SessPar)        : named tuple of session parameters
            ['trialpar'] (TrialPar)      : named tuple of trial parameters
            ['svmpar'] (SVMPar)          : named tuple of SVM parameters
            ['figpar'] (dict)            : dictionary containing following 
                                            subdictionaries:
                ['init']: dict with following inputs as attributes:
                    ['ncols'] (int)      : number of columns in the figures
                    ['sharex'] (bool)    : if True, x axis lims are shared 
                                           across subplots
                    ['sharey'] (bool)    : if True, y axis lims are shared 
                                           across subplots
                    ['subplot_hei'] (num): height of each subplot (inches)
                    ['subplot_wid'] (num): width of each subplot (inches)

                ['save']: dict with the following inputs as attributes:
                         'overwrite', as well as
                    ['datetime'] (bool) : if True, figures are saved in a 
                                          subfolder named based on the date 
                                          and time.
                    ['fig_ext'] (str)   : figure extension
                    ['overwrite'] (bool): if True, existing figures can be 
                                          overwritten
                    ['use_dt'] (str)    : datetime folder to use

                ['dirs']: dict with the following attributes:
                    ['figdir'] (str)   : main folder in which to save figures
                    ['roi'] (str)      : subdirectory name for ROI analyses
                    ['run'] (str)      : subdirectory name for running analyses
                    ['denoised'] (str) : subdirectory name for denoising plots 

                ['mng']: dict with the following attributes:
                    ['plt_bkend'] (str): mpl backend to use
                    ['fontdir'] (str)  : path to directory containing  
                                         additional fonts
    """

    args = copy.deepcopy(args)

    analysis_dict = dict()

    # analysis parameters
    analysis_dict['analyspar'] = mult_ntuple_util.init_analyspar(
        args.stats, args.error)

    # session parameters
    analysis_dict['sesspar'] = mult_ntuple_util.init_sesspar(
        sess_n, not(args.not_closest), args.layer, args.level, args.task, 
        incl=args.incl, min_corr=args.min_corr, min_wrong=args.min_wrong, 
        mouse_n=args.mouse_n)
    
    # trial parameters
    analysis_dict['trialpar'] = mult_ntuple_util.init_trialpar(
        args.pre, args.post, args.diff)

    # ROI parameters
    analysis_dict['roipar'] = mult_ntuple_util.init_roipar(
        args.trace_type, args.resp_rois)

    # SVM parameters
    analysis_dict['svmpar'] = mult_ntuple_util.init_svmpar(
        args.cv, args.outc_cl, args.stim_cl, args.ctrl, args.n_shuff, args.sub)

    # figure parameters
    analysis_dict['figpar'] = mult_plot_util.init_figpar(
        ncols=int(args.ncols), datetime=not(args.no_datetime), 
        overwrite=args.overwrite, output=args.output, plt_bkend=args.plt_bkend, 
        fontdir=args.fontdir)

    return analysis_dict


#############################################
def prep_analyses(sess_n, args, mouse_df):
    """
    prep_analyses(sess_n, args, mouse_df)

    Prepares named tuples and sessions for which to run analyses, based on the 
    arguments passed.

    Required args:
        - sess_n (int)          : session number to run analyses on, or 
                                  combination of session numbers to compare, 
                                  e.g. '1v2'
        - args (Argument parser): parser containing all parameters
        - mouse_df (pandas df)  : path name of dataframe containing information 
                                  on each session

    Returns:
        - sessions (list)     : list of sessions, or nested list per mouse 
                                if sess_n is a combination
        - analysis_dict (dict): dictionary of analysis parameters 
                                (see init_param_cont())

    """

    args = copy.deepcopy(args)

    comb, asc, rev, m_all = [lett in str(sess_n) 
                            for lett in ['v', 'asc', 'rev', 'm_all']]
    if not (comb or asc or rev or m_all) and sess_n not in ['first', 'last']:
        sess_n = int(sess_n)

    analysis_dict = init_param_cont(args, sess_n)
    sesspar = analysis_dict['sesspar']

    # get session IDs and create Sessions
    if comb or asc or rev:
        # returns mouse x (mouse_n, sess_n, area, plane)
        if comb:
            sessvals = mult_gen_util.sess_comb_per_mouse(
                mouse_df, bylab=True, omit_mice=args.omit_mice, 
                **sesspar._asdict())
        elif asc or rev:
            sessvals = mult_gen_util.sess_ord_per_mouse(
                mouse_df, bylab=True, omit_mice=args.omit_mice, miss=True, 
                **sesspar._asdict())
        sessions = []
        for vals in sessvals:
            print('\nSession group: ')
            mouse_sess = mult_gen_util.init_sessions(
                *vals, args.datadir, mouse_df)
            sessions.append(mouse_sess)
    elif m_all:
        sesspar_dict = sesspar._asdict()
        _ = sesspar_dict.pop('sess_n')
        sessvals = mult_gen_util.mouse_all_sess_vals(mouse_df, **sesspar_dict)
        sessions = mult_gen_util.init_sessions(
            *sessvals, args.datadir, mouse_df)
    else:
        print(sesspar)
        sessvals = mult_gen_util.sess_per_mouse(
            mouse_df, bylab=True, omit_mice=args.omit_mice, **sesspar._asdict())
        sessions = mult_gen_util.init_sessions(
            *sessvals, args.datadir, mouse_df)

    print(('\nAnalysis of {} {} responses during {}.'
        '\nSession {}').format(
            sesspar.level, sesspar.layer, sesspar.task, sesspar.sess_n))

    return sessions, analysis_dict

    
#############################################
def get_analysis_fcts():
    """
    get_analysis_fcts()

    Returns dictionary of analysis functions.

    Returns:
        - fct_dict (dict): dictionary where each key is an analysis letter, and
                           records the corresponding 
                           [function, possible tasks, possible comb values, 
                           possible asc/rev values] 
    """

    # fct, tasks, comb, asc/rev
    tasks_any = ['sens_stim', 'pd_2', 'switch', 'any']
    comb_any = [True, False]
    ascrev_any = [True, False]

    fct_dict = dict()

    # 1. Plots the denoised traces for each ROI
    fct_dict['d'] = [roi_analys.run_deconvolved_traces, tasks_any, False, False]

    # # 2. Plots the main PCA components
    # fct_dict['p'] = [roi_analys.run_pca_traces, tasks_any, False, False]

    # # 3. Plots the main NMF components
    # fct_dict['n'] = [roi_analys.run_nmf_traces, tasks_any, False, False]

    # 4. Calculates the correlation between NMFs and events
    fct_dict['c'] = [
        roi_analys.run_nmf_corr, ['pd_2', 'switch'], True, ascrev_any]

    # 5. Calculates SVM scores across learning for each mouse
    fct_dict['s'] = [
        roi_analys.run_svm_across_learn, ['sens_stim', 'pd_2'], comb_any, True]

    # 6. Analyses and plots average traces by stimulus across sessions 
    fct_dict['t'] = [
        roi_analys.run_traces_split, tasks_any, comb_any, ascrev_any]

    # 7. NMF before SVM?
    # 8. graph SVM weights averaged across time (in the 1 sec post)
    # 9. OR run SVM for each slice of time around onset
    # 10. Graph average+sem SVM performance for each trial (for each mouse)

    return fct_dict


#############################################
def run_analyses(sessions, analysis_dict, analyses, seed=None, parallel=False):
    """
    run_analyses(sessions, analyspar, sesspar, figpar)

    Run requested analyses on sessions using the named tuples passed.

    Required args:
        - sessions (list)          : list of sessions
        - analysis_dict (dict): analysis parameter dictionary 
                                (see init_param_cont())
        - analyses (str)      : analyses to run
    
    Optional args:
        - seed (int)     : seed to use
                           default: None
        - parallel (bool): if True, some analyses are parallelized 
                           across CPU cores 
                           default: False
    """

    if len(sessions) == 0:
        print('No sessions fit these criteria.')
        return

    # changes backend and defaults
    plot_util.manage_mpl(cmap=False, **analysis_dict['figpar']['mng'])

    comb, asc, rev = [lett in str(sess_n) for lett in ['v', 'asc', 'rev']]
    if rev:
        comb = False

    fct_dict = get_analysis_fcts()

    args_dict = copy.deepcopy(analysis_dict)
    for key, item in zip(['seed', 'parallel'], [seed, parallel]):
        args_dict[key] = item

    # run through analyses
    for analysis in analyses:
        if analysis not in fct_dict.keys():
            raise ValueError(f'{analysis} analysis not found.')
        fct, tasks_req, comb_req, ascrev_req = fct_dict[analysis]
        
        # check requirements
        if analysis_dict['sesspar'].task not in gen_util.list_if_not(tasks_req):
            continue
        if comb not in gen_util.list_if_not(comb_req):
            continue
        if bool(asc + rev) not in gen_util.list_if_not(ascrev_req):
            continue
        
        args_dict_use = gen_util.keep_dict_keys(
            args_dict, inspect.getfullargspec(fct).args)
        fct(sessions=sessions, analysis=analysis, **args_dict_use)
    

if __name__ == "__main__":

    parser = argparse.ArgumentParser()

        # general parameters
    parser.add_argument('--datadir', default=None, 
        help=('data directory (if None, uses a directory defined below'))
    parser.add_argument('--output', default='', help='where to store output')
    parser.add_argument('--plt_bkend', default=None, 
        help='mpl backend to use, e.g. when running on server')
    parser.add_argument('--seed', default=-1, type=int, 
        help='random seed (-1 for None)')
    parser.add_argument('--analyses', default='all', 
        help='analyses to run: denoised traces (d)')
    parser.add_argument('--sess_n', default=1,
        help='session to aim for, e.g. 1, 2, last, all, asc4, m_all')
    parser.add_argument('--dict_path', default='', 
        help=('path to info dictionary or directory of dictionaries from '
            'which to plot data.'))
    parser.add_argument('--parallel', action='store_true', 
        help='run analyses in parallel.')

        # session parameters
    parser.add_argument('--mouse_n', default=1, help='mouse to analyse')
    parser.add_argument('--task', default='pd_2', help='task to analyse')
    parser.add_argument('--layer', default='cycle', help=('layer, e.g., L2, '
                                                          'L3, any, cycle'))
    parser.add_argument('--level', default='cycle', 
        help='level, e.g., shallow, mid, deep, any, cycle')

        # trial parameters
    parser.add_argument('--post', default=1.0, type=float, 
        help='sec after reference frames')

        # SVM parameters
    parser.add_argument('--cv', default=5, type=int, 
        help='nbr of cross validation folds')
    parser.add_argument('--outc_cl', default='corr_wrong', 
        help=('how to split outcomes, e.g. all, corr_wrong, go, nogo'))
    parser.add_argument('--stim_cl', default='all', 
        help=('which outcomes to include for stim position.'))
    parser.add_argument('--n_shuff', default=20, 
        help=('Nbr of shuffled SVMs to run.'))
    parser.add_argument('--ctrl', action='store_true', 
        help=('If True, control sensory positions used.'))
    parser.add_argument('--sub', default=0, type=float, 
        help=('Number of ROIs or percentage to retain.'))

        # ROI parameters
    parser.add_argument('--trace_type', default='denoised', 
        help='ROI signals to use.')
    parser.add_argument('--resp_rois', action='store_true', 
        help='use only responsive ROIs.')

    # generally fixed 
        # analysis parameters
    parser.add_argument('--stats', default='mean', help='plot mean or median')
    parser.add_argument('--error', default='sem', 
        help='sem for SEM/MAD, std for std/qu')    

        # session parameters
    parser.add_argument('--not_closest', action='store_true', 
        help=('if True, the closest session number is used. '
            'Otherwise, only exact.'))
    parser.add_argument('--incl', default=1, help='sessions to include')
    parser.add_argument('--min_corr', default=-1, 
        help=('minimal number of correct trials (e.g. 5, 5go, 2each)'))
    parser.add_argument('--min_wrong', default=-1, 
        help=('minimal number of wrong trials (e.g. 5, 5go, 2each)'))
        # trial parameters  
    parser.add_argument('--pre', default=0, type=float, help='sec before frame')
    parser.add_argument('--diff', action='store_true', help='use diff in SVM')
       
        # figure parameters
    parser.add_argument('--ncols', default=4, help='number of columns')
    parser.add_argument('--no_datetime', action='store_true',
        help='create a datetime folder')
    parser.add_argument('--overwrite', action='store_true', 
        help='allow overwriting')

    args = parser.parse_args()

    args.fontdir = DEFAULT_FONTDIR

    if args.dict_path is not '':
        source = 'roi'
        plot_dicts.plot_from_dicts(
            args.dict_path, source=source, plt_bkend=args.plt_bkend, 
            fontdir=args.fontdir, parallel=args.parallel, 
            datetime=not(args.no_datetime))

    else:
        args.omit_mice = []
        args = reformat_args(args)

        if args.datadir is None: args.datadir = DEFAULT_DATADIR
        mouse_df = DEFAULT_MOUSE_DF_PATH
        
        for args.layer, args.level in args.cycler:
            try:
                # get numbers of sessions to analyse
                if args.sess_n == 'all':
                    all_sess_ns = mult_gen_util.get_sess_vals(mouse_df, 
                        'sess_n', mouse_n=args.mouse_n, task=args.task, 
                        layer=args.layer, level=args.level,  incl=args.incl, 
                        min_corr=args.min_corr, min_wrong=args.min_wrong, 
                        omit_mice=args.omit_mice, unique=True, sort=True, 
                        split_planes=False)
                else:
                    all_sess_ns = gen_util.list_if_not(args.sess_n)
    
                for sess_n in all_sess_ns:
                    sessions, analys_pars = prep_analyses(
                        sess_n, args, mouse_df)
                    run_analyses(sessions, analys_pars, args.analyses, 
                        args.seed, args.parallel)
            except ValueError as err:
                if str(err) == 'No sessions meet the criteria.':
                    pass
                else:
                    raise err


