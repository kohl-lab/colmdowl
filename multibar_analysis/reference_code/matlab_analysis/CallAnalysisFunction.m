clear all
close all hidden

allmouse{1}='AIGC1.1a'; %old (no exact frame rate)
allmouse{2}='CGCC8.5a'; %not exact frame rate on 20180208
allmouse{3}='AIGC1.1d'; %old
allmouse{4}='CTBD2.1b';
allmouse{5}='VIP51.1a';
allmouse{6}='RKNF15.7a';
allmouse{7}='AIGC2.3b'; % the non df-f structure is used here cause the pipeline throws an error
allmouse{8}='AIGC2.3c';
allmouse{9}='AIGC2.3f';
allmouse{10}='AIGC2.2a';
allmouse{11}='AIGC2.2c';
allmouse{12}='CTBD1.3b';
allmouse{13}='CTBD1.3d';
allmouse{14}='CTBD1.3g'; %dates with no issues are [1 2 4 5]
allmouse{15}='CTBD6.3c';
allmouse{16}='CTBD6.3h';
allmouse{17}='CTBD2.1i';
allmouse{18}='CTBD2.5d';
allmouse{19}='CTBD2.5a';
allmouse{20}='CTBD7.2a';
allmouse{21}='CTBD7.2e';
allmouse{22}='CTBD11.3e';
allmouse{23}='CTBD11.3g';
allmouse{24}='CTBD7.6e';
allmouse{25}='CTBD7.6f';
allmouse{26}='CTBD13.5c';



root=['/Users/mariangelapanniello/Desktop/2p_data/df-structures/imaging/']; %for df/f traces
%root=['/Users/mariangelapanniello/Dropbox/K_lab/structures/'];

savePath = ['/Users/mariangelapanniello/Desktop/Results/'];
%savePath = ['/Users/mariangelapanniello/Desktop/Results/test_licks/'];



stats=1;
mari_ttest=1; %mari_ttest abd mmk_ttest cannot be both 1, cause they don't have two separate outputs, but they both output good_rois, which will be used to run the anova test or other tests to check sensitivity
mmk_ttest=0; sparsness_indx=20; % threshold for number of trials neurons has to pass t-test on, to be considered responsive
anova_test=0;
sel_index_test=1;
sel_index_integral_test=0;
do_kerrys_test=0;
trial_test=0; %an anova to see whether a neuron prefers go or no go trials
outcome_test=2; %set to 1 to run an anova to see whether a neuron is more active in correct, missed, false, reject or toosoon trials. Set to 2 if you don't want to run any of these tests


get_shape=0; %if you want to manually input the response curve' shape
manual_selection=1; % to manually or authomatically scelect neuron's category
plot_resp=0; % to plot max responses for response curve shapes
plot_trials=0; % to plot transients over trials
vel_threshold=40;

savedata=1;
%%

for thismouse = [26]%:length(allmouse)
    mouse = allmouse{thismouse};
    
    %stim and prestim for passive stimulation
    prestim_sec = 3; %prestimulus window in seconds
    stim_sec = 3; %stimulus window in seconds
    
    %stim and prestim for training (trials and outcomes)
    prestim_discrim = 3; %choose length in sec of presstim period
    stim_discrim = 6; %length of post-stim when we only analyse trial types. without outcome. It should be the length of the stim+the length of the withold_window, in sec. 
    prestim_outcomes = 3; %prestim window when working on outcomes
    stim_outcomes = 6.5; %for working on outcomes(this should be similar to length stim + length licking window = these two are generally 1.5 and 5 seconds)
    prestim_licks = 2;
    
    
    load([root mouse '.mat']);
    dates = fieldnames(imaging);
    
    for this_date = 1:length(dates)
        date = dates{this_date};
        areas= fieldnames(imaging.(date));
        
        for this_area = 1:length(areas)
            area = areas{this_area};
            all_planes= fieldnames(imaging.(date).(area));
            
            if isfield(imaging.(date).(area).session_behaviour, 'velocity')
                
                velocity=imaging.(date).(area).session_behaviour.velocity;
                if plot_trials
                figure()
                plot(velocity)
                end
            end
            

            for this_plane = 1:length(all_planes)-1
                
                plane = all_planes{this_plane};
                frate = imaging.(date).(area).(plane).fRate;
                
                corrected_traces= imaging.(date).(area).(plane).fluoresence_corrected; %whole traces corrected for neuropil and df/f'ed
                %figure(10);clf
                %imagesc(corrected_traces)
                
                
                if isfield(imaging.(date).(area).session_behaviour, 'go_position')
                    
                    %create structure for followng analysis:
                    discr_trials.(date).(area).(plane).good_rois_trials=[]; %results of t-test based on trial type
                    discr_outcomes.(date).(area).(plane).good_rois_outcomes=[]; %results of t-test based on outcome type
                    discr_trials.(date).(area).(plane).best_trials=[]; %does roi prefer go or nogo? based on t-test between the responses to the two trial types
                    discr_outcomes.(date).(area).(plane).best_outcomes=[]; % is the cell more active when outcome is correct, missed, false, reject or too_soon? done with anova
                    discr_trials.(date).(area).(plane).anova_trials=[];
                    discr_outcomes.(date).(area).(plane).anova_outcomes=[];
                    discr_trials.(date).(area).(plane).data_trials={}; %data cell to use for correlation analysis
                    discr_outcomes.(date).(area).(plane).data_outcomes={}; %data cell to use for correlation analysis
                    discr_trials.(date).(area).(plane).is_red=[]; %is is a red labelled neuron
                    discr_outcomes.(date).(area).(plane).is_red=[];
                    discr_trials.(date).(area).(plane).inhib_rois_trials=[];
                    discr_outcomes.(date).(area).(plane).inhib_rois_outcomes=[];
                    discr_outcomes.(date).(area).(plane).all_trials_licks=[];
                    discr_outcomes.(date).(area).(plane).all_trials_licks_outcome=[];
                    
                    [discr_trials, discr_outcomes]=analyse_discrim(discr_trials, discr_outcomes, imaging, date, area, plane, this_area, this_plane,frate, corrected_traces, stats, plot_trials, plot_resp, trial_test, outcome_test, prestim_discrim, stim_discrim, prestim_outcomes, stim_outcomes, prestim_licks);
                    
                else
                    
                    stim_length= (imaging.(date).(area).session_behaviour.motor_back(1) - imaging.(date).(area).session_behaviour.motor_atWhisk(1))*1/frate;%the time between when the motor arrives at the whiskers and when it starts going back (for plotting purposes)
                    pos_list=(imaging.(date).(area).session_behaviour.stim_position)';
                    speed_list=(imaging.(date).(area).session_behaviour.stim_speed)';
                    pos_speed_list=[pos_list, speed_list];
                    pos_values=unique(pos_list);
                    speed_values=unique(speed_list);
                    combinations = combvec(pos_values', speed_values'); %this must be built in the same order as pos_speed_list(ie. either first pos and then speed for both, or the other way round)
                    
                    if mari_ttest
                        good_rois=zeros(length(corrected_traces(:,1)),1); %is 1 if neuron passes t test, 0 if not.
                    elseif mmk_ttest
                        good_rois=[];
                    end
                    
                    anova_pass_pos=[]; %cells passing anova for position
                    anova_pass_speed=[]; %cells passing anova for speed
                    
                    tuning_run.(date).(area).(plane).good_rois=[];
                    tuning_norun.(date).(area).(plane).good_rois=[];
                    tuning_run.(date).(area).(plane).resp_go=[];
                    tuning_run.(date).(area).(plane).resp_nogo=[];
                    tuning_norun.(date).(area).(plane).resp_go=[];
                    tuning_norun.(date).(area).(plane).resp_nogo=[];
                    tuning_run.(date).(area).(plane).best_pos=[];
                    tuning_norun.(date).(area).(plane).best_pos=[];
                    tuning_run.(date).(area).(plane).anova_pos=[];
                    tuning_run.(date).(area).(plane).anova_speed=[];
                    tuning_norun.(date).(area).(plane).anova_pos=[];
                    tuning_norun.(date).(area).(plane).anova_speed=[];
                    tuning_run.(date).(area).(plane).select_index=[];
                    tuning_norun.(date).(area).(plane).select_index=[];
                    tuning_run.(date).(area).(plane).resp_index=[];
                    tuning_norun.(date).(area).(plane).resp_index=[];
                    tuning_run.(date).(area).(plane).categories=[];
                    tuning_norun.(date).(area).(plane).categories=[];
                    tuning_run.(date).(area).(plane).pos_nospeed=[];
                    tuning_run.(date).(area).(plane).pos_speedA=[];
                    tuning_run.(date).(area).(plane).is_red=[];
                    tuning_norun.(date).(area).(plane).pos_nospeed=[];
                    tuning_norun.(date).(area).(plane).pos_speedA=[];
                    tuning_norun.(date).(area).(plane).is_red=[];
                    
                    tuning_all.(date).(area).(plane).good_rois=[];
                    tuning_all.(date).(area).(plane).resp_go=[];
                    tuning_all.(date).(area).(plane).resp_nogo=[];
                    tuning_all.(date).(area).(plane).best_pos=[];
                    tuning_all.(date).(area).(plane).anova_pos=[];
                    tuning_all.(date).(area).(plane).anova_speed=[];
                    tuning_all.(date).(area).(plane).select_index=[];
                    tuning_all.(date).(area).(plane).resp_index=[];
                    tuning_all.(date).(area).(plane).categories=[];
                    tuning_all.(date).(area).(plane).pos_nospeed=[];
                    tuning_all.(date).(area).(plane).pos_speedA=[];
                    tuning_all.(date).(area).(plane).is_red=[];
                    
                    tuning_all.(date).(area).(plane).data={};
                    tuning_norun.(date).(area).(plane).data={};
                    tuning_run.(date).(area).(plane).data={};
                    
                    
                    for this_neuron = 1:length(corrected_traces(:,1))
                        
                        data_thisneuron= cell(length(combinations(1,:)),1); %for each neuron a cell [#of pos x speed combinations by 1]. Each of these contains a matrix [# trials for that position by #frames in the trial (includes pre and post stim)]
                        data_thisneuron_toplot= cell(length(combinations(1,:)),1); %use this to plot as many frames before and after stim you want to plot, regardless of how long a trial is
                        data_velocity= cell(length(combinations(1,:)),1);
                        
                        for kk = 2:(length(imaging.(date).(area).session_behaviour.motor_atWhisk)-1) % taking from the second to the penultime trial here!! to avoid issues with creating traces!
                            ss = imaging.(date).(area).session_behaviour.motor_atWhisk(kk); %trials are centered around motor_start (i.e. stimulation starts when motor_start occurs)
                            this_trial = corrected_traces(this_neuron,ss-floor(frate*prestim_sec):ss+floor(frate*stim_sec));
                            this_trial_toplot = corrected_traces(this_neuron,ss-floor(frate*prestim_sec):ss+floor(frate*(stim_sec*2)));%use this to plot as many frames before and after stim you want to plot, regardless of how long a trial is
                            this_trial_velocity = velocity(ss-floor(frate*prestim_sec):ss+floor(frate*stim_sec))';
                            
                            this_comb = pos_speed_list(kk,:);
                            for index = 1:length(combinations(1,:))
                                if isequal(this_comb',combinations(:,index))
                                    break
                                end
                            end
                            data_thisneuron{index} = [data_thisneuron{index}; this_trial];
                            data_thisneuron_toplot{index} = [data_thisneuron_toplot{index}; this_trial_toplot];
                            data_velocity{index} = [data_velocity{index}; this_trial_velocity];
                        end
                        
                        for jj = 1:length(data_velocity)
                            max_vel_trial{jj}=max(abs(data_velocity{jj}(:,:)')); %max velocity reached within a trial (considering pre and post stim together)
                        end
                        
                        data_thisneuron_run = {};
                        data_thisneuron_norun = {};
                        data_thisneuron_toplot_run = {};
                        data_thisneuron_toplot_norun = {};
                        
                        for i=1:size(data_thisneuron)
                            data_thisneuron_run{i,1} = [];
                            data_thisneuron_norun{i,1} = [];
                            data_thisneuron_toplot_run{i,1} = [];
                            data_thisneuron_toplot_norun{i,1} = [];
                            
                            for j=1:size(data_thisneuron{i,1},1) % rows -> number of trials
                                trialvel = max_vel_trial{i}(j);
                                if trialvel >= vel_threshold || trialvel <= (-vel_threshold)% mouse is running
                                    data_thisneuron_run{i,1} = [data_thisneuron_run{i,1}; data_thisneuron{i,1}(j,:)];
                                    data_thisneuron_toplot_run{i,1} = [data_thisneuron_toplot_run{i,1}; data_thisneuron_toplot{i,1}(j,:)];
                                else % mouse is not running
                                    data_thisneuron_norun{i,1} = [data_thisneuron_norun{i,1}; data_thisneuron{i,1}(j,:)];
                                    data_thisneuron_toplot_norun{i,1} = [data_thisneuron_toplot_norun{i,1}; data_thisneuron_toplot{i,1}(j,:)];
                                end
                            end
                            if isempty(data_thisneuron_run{i})
                                data_thisneuron_run{i} = zeros(1,length(data_thisneuron{i,1}(1,:)));
                            end
                            if isempty(data_thisneuron_norun{i})
                                data_thisneuron_norun{i} = zeros(1,length(data_thisneuron{i,1}(1,:)));
                            end
                            if isempty(data_thisneuron_toplot_run{i})
                                data_thisneuron_toplot_run{i} = zeros(1,length(data_thisneuron_toplot{i,1}(1,:)));
                            end
                            if isempty(data_thisneuron_toplot_norun{i})
                                data_thisneuron_toplot_norun{i} = zeros(1,length(data_thisneuron_toplot{i,1}(1,:)));
                            end
                            
                        end
                        
                        tuning_run = do_plot_stats(imaging, date, area, plane, this_area, this_plane, data_thisneuron_run, data_thisneuron_toplot_run, tuning_run, frate, stim_sec, prestim_sec, stim_length, combinations, this_neuron, plot_trials, stats, 0, 'Run trials', mari_ttest, mmk_ttest, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois, anova_pass_pos, anova_pass_speed, do_kerrys_test, anova_test, sel_index_test, sel_index_integral_test);
                        tuning_norun = do_plot_stats(imaging, date, area, plane, this_area, this_plane, data_thisneuron_norun, data_thisneuron_toplot_norun, tuning_norun, frate, stim_sec, prestim_sec, stim_length, combinations, this_neuron, plot_trials, stats, 100, 'No run trials', mari_ttest, mmk_ttest, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois, anova_pass_pos, anova_pass_speed, do_kerrys_test, anova_test, sel_index_test, sel_index_integral_test);
                        tuning_all = do_plot_stats(imaging, date, area, plane, this_area, this_plane, data_thisneuron, data_thisneuron_toplot, tuning_all, frate, stim_sec, prestim_sec, stim_length, combinations, this_neuron, plot_trials, stats, 200, 'All trials', mari_ttest, mmk_ttest, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois, anova_pass_pos, anova_pass_speed, do_kerrys_test, anova_test, sel_index_test, sel_index_integral_test);
                        
                    end
                    
                end
            end
            
        end
    end
    
    if savedata
        tuningtosave=[savePath '/tuning_data/' mouse '_RvsNR_tuning.mat'];
        save(tuningtosave, 'tuning_run','tuning_norun','tuning_all', 'frate', 'stim_length','prestim_sec','stim_sec','-v7.3');
        
         discrim_tosave=[savePath '/discrim_data/' mouse '_trials_oucome_resps.mat'];
         save(discrim_tosave, 'discr_trials','discr_outcomes', 'frate', 'prestim_discrim', 'stim_discrim', 'prestim_outcomes', 'stim_outcomes', '-v7.3');
    end
    
    display('done!')
    
end
