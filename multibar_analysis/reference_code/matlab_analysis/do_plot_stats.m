function [ tuning ] = do_plot_stats(imaging, date, area, plane, this_area, this_plane, data_thisneuron, data_thisneuron_toplot, tuning, frate, stim_sec, prestim_sec, stim_length, combinations, this_neuron, plot_trials, stats, fig_offset, fig_title_suffix, mari_ttest, mmk_ttest, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois, anova_pass_pos, anova_pass_speed, do_kerrys_test, anova_test, sel_index_test, sel_index_integral_test)

%create cells of the right shape to be filled with responses from all trials, for each position/speed
test_prestim_mari = cell(length(data_thisneuron),1);
test_stim_mari = cell(length(data_thisneuron),1);
test_prestim_mmk = cell(length(data_thisneuron),1);
test_stim_mmk = cell(length(data_thisneuron),1);
avg_allcomb = [];
avg_allcomb_toplot = [];

tuning.(date).(area).(plane).data{this_neuron}=data_thisneuron;%this
tuning.(date).(area).(plane).data{this_neuron}=data_thisneuron;%this
tuning.(date).(area).(plane).data{this_neuron}=data_thisneuron;%this

for jj = 1:length(data_thisneuron)
    avg_thiscomb = mean(data_thisneuron{jj},1);%mean response curve averaged across trials, for one pos x speed combination
    avg_thiscomb_toplot = mean(data_thisneuron_toplot{jj},1);
    avg_allcomb = [avg_allcomb; avg_thiscomb];
    avg_allcomb_toplot = [avg_allcomb_toplot; avg_thiscomb_toplot];
    
    test_prestim_mari{jj} = max(data_thisneuron{jj}(:,1:floor(frate*prestim_sec))'); %create this for stats tests
    test_stim_mari{jj} = max(data_thisneuron{jj}(:,floor(frate*prestim_sec):end)'); %create this for stats tests
    test_prestim_mmk{jj} = data_thisneuron{jj}(:,1:floor(frate*prestim_sec));
    test_stim_mmk{jj} = data_thisneuron{jj}(:,floor(frate*prestim_sec):end);
end

if plot_trials
    ccbin=[[0 1 0];[1 0 1];[1 0 0];[0 0 1]; [1 1 0] ; [0 0 0]];
    avg_allcomb_toplot=avg_allcomb_toplot';
    figure(3+fig_offset);clf
    if fig_offset==0
        set(gcf, 'position', [384   372   616   258]);
    elseif fig_offset==100
        set(gcf, 'position', [371    53   623   245]);
    elseif fig_offset==200
        set(gcf, 'position', [ 378   704   622   249]);
    end
    
    for kk=1:length(data_thisneuron_toplot)
        subplot(length(speed_values),length(pos_values),kk)
        currentFigure = gcf;
        title(currentFigure.Children(end), [fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)]);
        time= (1:1:size(data_thisneuron_toplot{kk},2)).*1./frate; time_toplot=mat2cell(time, 1, size(data_thisneuron_toplot{kk},2));%to plot time instead of frames on x axis make up a cell array, so that you can use cellfun
        %p = cellfun(@plot,(cellfun(@transpose,data_thisneuron_toplot(kk),'UniformOutput',false)), 'UniformOutput',false);
        p = cellfun(@plot,time_toplot, (cellfun(@transpose,data_thisneuron_toplot(kk),'UniformOutput',false)), 'UniformOutput',false);
        set(gca,'ylim',[-1 5], 'xlim', [0 10], 'LineWidth', 2, 'FontSize', 14)
        if kk==1
                     ylabel('df/f (a.u.)')
                     xlabel('time (s)')
        end
        xticks ([0:1:6])
        box off
        if imaging.(date).(area).(plane).is_red(this_neuron)==0
            set(p{1:end},'Color',[.8 .8 .8])
        else
            set(p{1:end},'Color',[.65 1 .2])
        end
        
        hold on
        x1=[prestim_sec prestim_sec]; y1=ylim;
        x2=[prestim_sec+stim_length prestim_sec+stim_length]; y2=ylim;
        %plot(x1, y1, '-k')
        hold on
        %plot(x2, y2, '-k')
        y_plot=[y1, fliplr(y1)];
        x_plot=[x1, fliplr(x2)];
        
        h=fill(x_plot, y_plot, ccbin(kk,:));
        
        set(h,'facealpha',.3)
        set(h,'EdgeColor','none')
        
        hold on
        plot(time, avg_allcomb_toplot(:,kk),'Linewidth', 1.5, 'Color', [0 0 0])
    end
    
    figure(5+fig_offset);clf
    if fig_offset==0
        set(gcf, 'position', [1010         389         560         187]);
    elseif fig_offset==100
        set(gcf, 'position', [1010         130         560         185]);
    elseif fig_offset==200
        set(gcf, 'position', [1010         652         559         187]);
    end
    for kk=1:length(data_thisneuron_toplot)
        subplot(length(speed_values),length(pos_values),kk)
        currentFigure = gcf;
        title(currentFigure.Children(end), [fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)]);
        imagesc(cell2mat(data_thisneuron_toplot(kk)))
        set(gca,'LineWidth', 2, 'FontSize', 12)
    end
    
    
    figure(6+fig_offset)
    plot(time,avg_allcomb_toplot)
    title([fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)])
    set(gca, 'ylim', [-1 5],  'LineWidth', 2, 'FontSize', 14)
    box off

        ylabel('df/f (a.u.)')
        xlabel('time (s)')

    if fig_offset==0
        set(gcf, 'position', [19   378   345   251]);
    elseif fig_offset==100
        set(gcf, 'position', [23    53   347   248]);
    elseif fig_offset==200
        set(gcf, 'position', [33   704   345   252]);
    end
    
    %         figure(5+fig_offset);clf
    %         for kk=1:length(data_thisneuron_toplot)
    %             subplot(length(speed_values),length(pos_values),kk)
    %             %find_min= cellfun(@min, data_thisneuron_toplot(kk),'UniformOutput',false); %minimum value for each frame, across trials. It'll be the lower edge on the shaded curve in the plot
    %             find_min= cellfun(@(x) min(x,2),data_thisneuron_toplot(kk),'UniformOutput',false);%data_thisneuron_toplot(kk),'UniformOutput',false); %minimum value for each frame, across trials. It'll be the lower edge on the shaded curve in the plot
    %             find_min=(cell2mat(find_min));
    %             find_max= cellfun(@(x) max(x,2),data_thisneuron_toplot(kk),'UniformOutput',false);%data_thisneuron_toplot(kk),'UniformOutput',false); %minimum value for each frame, across trials. It'll be the lower edge on the shaded curve in the plot
    %             %find_max= cellfun(@max, data_thisneuron_toplot(kk),'UniformOutput',false);
    %             find_max=(cell2mat(find_max));
    %             XX=linspace(1,length(data_thisneuron_toplot{kk}),length(data_thisneuron_toplot{kk}));
    %             X_plot = [XX, fliplr(XX)];
    %             Y_plot = [find_min, fliplr(find_max)];
    %             h=fill(X_plot, Y_plot, 'g');
    %             set(h,'facealpha',.25)
    %             set(h,'EdgeColor','none')
    %             hold on
    %             plot(avg_allcomb_toplot(:,kk),'Linewidth', 1.5, 'Color', [0 0 0])
    %         end
    
    
    pause
end

prestim = avg_allcomb(:,1:floor(frate*prestim_sec))';
stim = avg_allcomb(:,floor(frate*prestim_sec)+1:end)';
responses_allcomb = (max(stim)) - (max(prestim));

%normalise responses between 0 and 1
prestim_stim=[prestim; stim];
prestim_stim_norm=[];
for gg=1:length((prestim_stim(1,:)))
    normalised=(prestim_stim(:,gg)-min(prestim_stim(:,gg)))/(max(prestim_stim(:,gg))-min(prestim_stim(:,gg))); %traces for each stim are normalised between 0 and 1
    prestim_stim_norm=[normalised prestim_stim_norm];
end
prestim_norm=prestim_stim_norm(1:floor(frate*prestim_sec),:);
stim_norm = prestim_stim_norm((floor(frate*prestim_sec)+1:end),:);

if stats
    if mari_ttest
        all_results_mari=[];
        all_results_mari_INHIB=[];
        for runtest=1:length(test_prestim_mari)
            this_result_mari=ttest(test_stim_mari{runtest}(1,:),test_prestim_mari{runtest}(1,:),'alpha',0.05,'tail','right');
            this_result_mari_INHIB=ttest(test_stim_mari{runtest}(1,:),test_prestim_mari{runtest}(1,:),'alpha',0.05,'tail','left');
            all_results_mari=[all_results_mari this_result_mari];
            all_results_mari_INHIB=[all_results_mari_INHIB this_result_mari_INHIB];
        end
        
        if all_results_mari(3)==1 % is the neuron responding to the position used as go?
           tuning.(date).(area).(plane).resp_go(this_neuron)=1;
        else
           tuning.(date).(area).(plane).resp_go(this_neuron)=0;
        end
        
        if all_results_mari(6)==1 % is the neuron responding to the position used as nogo?
           tuning.(date).(area).(plane).resp_nogo(this_neuron)=1;
        else
           tuning.(date).(area).(plane).resp_nogo(this_neuron)=0;
        end
        
        if sum(all_results_mari)>=1
            good_rois(this_neuron)=1;
            tuning.(date).(area).(plane).good_rois(this_neuron)=1;
        else
            tuning.(date).(area).(plane).good_rois(this_neuron)=0;
        end
        
        if sum(all_results_mari_INHIB)>=1
            inhib_rois(this_neuron)=1;
            tuning.(date).(area).(plane).inhib_rois(this_neuron)=1;
        else
            tuning.(date).(area).(plane).inhib_rois(this_neuron)=0;
        end
        
    end
    if mmk_ttest
        results_trials_mmk=cell(length(combinations(1,:)),1);
        for runtest_thispos=1:length(test_prestim_mmk)
            for zz=1:cellfun('size',data_thisneuron(runtest_thispos),1) %for each trial
                this_result_mmk=ttest2(test_stim_mmk{runtest_thispos}(zz,:),test_prestim_mmk{runtest_thispos}(zz,:),'alpha',0.01,'tail','right');
                results_trials_mmk{runtest_thispos}=[results_trials_mmk{runtest_thispos}; this_result_mmk];
            end
        end
        allresp_thiscell_mmk=[];
        for mm = 1:length(test_prestim_mmk)
            if sum([results_trials_mmk{mm}])>=(cellfun('length',results_trials_mmk(mm))*sparsness_indx)/100
                resp_mmk=1;
            else
                resp_mmk=0;
                
            end
            allresp_thiscell_mmk=[allresp_thiscell_mmk resp_mmk];
        end
        if sum(allresp_thiscell_mmk)>=1
            good_rois=[good_rois 1];
            tuning.(date).(area).(plane).good_rois(this_neuron)=1;
        else
            good_rois=[good_rois 0];
            tuning.(date).(area).(plane).good_rois(this_neuron)=0;
        end
    end
    
    %%% check responses to positions regardless of speed %%%%
    max_pos_prestim = [];
    max_pos_stim = [];
    mean_pos_prestim = [];
    mean_pos_stim = [];
    area_pos_prestim = [];
    area_pos_stim = [];
    mean_prestim_norm = [];
    mean_stim_norm = [];
    
    for zz = 1:length(pos_values)
        prestim_pos_temp = [];
        stim_pos_temp = [];
        prestim_pos_temp_norm = [];
        stim_pos_temp_norm = [];
        for xx = 1:length(speed_values)
            prestim_pos_temp = [prestim_pos_temp, prestim(:,zz+(xx-1)*length(pos_values))];
            stim_pos_temp = [stim_pos_temp, stim(:,zz+(xx-1)*length(pos_values))];
            prestim_pos_temp_norm = [prestim_pos_temp_norm, prestim_norm(:,zz+(xx-1)*length(pos_values))];
            stim_pos_temp_norm = [stim_pos_temp_norm, stim_norm(:,zz+(xx-1)*length(pos_values))];
        end
        mean_pos_stim = [mean_pos_stim mean(stim_pos_temp')];  %avg value of all poststim frames
        mean_pos_prestim = [mean_pos_prestim mean(prestim_pos_temp')];  %avg value of all prestim frames
        max_pos_prestim = [max_pos_prestim max(prestim_pos_temp')];
        max_pos_stim = [max_pos_stim max(stim_pos_temp')];
        
        area_pos_prestim = [area_pos_prestim 1/frate*(sum(prestim_pos_temp_norm))];
        area_pos_stim = [area_pos_stim   1/frate*(sum(stim_pos_temp_norm))];
        mean_prestim_norm = [mean_prestim_norm mean(prestim_pos_temp_norm)]; %mean value of normalised avg response curves
        mean_stim_norm = [mean_stim_norm mean(stim_pos_temp_norm)];
    end
    
    if length(mean_pos_stim)>length(mean_pos_prestim)
        responses_pos = mean_pos_stim (1:length(mean_pos_prestim)) - mean_pos_prestim;
    elseif length(mean_pos_prestim)>length(mean_pos_stim)
        responses_pos = mean_pos_stim  - mean_pos_prestim(1:length(mean_pos_stim));
    else
        responses_pos = mean_pos_stim  - mean_pos_prestim;
    end
    
    %%% these two lines take the non normalised max or mean responses (their number equals the number of positions) and normalises them only now
    %responses_pos_norm=responses_pos./max(responses_pos); %normalised to the maximum
    responses_pos_norm=(responses_pos-min(responses_pos))/(max(responses_pos)-min(responses_pos)); %normalised betwen 0 and 1
    %%%% ends here %%%%
    
    %%%% here we use the normalised average response curves, for each of which we extracted the mean value above. We now produce our 4 responses according to them.
    responses_pos_norm_mean = mean_stim_norm - mean_prestim_norm;
    %%%
    
    %%% here instead we used the normalised average curves created to calculate the areas under the curve for prestim and post stim. We subtract the two areas and try the selectivity index on this value
    responses_pos_area=area_pos_stim - area_pos_prestim;
    %%%
    
    %find index of best positions
    if good_rois(this_neuron)==1
        [trash,index_pos] =max(responses_pos); %find index of best position
        tuning.(date).(area).(plane).best_pos(this_neuron)=index_pos;
    else
        tuning.(date).(area).(plane).best_pos(this_neuron)=0;
    end
    
    %%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%% start tests to check for neuronal sensitivity to stimulus %%%%%%%%%%%%%%%%
    if anova_test
        %%%%% ANOVA TEST for effect of speed and position %%%%%% %%%%%%
        anova_responses = cellfun(@minus,test_stim_mari,test_prestim_mari, 'UniformOutput',false);
        anova_means = (cellfun(@mean,anova_responses, 'UniformOutput',false))';
        pos_factor=combinations(1,:);
        speed_factor=combinations(2,:);
        
        all_pvalues = anovan(anova_means,{pos_factor, speed_factor},'display','off');%,'model','interaction','display','off','varnames',{'position'; 'speed'});
        
        pos_tuned=all_pvalues(1)<0.05; %check if this neuron is sensitive to position
        speed_tuned=all_pvalues(2)<0.05; %check if this neuron is sensitive to speed
        if good_rois(this_neuron)==1
            anova_pass_pos=[anova_pass_pos pos_tuned];
            anova_pass_speed=[anova_pass_speed speed_tuned];
            tuning.(date).(area).(plane).anova_pos(this_neuron)=pos_tuned;
            tuning.(date).(area).(plane).anova_speed(this_neuron)=speed_tuned;
        else
            anova_pass_pos=[anova_pass_pos 0];
            anova_pass_speed=[anova_pass_speed 0];
            tuning.(date).(area).(plane).anova_pos(this_neuron)=0;
            tuning.(date).(area).(plane).anova_speed(this_neuron)=0;
        end
    end   
    if sel_index_test %selectivity index from Puccini et al., 2006 (plos biology). To check selecetivity to position regardless of speed
        if good_rois(this_neuron)==1
            sel_index= (max(responses_pos)-mean(responses_pos));%/mean(responses_pos_norm);
            tuning.(date).(area).(plane).select_index(this_neuron)=sel_index;
        else
            tuning.(date).(area).(plane).select_index(this_neuron)=0;
        end
    end  
    if sel_index_integral_test %sel index using areas under the curves (under pre and post-stim)
        if good_rois(this_neuron)==1
            sel_index= (max(responses_pos_area)-mean(responses_pos_area));%/max(responses_pos_area);
            tuning.(date).(area).(plane).select_index(this_neuron)=sel_index;
        else
            tuning.(date).(area).(plane).select_index(this_neuron)=0;
        end
    end  
    if do_kerrys_test %kerry's selectivity test: numb of stim passing t-test/tot numb of stim
        response_index=(sum(all_results_mari)/length(all_results_mari))*100;
        tuning.(date).(area).(plane).resp_index(this_neuron)=response_index;
    end
end


plotresp_allcomb = zeros(length(pos_values), length(speed_values));
for jj = 1:length(responses_allcomb)
    pos = combinations(1,jj);
    speed = combinations(2,jj);
    
    pos_index = find(~(pos_values-pos));
    speed_index = find(~(speed_values-speed));
    
    plotresp_allcomb(pos_index,speed_index) = responses_allcomb(jj);
end

if get_shape
    
    if good_rois(this_neuron)==1
        
        if plot_resp
            figure(1+fig_offset); clf
            %set(gcf,'Position',[5   230   757   335])
            subplot(1,2,1);
            imagesc(plotresp_allcomb');
            ylabel('speeds')
            set(gca,'yticklabel',{'','slow','fast'},'ytick',[0:1:length(speed_values)])
            %yticks([0:1:length(speed_values)])
            xlabel('positions (mm distance)')
            set(gca,'xticklabel',{'','ant','middle-ant','middle-post','post'},'xtick',[0:1:length(pos_values)])
            %xticks([0:1:length(pos_values)])
            title([fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)])
            
            subplot(1,2,2);hold on
            plot(responses_pos, '-k')
            for plot_value =1:length(responses_pos)
                if responses_pos(plot_value)==max(responses_pos)
                    plot(plot_value,responses_pos(plot_value), '*r') %plot the max response in red
                elseif responses_pos(plot_value)>=(max(responses_pos)/4)*3 && responses_pos(plot_value)< max(responses_pos)
                    plot(plot_value, responses_pos(plot_value), '*b') %plot in blue if within 75% of max response
                elseif responses_pos(plot_value)>=(max(responses_pos)/4)*2 && responses_pos(plot_value)< (max(responses_pos)/4)*3
                    plot(plot_value, responses_pos(plot_value), '*g') %plot in greem if within 50% of max response
                else
                    plot(plot_value, responses_pos(plot_value), '*k') %plot in black if lower than 50%
                end
            end
            
            %colormap
            xlabel('positions')
            ylabel('df/f')
            set(gca, 'xticklabel',{'','ant','middle-ant','middle-post','post'},'xtick',[0:1:length(pos_values)])
            %xticks([0:1:length(pos_values)])
            %xticklabels({'','ant','middle-ant','middle-post','post'})
        end
        
        if manual_selection
            %%% for manual selection of category %%%
            shape=input('Is this a monotonic neuron?'); % 1 for yes to one side, 2 for yes to the other, 3 for single peaked, 0 for undefined
            tuning.(date).(area).(plane).categories(this_neuron) = shape;
            %%% ends here %%%
        else
            %%%%  for automated selection of category: this works only if we have 4 stimuli!!!  %%%%%
            if responses_pos(1)==max(responses_pos) && responses_pos(2)>=(max(responses_pos)/4)*3 && responses_pos(3)<=(max(responses_pos)/2) && responses_pos(4)<=(max(responses_pos)/2)
                tuning.(date).(area).(plane).categories(this_neuron) = 1; % monotonic anterior
            elseif responses_pos(1)==max(responses_pos) && responses_pos(2)>=(max(responses_pos)/4)*3 && responses_pos(3)>=(max(responses_pos)/4)*3 && responses_pos(4)<=(max(responses_pos)/2)
                tuning.(date).(area).(plane).categories(this_neuron) = 1; % monotonic anterior
            elseif responses_pos(4)==max(responses_pos) && responses_pos(3)>=(max(responses_pos)/4)*3 && responses_pos(2)>=(max(responses_pos)/4)*3 && responses_pos(1)<=(max(responses_pos)/2)
                tuning.(date).(area).(plane).categories(this_neuron) = 2; % monotonic posterior
            elseif responses_pos(4)==max(responses_pos) && responses_pos(3)>=(max(responses_pos)/4)*3 && responses_pos(2)<=(max(responses_pos)/2) && responses_pos(1)<=(max(responses_pos)/2)
                tuning.(date).(area).(plane).categories(this_neuron) = 2; % monotonic posterior
            else
                tuning.(date).(area).(plane).categories(this_neuron) = 0; % unclassified responsive neurons
            end
            
            sort_responses=sort(responses_pos); %array with responses sorted from low to high
            if sort_responses(1)<=(max(responses_pos))/2 && sort_responses(2)<=(max(responses_pos))/2 && sort_responses(3)<=(max(responses_pos))/2
                tuning.(date).(area).(plane).categories(this_neuron) = 3; % peaked neurons
            end
            %%% ends here %%%%
        end
    else
        tuning.(date).(area).(plane).categories(this_neuron) = 4; % category for neurons that don't pass t-test
        
    end
end

tuning.(date).(area).(plane).pos_nospeed(this_neuron,:) = responses_pos;
tuning.(date).(area).(plane).pos_speedA(this_neuron,:) = (plotresp_allcomb(:,1))';
tuning.(date).(area).(plane).is_red(this_neuron) = imaging.(date).(area).(plane).is_red(this_neuron);
%tuning.(date).(area).(plane).pos_speedB(this_neuron,:) = (plotresp_allcomb(:,2))';

%pause

end
