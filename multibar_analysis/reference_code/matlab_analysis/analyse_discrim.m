function [ discrimination_trials,  discrimination_outcomes_cut] = analyse_discrim(discrimination_trials, discrimination_outcomes_cut, imaging, date, area, plane, this_area, this_plane, frate, corrected_traces, stats, plot_trials, plot_resp, trial_test, outcome_test, prestim_discrim, stim_discrim, prestim_outcomes, stim_outcomes, prestim_licks)

go_trials=sort([imaging.(date).(area).session_behaviour.correct imaging.(date).(area).session_behaviour.missed imaging.(date).(area).session_behaviour.too_soon]);
nogo_trials=sort([imaging.(date).(area).session_behaviour.false_positive imaging.(date).(area).session_behaviour.correct_rejection]);

id_nogotrials=zeros(1, length(nogo_trials)); %all nogo trials are generally identified as zero (see other ids below to identify trials outcomes within go trials.)
id_gotrials=ones(1, length(go_trials)); %all go trials are generally identified as one (see other ids below to identify trials outcomes within go trials.)

%make array stating at which times go and nogo trials occurred
go_trials=[go_trials; id_gotrials];
nogo_trials=[nogo_trials; id_nogotrials];

all_trials=sortrows([go_trials nogo_trials]')';


% IDs for each trial outcome
outcome_types = [1 2 3 4 5];
id_correct=ones(1,length(imaging.(date).(area).session_behaviour.correct)).*outcome_types(1);
id_missed=ones(1,length(imaging.(date).(area).session_behaviour.missed)).*outcome_types(2);
id_toosoon=ones(1,length(imaging.(date).(area).session_behaviour.too_soon)).*outcome_types(3);
id_false=ones(1,length(imaging.(date).(area).session_behaviour.false_positive)).*outcome_types(4);
id_reject=ones(1,length(imaging.(date).(area).session_behaviour.correct_rejection)).*outcome_types(5);
licks=imaging.(date).(area).session_behaviour.licks;

%make array stating at which times each outcome in a trial occurred
corect_outcome=[imaging.(date).(area).session_behaviour.correct; id_correct];
missed_outcome=[imaging.(date).(area).session_behaviour.missed; id_missed];
toosoon_outcome=[imaging.(date).(area).session_behaviour.too_soon; id_toosoon];
false_outcome=[imaging.(date).(area).session_behaviour.false_positive; id_false];
reject_outcome=[imaging.(date).(area).session_behaviour.correct_rejection; id_reject];

all_outcomes=sortrows([corect_outcome missed_outcome toosoon_outcome false_outcome reject_outcome]')';

%the time between when the motor arrives at the whiskers and when it starts going back (for plotting purposes)
stim_length =((imaging.(date).(area).session_behaviour.motor_back(1) - imaging.(date).(area).session_behaviour.motor_atWhisk(1))*1/frate)-0.6; %0.6 is the time taken by the linear stage to move back. Only once it's back, stepper motor goes back to initial position
trial_types=unique(all_trials(2,:));
%outcome_types=unique(all_outcomes(2,:));

% t-test based on mari's t-test.is 1 if neuron passes t test, 0 if not. The t-test is to check responsiveness of each roi to each outcome or trial type
good_rois_trials=zeros(length(corrected_traces(:,1)),1);
good_rois_outcomes=zeros(length(corrected_traces(:,1)),1);

anova_pass_pos_trial=[]; %cells passing anova for trial type
anova_pass_pos_outcome=[]; %cells passing anova for outcome type

min_length_outcomes=ones(5,1)*1000;

%from now on, analysis will be carried out separately, according to whether
%we're looking at responses to trial type or activity during different
%trial outcomes

%%%%% look for licks outside of stimulation period %%%
for ww = 2:(length(imaging.(date).(area).session_behaviour.motor_atWhisk)-2) % taking from the second to the third to last trial here!! to avoid issues with creating traces!
        ss = imaging.(date).(area).session_behaviour.motor_atWhisk(ww); 
        oo=all_outcomes(1,ww);

        prestim_licks = [(ss-floor(frate*prestim_discrim)) (ss-floor(frate*0.2))]; %the window of ITI where to look for licks: from start of prestim wndow to about 3 frames before stim onset
        find_licks_interval = licks(licks>prestim_licks(1) & licks<prestim_licks(2));
        discrimination_outcomes_cut.(date).(area).(plane).all_trials_licks_outcome=[discrimination_outcomes_cut.(date).(area).(plane).all_trials_licks_outcome all_outcomes(2,ww)];
        if  isempty(find_licks_interval)
        discrimination_outcomes_cut.(date).(area).(plane).all_trials_licks=[discrimination_outcomes_cut.(date).(area).(plane).all_trials_licks 0];
        else
        discrimination_outcomes_cut.(date).(area).(plane).all_trials_licks=[discrimination_outcomes_cut.(date).(area).(plane).all_trials_licks 1]; % there are licks during prestim period. Take this trial for comparison  
        end
end
%%%%%%

for this_neuron = 1:length(corrected_traces(:,1))
    data_thisneuron_trial= cell(length(trial_types),1); %for each neuron a cell [#of pos x speed combinations by 1]. Each of these contains a matrix [# trials for that position by #frames in the trial (includes pre and post stim)]
    data_thisneuron_outcome= cell(length(outcome_types),1);
    data_thisneuron_outcome_cut= cell(length(outcome_types),1);
    

    for kk = 2:(length(imaging.(date).(area).session_behaviour.motor_atWhisk)-2) % taking from the second to the third to last trial here!! to avoid issues with creating traces!
        ss = imaging.(date).(area).session_behaviour.motor_atWhisk(kk); 
        oo=all_outcomes(1,kk);
        this_trial = corrected_traces(this_neuron,ss-floor(frate*prestim_discrim):ss+floor(frate*stim_discrim));
        %this_trial_toplot = corrected_traces(this_neuron,ss-floor(frate*prestim_sec):ss+floor(frate*(stim_sec*2)));%use this to plot as many frames before and after stim you want to plot, regardless of how long a trial is
       
        %  use the two below alternatively, based on whether you want to analyse outcome activity or just plot it.
        this_outcome_cut = corrected_traces(this_neuron,ss-floor(frate*prestim_discrim):oo); %this is to analyse activity from before stimulus to till outcome comes
        this_outcome = corrected_traces(this_neuron,ss-floor(frate*prestim_outcomes):ss+floor(frate*stim_outcomes)); %this avoids cutting trials: to use for plotting and for analysing outcomes till the end of licking window
        length_outcomeTrials=length(ss-floor(frate*prestim_outcomes):ss+floor(frate*stim_outcomes));
        
        this_trialtype = all_trials(2,kk); %go through go and nogo trials
        this_outcometype = all_outcomes(2,kk); %go through correct, miss, false, reject, toosoon trials
        
        for index_trial = 1:length(trial_types)
            if isequal(this_trialtype',trial_types(:,index_trial))
                break
            end
        end
        
        
        for index_outcome = 1:length(outcome_types)
            if isequal(this_outcometype',outcome_types(:,index_outcome))
                break
            end
        end
        
        data_thisneuron_trial{index_trial} = [data_thisneuron_trial{index_trial}; this_trial]; 
        data_thisneuron_outcome{index_outcome} = [data_thisneuron_outcome{index_outcome}; this_outcome];
        
        min_length_outcomes(this_outcometype)=min([min_length_outcomes(this_outcometype), length(this_outcome_cut)]);       
        if ~isempty(data_thisneuron_outcome_cut{index_outcome})
            data_thisneuron_outcome_cut{index_outcome}(:,min_length_outcomes(this_outcometype)+1:end) = [];
        end
        data_thisneuron_outcome_cut{index_outcome} = [data_thisneuron_outcome_cut{index_outcome}; this_outcome_cut(1:min_length_outcomes(this_outcometype))];
                
    end
    
    discrimination_trials = do_plot_stats_trials(imaging, date, area, plane, this_area, this_plane, this_neuron, data_thisneuron_trial, discrimination_trials, frate, prestim_discrim, stim_discrim, stim_length, all_trials, trial_types, good_rois_trials, stats, plot_trials, 0, 'Responses in trial type', plot_resp, trial_test, anova_pass_pos_trial) ;  %(this_neuron, plot_trials, stats, 200, 'All trials', mari_ttest, mmk_ttest, selectivity_test, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois);
    %discrimination_outcomes = do_plot_stats_outcomes(imaging, date, area, plane, this_area, this_plane, this_neuron, data_thisneuron_outcome, discrimination_outcomes, frate, prestim_discrim, stim_discrim, stim_length, all_outcomes, outcome_types, good_rois_outcomes, stats, plot_trials, 100, 'Responses in outcome type', plot_resp, outcome_test, anova_pass_pos_outcome, prestim_outcomes, stim_outcomes);   %(this_neuron, plot_trials, stats, 200, 'All trials', mari_ttest, mmk_ttest, selectivity_test, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois);
    discrimination_outcomes_cut = do_plot_stats_outcomes_cut_temp(imaging, date, area, plane, this_area, this_plane, this_neuron, data_thisneuron_outcome, data_thisneuron_outcome_cut, discrimination_outcomes_cut, frate, prestim_discrim, stim_discrim, stim_length, all_outcomes, outcome_types, good_rois_outcomes, stats, plot_trials, 100, 'Responses in outcome type', plot_resp, outcome_test, anova_pass_pos_outcome, prestim_outcomes, stim_outcomes, length_outcomeTrials);   %(this_neuron, plot_trials, stats, 200, 'All trials', mari_ttest, mmk_ttest, selectivity_test, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois);

    
end

end
