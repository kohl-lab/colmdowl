function [ tuning_outcomes ] = do_plot_stats_outcomes_cut(imaging, date, area, plane, this_area, this_plane, this_neuron, data_thisneuron_outcome, data_thisneuron_outcome_cut, tuning_outcomes, frate, prestim_discrim, stim_discrim, stim_length, all_outcomes, outcome_types, good_rois_outcomes, stats, plot_trials, fig_offset, fig_title_suffix, plot_resp, outcome_test, anova_pass_pos_outcome, prestim_outcomes, stim_outcomes, length_outcomeTrials)   %(this_neuron, plot_trials, stats, 200, 'All trials', mari_ttest, mmk_ttest, selectivity_test, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois);


%create cells of the right shape to be filled with responses from all trials, for each position/speed
test_prestim = cell(length(data_thisneuron_outcome),1);
test_stim = cell(length(data_thisneuron_outcome),1);
%avg_allcomb = [];
avg_allcomb = cell(length(data_thisneuron_outcome),1);

tuning_outcomes.(date).(area).(plane).data_outcomes{this_neuron}=data_thisneuron_outcome;%this


for jj = 1:length(data_thisneuron_outcome)
    if ~isempty (data_thisneuron_outcome{jj})
    avg_thiscomb = mean(data_thisneuron_outcome{jj},1);%mean response curve averaged across trials, for one pos x speed combination
    avg_thiscomb=avg_thiscomb';
    
    test_prestim{jj} = max(data_thisneuron_outcome{jj}(:,1:floor(frate*prestim_outcomes))'); %create this for stats tests
    test_stim{jj} = max(data_thisneuron_outcome{jj}(:,floor(frate*stim_outcomes):end)'); %create this for stats tests
    else %if there are no trials with the current outcome
       avg_thiscomb = zeros(length_outcomeTrials, 1); %make an average curve across trials of that outcome: all data points in this curve are zero, and it is just as long as
       avg_thiscomb = single(avg_thiscomb); %all arrays in cells are single, so this one should be too.
       test_prestim{jj} = [0.1 0 0 0 0 0]; % I will compare test_prestim and test_stim in a ttest below. If they are identical arrays (for axamples zeros arrays of the same length), ttest will return a NaN. I need them to be slightly different (but not different enough to have a positive ttest!)
       test_stim{jj} = [0.1 0.1 0 0 0 0];
    end
    %avg_allcomb = [avg_allcomb; avg_thiscomb];
    avg_allcomb{jj} = avg_thiscomb;
end

if plot_trials
    ccbin=[[0 0 1];[1 0 0]; [.6 .8 1]; [.9 .4 .17]; [0 1 0]]; %order of plotting is: correct, miss, too soon, false, reject. correct=blu, miss=red; too soon=lightblue, false pos = orange, corr rejection=green 
    avg_allcomb_toplot=cellfun(@transpose, avg_allcomb,'UniformOutput',false);
    figure(3+fig_offset);clf
    figure(6+fig_offset);clf
    

    
    for kk=1:length(data_thisneuron_outcome)
        figure(3+fig_offset)
        set(gcf, 'position', [360   108   713   263]);
        subplot(1,length(outcome_types),kk)
        currentFigure = gcf;
        title(currentFigure.Children(end), [fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)]);
        time= (1:1:size(avg_allcomb{kk},1)).*1./frate; time_toplot=mat2cell(time, 1, size(avg_allcomb{kk},1));%to plot time instead of frames on x axis make up a cell array, so that you can use cellfun
        %p = cellfun(@plot,(cellfun(@transpose,data_thisneuron_toplot(kk),'UniformOutput',false)), 'UniformOutput',false);
        if isempty(data_thisneuron_outcome{kk})
        p = cellfun(@plot,time_toplot, avg_allcomb(kk), 'UniformOutput',false);    
        else
        p = cellfun(@plot,time_toplot, (cellfun(@transpose,data_thisneuron_outcome(kk),'UniformOutput',false)), 'UniformOutput',false);
        end 
        set(gca,'ylim',[-1 6], 'xlim', [0 10], 'LineWidth', 2, 'FontSize', 14)
        %             ylabel('df/f')
        %             xlabel('time (s)')
        xticks ([0:1:10])
        box off
        if imaging.(date).(area).(plane).is_red(this_neuron)==0
            set(p{1:end},'Color',[.8 .8 .8])
        else
            set(p{1:end},'Color',[.65 1 .2])
        end
        
        hold on
        x1=[prestim_outcomes prestim_outcomes]; y1=ylim;
        x2=[prestim_outcomes+stim_length prestim_outcomes+stim_length]; y2=ylim;
        %plot(x1, y1, '-k')
        hold on
        %plot(x2, y2, '-k')
        y_plot=[y1, fliplr(y1)];
        x_plot=[x1, fliplr(x2)];
        
        h=fill(x_plot, y_plot, ccbin(kk,:));
        
        set(h,'facealpha',.3)
        set(h,'EdgeColor','none')
        
        hold on
        MeanSign_toplot=avg_allcomb_toplot{kk};
        plot(time, MeanSign_toplot,'Linewidth', 1.5, 'Color', [0 0 0])
        
    figure(6+fig_offset)
    set(gcf, 'position', [5   118   345   251]);
    hold on
    plot(time,MeanSign_toplot, 'Color', ccbin(kk,:))
    title([fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)])
    set(gca, 'ylim', [-1 5],  'LineWidth', 2, 'FontSize', 14)
    box off
    set(gcf, 'position', [19   378   345   251]);
    hold on
        x1=[prestim_outcomes prestim_outcomes]; y1=ylim;
        x2=[prestim_outcomes+stim_length prestim_outcomes+stim_length]; y2=ylim;
        %plot(x1, y1, '-k')
        hold on
        %plot(x2, y2, '-k')
        y_plot=[y1, fliplr(y1)];
        x_plot=[x1, fliplr(x2)];
        
        h=fill(x_plot, y_plot, [.5 .5 .5]);
        
        set(h,'facealpha',.1)
        set(h,'EdgeColor','none')
    end
    
    figure(5+fig_offset);clf
    set(gcf, 'position', [1087         110         690         264]);

    for kk=1:length(data_thisneuron_outcome)
        subplot(1,length(outcome_types),kk)
        currentFigure = gcf;
        title(currentFigure.Children(end), [fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)]);
        imagesc(cell2mat(data_thisneuron_outcome(kk)))
        set(gca,'LineWidth', 2, 'FontSize', 12)
    end
    
    
    

    pause
end

avg_allcomb = (cell2mat(avg_allcomb'))';
prestim = avg_allcomb(:,1:floor(frate*prestim_outcomes))';
stim = avg_allcomb(:,floor(frate*prestim_outcomes)+1:end)';
responses_allcomb = (max(stim)) - (max(prestim));

%normalise responses between 0 and 1
prestim_stim=[prestim; stim];
prestim_stim_norm=[];
for gg=1:length((prestim_stim(1,:)))
    normalised=(prestim_stim(:,gg)-min(prestim_stim(:,gg)))/(max(prestim_stim(:,gg))-min(prestim_stim(:,gg))); %traces for each stim are normalised between 0 and 1
    prestim_stim_norm=[normalised prestim_stim_norm];
end
prestim_norm=prestim_stim_norm(1:floor(frate*prestim_outcomes),:);
stim_norm = prestim_stim_norm((floor(frate*prestim_outcomes)+1:end),:);

if stats
        all_results=[];
        all_results_INHIB=[];
        for runtest=1:length(test_prestim)
            this_result_mari=ttest(test_stim{runtest}(1,:),test_prestim{runtest}(1,:),'alpha',0.05,'tail','right');
            this_result_mari_INHIB=ttest(test_stim{runtest}(1,:),test_prestim{runtest}(1,:),'alpha',0.05,'tail','left');
            all_results=[all_results this_result_mari];
            all_results_INHIB=[all_results_INHIB this_result_mari_INHIB];
        end
        if sum(all_results)>=1
            good_rois_outcomes(this_neuron)=1;
            tuning_outcomes.(date).(area).(plane).good_rois_outcomes(this_neuron)=1;
        else
            tuning_outcomes.(date).(area).(plane).good_rois_outcomes(this_neuron)=0;
        end
        
        if sum(all_results_INHIB)>=1
            inhib_rois_trials(this_neuron)=1;
            tuning_outcomes.(date).(area).(plane).inhib_rois_trials(this_neuron)=1;
        else
            tuning_outcomes.(date).(area).(plane).inhib_rois_trials(this_neuron)=0;
        end

    
    %%% check responses in outcomes %%%%
    max_pos_prestim = [];
    max_pos_stim = [];
    mean_pos_prestim = [];
    mean_pos_stim = [];
    area_pos_prestim = [];
    area_pos_stim = [];
    mean_prestim_norm = [];
    mean_stim_norm = [];
    
    for zz = 1:length(outcome_types)
        prestim_pos_temp = [];
        stim_pos_temp = [];
        prestim_pos_temp_norm = [];
        stim_pos_temp_norm = [];
        for xx = 1 %this is structured as in do_plot_stats, when we could use more than one speed. Now it shouldn't be a for loop, as our stim is always (2x1), but I decided to leave the same structure
            prestim_pos_temp = [prestim_pos_temp, prestim(:,zz+(xx-1)*length(outcome_types))];
            stim_pos_temp = [stim_pos_temp, stim(:,zz+(xx-1)*length(outcome_types))];
            prestim_pos_temp_norm = [prestim_pos_temp_norm, prestim_norm(:,zz+(xx-1)*length(outcome_types))];
            stim_pos_temp_norm = [stim_pos_temp_norm, stim_norm(:,zz+(xx-1)*length(outcome_types))];
        end
        mean_pos_stim = [mean_pos_stim mean(stim_pos_temp')];  %avg value of all poststim frames
        mean_pos_prestim = [mean_pos_prestim mean(prestim_pos_temp')];  %avg value of all prestim frames
        max_pos_prestim = [max_pos_prestim max(prestim_pos_temp')];
        max_pos_stim = [max_pos_stim max(stim_pos_temp')];
        
        area_pos_prestim = [area_pos_prestim 1/frate*(sum(prestim_pos_temp_norm))];
        area_pos_stim = [area_pos_stim   1/frate*(sum(stim_pos_temp_norm))];
        mean_prestim_norm = [mean_prestim_norm mean(prestim_pos_temp_norm)]; %mean value of normalised avg response curves
        mean_stim_norm = [mean_stim_norm mean(stim_pos_temp_norm)];
    end
    
    if length(mean_pos_stim)>length(mean_pos_prestim)
        responses_pos = mean_pos_stim (1:length(mean_pos_prestim)) - mean_pos_prestim;
    elseif length(mean_pos_prestim)>length(mean_pos_stim)
        responses_pos = mean_pos_stim  - mean_pos_prestim(1:length(mean_pos_stim));
    else
        responses_pos = mean_pos_stim  - mean_pos_prestim;
    end
    
    %%% these two lines take the non normalised max or mean responses (their number equals the number of positions) and normalises them only now
    %responses_pos_norm=responses_pos./max(responses_pos); %normalised to the maximum
    responses_pos_norm=(responses_pos-min(responses_pos))/(max(responses_pos)-min(responses_pos)); %normalised betwen 0 and 1
    %%%% ends here %%%%
    
    %%%% here we use the normalised average response curves, for each of which we extracted the mean value above. We now produce our 2 responses according to them.
    responses_pos_norm_mean = mean_stim_norm - mean_prestim_norm;
    %%%
    
    %%% here instead we used the normalised average curves created to calculate the areas under the curve for prestim and post stim. We subtract the two areas and try the selectivity index on this value
    responses_pos_area=area_pos_stim - area_pos_prestim;
    %%%
    
    %find index of best positions
    if good_rois_outcomes(this_neuron)==1
        [trash,outcome_trial] =max(responses_pos); %find index of best position
        tuning_outcomes.(date).(area).(plane).best_outcomes(this_neuron)=outcome_trial;
    else
        tuning_outcomes.(date).(area).(plane).best_outcomes(this_neuron)=0;
    end
    %%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%% here we can run tests to check for selectivity across outcomes. But this is not necessary. Taking the favourite outcome as the one to which the response is highest will be enough %%%%%%%%%%%%%%%%
    if outcome_test==1
        %%%%% ANOVA TEST for effect of speed and position %%%%%% %%%%%%
        anova_responses = cellfun(@minus,test_stim,test_prestim, 'UniformOutput',false);
        anova_means = (cellfun(@mean,anova_responses))';
        outcome_factor=outcome_types(1,:); %the only factor is the trial type

        
        all_pvalues = anovan(anova_means,{outcome_factor},'display','off');%,'model','interaction','display','off','varnames',{'position'; 'speed'});

        pos_tuned=all_pvalues(1)<0.05; %check if this neuron is sensitive to position
        if good_rois_outcomes(this_neuron)==1
            anova_pass_pos_outcome=[anova_pass_pos_outcome pos_tuned];
            tuning_outcomes.(date).(area).(plane).anova_outcomes(this_neuron)=pos_tuned;
        else
            anova_pass_pos_outcome=[anova_pass_pos_outcome 0];
            tuning_outcomes.(date).(area).(plane).anova_outcomes(this_neuron)=0;
        end
       
    end
end

tuning_outcomes.(date).(area).(plane).pos_nospeed(this_neuron,:) = responses_pos;
tuning_outcomes.(date).(area).(plane).is_red(this_neuron) = imaging.(date).(area).(plane).is_red(this_neuron);

%pause

end
