function [ tuning_trials ] = do_plot_stats_trials(imaging, date, area, plane, this_area, this_plane, this_neuron, data_thisneuron_trial, tuning_trials, frate, prestim_discrim, stim_discrim, stim_length, all_trials, trial_types, good_rois_trials, stats, plot_trials, fig_offset, fig_title_suffix, plot_resp, trial_test, anova_pass_pos_trial)   %(this_neuron, plot_trials, stats, 200, 'All trials', mari_ttest, mmk_ttest, selectivity_test, plot_resp, get_shape, manual_selection, pos_values, speed_values, good_rois);


%create cells of the right shape to be filled with responses from all trials, for each position/speed
test_prestim = cell(length(data_thisneuron_trial),1);
test_stim = cell(length(data_thisneuron_trial),1);
avg_allcomb = [];

tuning_trials.(date).(area).(plane).data_trials{this_neuron}=data_thisneuron_trial;%this


for jj = 1:length(data_thisneuron_trial)
    avg_thiscomb = mean(data_thisneuron_trial{jj},1);%mean response curve averaged across trials, for one pos x speed combination
    avg_allcomb = [avg_allcomb; avg_thiscomb];
    
    test_prestim{jj} = max(data_thisneuron_trial{jj}(:,1:floor(frate*prestim_discrim))'); %create this for stats tests
    test_stim{jj} = max(data_thisneuron_trial{jj}(:,floor(frate*stim_discrim):end)'); %create this for stats tests

end

if plot_trials
    ccbin=[[0 1 0];[1 0 1]];
    avg_allcomb_toplot=avg_allcomb';
    figure(3+fig_offset);clf
    set(gcf, 'position', [374   484   616   258]);

    
    for kk=1:length(data_thisneuron_trial)
        subplot(1,length(trial_types),kk)
        currentFigure = gcf;
        title(currentFigure.Children(end), [fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)]);
        time= (1:1:size(data_thisneuron_trial{kk},2)).*1./frate; time_toplot=mat2cell(time, 1, size(data_thisneuron_trial{kk},2));%to plot time instead of frames on x axis make up a cell array, so that you can use cellfun
        %p = cellfun(@plot,(cellfun(@transpose,data_thisneuron_toplot(kk),'UniformOutput',false)), 'UniformOutput',false);
        p = cellfun(@plot,time_toplot, (cellfun(@transpose,data_thisneuron_trial(kk),'UniformOutput',false)), 'UniformOutput',false);
        set(gca,'ylim',[-1 6], 'xlim', [0 10], 'LineWidth', 2, 'FontSize', 14)
        %             ylabel('df/f')
        %             xlabel('time (s)')
        xticks ([0:1:10])
        box off
        if imaging.(date).(area).(plane).is_red(this_neuron)==0
            set(p{1:end},'Color',[.8 .8 .8])
        else
            set(p{1:end},'Color',[.65 1 .2])
        end
        
        hold on
        x1=[prestim_discrim prestim_discrim]; y1=ylim;
        x2=[prestim_discrim+stim_length prestim_discrim+stim_length]; y2=ylim;
        %plot(x1, y1, '-k')
        hold on
        %plot(x2, y2, '-k')
        y_plot=[y1, fliplr(y1)];
        x_plot=[x1, fliplr(x2)];
        
        h=fill(x_plot, y_plot, ccbin(kk,:));
        
        set(h,'facealpha',.3)
        set(h,'EdgeColor','none')
        
        hold on
        plot(time, avg_allcomb_toplot(:,kk),'Linewidth', 1.5, 'Color', [0 0 0])
    end
    
    figure(5+fig_offset);clf
    set(gcf, 'position', [1002         487         560         249]);

    for kk=1:length(data_thisneuron_trial)
        subplot(1,length(trial_types),kk)
        currentFigure = gcf;
        title(currentFigure.Children(end), [fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)]);
        imagesc(cell2mat(data_thisneuron_trial(kk)))
        set(gca,'LineWidth', 2, 'FontSize', 12)
    end
    
    
    figure(6+fig_offset)
    plot(time,avg_allcomb_toplot)
    title([fig_title_suffix ' neuron ' num2str(this_neuron) ' plane ' num2str(this_plane) ' area ' num2str(this_area)])
    set(gca, 'ylim', [-1 5],  'LineWidth', 2, 'FontSize', 14)
    box off
    set(gcf, 'position', [22   492   345   251]);

    
    %         figure(5+fig_offset);clf
    %         for kk=1:length(data_thisneuron_toplot)
    %             subplot(length(speed_values),length(pos_values),kk)
    %             %find_min= cellfun(@min, data_thisneuron_toplot(kk),'UniformOutput',false); %minimum value for each frame, across trials. It'll be the lower edge on the shaded curve in the plot
    %             find_min= cellfun(@(x) min(x,2),data_thisneuron_toplot(kk),'UniformOutput',false);%data_thisneuron_toplot(kk),'UniformOutput',false); %minimum value for each frame, across trials. It'll be the lower edge on the shaded curve in the plot
    %             find_min=(cell2mat(find_min));
    %             find_max= cellfun(@(x) max(x,2),data_thisneuron_toplot(kk),'UniformOutput',false);%data_thisneuron_toplot(kk),'UniformOutput',false); %minimum value for each frame, across trials. It'll be the lower edge on the shaded curve in the plot
    %             %find_max= cellfun(@max, data_thisneuron_toplot(kk),'UniformOutput',false);
    %             find_max=(cell2mat(find_max));
    %             XX=linspace(1,length(data_thisneuron_toplot{kk}),length(data_thisneuron_toplot{kk}));
    %             X_plot = [XX, fliplr(XX)];
    %             Y_plot = [find_min, fliplr(find_max)];
    %             h=fill(X_plot, Y_plot, 'g');
    %             set(h,'facealpha',.25)
    %             set(h,'EdgeColor','none')
    %             hold on
    %             plot(avg_allcomb_toplot(:,kk),'Linewidth', 1.5, 'Color', [0 0 0])
    %         end
    
    %pause %to pause after each neuron
end

prestim = avg_allcomb(:,1:floor(frate*prestim_discrim))';
stim = avg_allcomb(:,floor(frate*prestim_discrim)+1:end)';
responses_allcomb = (max(stim)) - (max(prestim));

%normalise responses between 0 and 1
prestim_stim=[prestim; stim];
prestim_stim_norm=[];
for gg=1:length((prestim_stim(1,:)))
    normalised=(prestim_stim(:,gg)-min(prestim_stim(:,gg)))/(max(prestim_stim(:,gg))-min(prestim_stim(:,gg))); %traces for each stim are normalised between 0 and 1
    prestim_stim_norm=[normalised prestim_stim_norm];
end
prestim_norm=prestim_stim_norm(1:floor(frate*prestim_discrim),:);
stim_norm = prestim_stim_norm((floor(frate*prestim_discrim)+1:end),:);

if stats
        all_results=[];
        all_results_INHIB=[];
        for runtest=1:length(test_prestim)
            this_result_mari=ttest(test_stim{runtest}(1,:),test_prestim{runtest}(1,:),'alpha',0.05,'tail','right');
            this_result_mari_INHIB=ttest(test_stim{runtest}(1,:),test_prestim{runtest}(1,:),'alpha',0.05,'tail','left');
            all_results=[all_results this_result_mari];
            all_results_INHIB=[all_results_INHIB this_result_mari_INHIB];
        end
        if sum(all_results)>=1
            good_rois_trials(this_neuron)=1;
            tuning_trials.(date).(area).(plane).good_rois_trials(this_neuron)=1;
        else
            tuning_trials.(date).(area).(plane).good_rois_trials(this_neuron)=0;
        end
        
        if sum(all_results_INHIB)>=1
            inhib_rois_trials(this_neuron)=1;
            tuning_trials.(date).(area).(plane).inhib_rois_trials(this_neuron)=1;
        else
            tuning_trials.(date).(area).(plane).inhib_rois_trials(this_neuron)=0;
        end

    
    %%% check responses to positions regardless of speed %%%%
    max_pos_prestim = [];
    max_pos_stim = [];
    mean_pos_prestim = [];
    mean_pos_stim = [];
    area_pos_prestim = [];
    area_pos_stim = [];
    mean_prestim_norm = [];
    mean_stim_norm = [];
    
    for zz = 1:length(trial_types)
        prestim_pos_temp = [];
        stim_pos_temp = [];
        prestim_pos_temp_norm = [];
        stim_pos_temp_norm = [];
        for xx = 1 %this is structured as in do_plot_stats, when we could use more than one speed. Now it shouldn't be a for loop, as our stim is always (2x1), but I decided to leave the same structure
            prestim_pos_temp = [prestim_pos_temp, prestim(:,zz+(xx-1)*length(trial_types))];
            stim_pos_temp = [stim_pos_temp, stim(:,zz+(xx-1)*length(trial_types))];
            prestim_pos_temp_norm = [prestim_pos_temp_norm, prestim_norm(:,zz+(xx-1)*length(trial_types))];
            stim_pos_temp_norm = [stim_pos_temp_norm, stim_norm(:,zz+(xx-1)*length(trial_types))];
        end
        mean_pos_stim = [mean_pos_stim mean(stim_pos_temp')];  %avg value of all poststim frames
        mean_pos_prestim = [mean_pos_prestim mean(prestim_pos_temp')];  %avg value of all prestim frames
        max_pos_prestim = [max_pos_prestim max(prestim_pos_temp')];
        max_pos_stim = [max_pos_stim max(stim_pos_temp')];
        
        area_pos_prestim = [area_pos_prestim 1/frate*(sum(prestim_pos_temp_norm))];
        area_pos_stim = [area_pos_stim   1/frate*(sum(stim_pos_temp_norm))];
        mean_prestim_norm = [mean_prestim_norm mean(prestim_pos_temp_norm)]; %mean value of normalised avg response curves
        mean_stim_norm = [mean_stim_norm mean(stim_pos_temp_norm)];
    end
    
    if length(mean_pos_stim)>length(mean_pos_prestim)
        responses_pos = mean_pos_stim (1:length(mean_pos_prestim)) - mean_pos_prestim;
    elseif length(mean_pos_prestim)>length(mean_pos_stim)
        responses_pos = mean_pos_stim  - mean_pos_prestim(1:length(mean_pos_stim));
    else
        responses_pos = mean_pos_stim  - mean_pos_prestim;
    end
    
    %%% these two lines take the non normalised max or mean responses (their number equals the number of positions) and normalises them only now
    %responses_pos_norm=responses_pos./max(responses_pos); %normalised to the maximum
    responses_pos_norm=(responses_pos-min(responses_pos))/(max(responses_pos)-min(responses_pos)); %normalised betwen 0 and 1
    %%%% ends here %%%%
    
    %%%% here we use the normalised average response curves, for each of which we extracted the mean value above. We now produce our 2 responses according to them.
    responses_pos_norm_mean = mean_stim_norm - mean_prestim_norm;
    %%%
    
    %%% here instead we used the normalised average curves created to calculate the areas under the curve for prestim and post stim. We subtract the two areas and try the selectivity index on this value
    responses_pos_area=area_pos_stim - area_pos_prestim;
    %%%
    
    %find index of best positions (go or nogo)
    if good_rois_trials(this_neuron)==1
        [trash,index_trial] =max(responses_pos); %find index of best position (go or nogo)
        tuning_trials.(date).(area).(plane).best_trials(this_neuron)=index_trial;
    else
        tuning_trials.(date).(area).(plane).best_trials(this_neuron)=0;
    end
    %%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%% start tests to check for neuronasl sensitivity to stimulus %%%%%%%%%%%%%%%%
    if trial_test==1
        %%%%% ANOVA TEST for effect of speed and position %%%%%% %%%%%%
        anova_responses = cellfun(@minus,test_stim,test_prestim, 'UniformOutput',false);
        anova_means = (cellfun(@mean,anova_responses))';
        trial_factor=trial_types(1,:); %the only factor is the trial type

        
        all_pvalues = anovan(anova_means,{trial_factor},'display','off');%,'model','interaction','display','off','varnames',{'position'; 'speed'});

        pos_tuned=all_pvalues(1)<0.05; %check if this neuron is sensitive to position
        if good_rois_trials(this_neuron)==1
            anova_pass_pos_trial=[anova_pass_pos_trial pos_tuned];
            tuning_trials.(date).(area).(plane).anova_trials(this_neuron)=pos_tuned;
        else
            anova_pass_pos_trial=[anova_pass_pos_trial 0];
            tuning_trials.(date).(area).(plane).anova_trials(this_neuron)=0;
        end
       
    end
end

tuning_trials.(date).(area).(plane).pos_nospeed(this_neuron,:) = responses_pos;
tuning_trials.(date).(area).(plane).is_red(this_neuron) = imaging.(date).(area).(plane).is_red(this_neuron);

%pause

end
