'''This script is for comparing trends in MI scores over multiple testing blocks. In order to do this I have converted
the code from colmdowl.py into a funciton which returns only the pandas dataframe of MI scores. We can then run this
function multiple times and compare scores.'''

import sys
import os
import copy
import sklearn
import gcmi
import warnings
import matplotlib
import warnings

import numpy as np
import tensorflow as tf
import tensortools as tt
import pandas as pd
import seaborn as sns

sys.path.extend(['.', '../'])
matplotlib.use('Qt5Agg')
from analysis import sessarea
from mult_util import mult_gen_util
from util import math_util
from scipy.stats import pointbiserialr
from collections import Counter
from matplotlib import pyplot as plt
from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


#We will assaign our path for our imaging data and mouse data
datadir = os.path.join(
    'C:/University_Work/colmdowling/multibar_analysis/data')
mouse_df = 'C:/University_Work/colmdowling/multibar_analysis/mouse_df_new.csv'

def return_more_than_n(list, n):
    '''Returns all numbers in a list which occur more than n times'''
    counts = Counter(list)
    out_list = [i for i in counts if counts[i]>n]
    return out_list

def run_gcmi(X, Y, n, rank):
    '''Checks that there are at least n occurences of each unique value in a list X. If each value occurs at least
    n-times, the output will be a list of MI scores for each trial factor and X. The output will be a list of 0s if
    there are less than n occurances.
    Required args:
            - X                 : 1D list of binary integers, representing the categorical (feature) variable.
            - Y                 : Array of trial factors from a TCA ouput.
            - n                 : The minimum number of unique values of X needed to calculate MI.
            - rank              : The number of principle components you wish to calculate'''
    output = []
    n = n-1
    ranklen = [i for i in range(1,rank+1)]
    if len(return_more_than_n(X, n)) < 2:
        for p in ranklen:
            output.append(0)
    elif len(return_more_than_n(X, n)) >= 2:
        for p in ranklen:
            mi = gcmi.gcmi_model_cd(Y[p - 1], X, 2)
            output.append(mi)
    return output


'''
def get_neuron_fac_greater_than(mouse_n, sess_n, area, plane_n, pre=1, post=1.5, rank=16, limit=0.4):
    # INITIALISING SESSAREA OBJECT
    # This defines our sessionarea object and assaigns it to the variable "sess".
    sess = sessarea.SessArea(
        datadir, area, mouse_n=mouse_n, sess_n=sess_n, mouse_df=mouse_df)
    # We will use ranklen when creating for loops for all principle components.
    ranklen = [i for i in range(1, rank + 1)]

    # GENERATING 3RD ORDER TENSORS FOR A PLANE
    plane = sess.get_plane(plane_n)  # Assaigns the current plane to a plane object called "plane"
    sess.set_plane_oi(plane_oi=plane_n)  # This just allows us to reference the plane. It doesn't change the code.
    # This will get all of corrected 2p data (dF/F) and convert it to a 3rd order tensor
    trial_ns = sess.get_trials_by_criteria()
    twop_ref_fr = plane.get_twop_fr_by_trial(trial_ns, first=True)
    frame, data = plane.get_roi_trace_array(twop_ref_fr, pre=pre,
                                            post=post, trace_type="corr")
    tensordata = tf.convert_to_tensor(data)
    # NB: Axis0 = ROI, Axis1 = Trials, Axis2 = Timepoints

    # TENSOR COMPOSITION ANALYSIS
    # We fit the model from 2 initial conditions to ensure the initial guess doesn't have too much weight on the solution.
    # If the models have a poor similarity we can change the random_state seed variable to generate new models.
    tca1 = tt.cp_als(tensordata, rank=rank, random_state=1, max_iter=500, verbose=False)
    tca2 = tt.cp_als(tensordata, rank=rank, random_state=2, max_iter=500, verbose=False)
    # Align the two fits and print a similarity score.
    sim = tt.kruskal_align(tca1.factors, tca2.factors, permute_U=True, permute_V=True)
    if sim < 0.8:
        warnings.warn("Similarity score < 0.8.")

    # Extract the factors and assaign them so we can plot them individually
    neuron_fac = tca1.factors[0].T
    trial_fac = tca1.factors[1].T
    time_fac = tca1.factors[2].T

    # MUTUAL INFO + STIM POSITION
    stim_pos = sess.trial_df['stim_pos']
    stim_pos_int = [0 if position == 70 else "yellow" for position in stim_pos]
    mi_stim_pos = run_gcmi(stim_pos_int, trial_fac, 2, rank)

    mi_df = pd.DataFrame({
        'PC': ranklen,                                      # Principle component n (discrete)
        'stim_pos': mi_stim_pos,                            # Whether it was a GO or NO-GO trial (discrete)
    })
    neuron_fac_total = np.mean(neuron_fac, axis=1)

#    for p in ranklen:
#        mi_df.append(neuron_fac[p-1])
    return neuron_fac_total

print("Neuron fac")
get_neuron_fac_greater_than(1, 7, 2, 1, pre=1, post=1.5, rank=16, limit=0.4)
'''


def run_tca_get_mi(mouse_n, sess_n, area, plane_n, pre=1, post=1.5, rank=16):
    '''Runs a tensor component analysis on a sessarea object. Then calculates mutual information between each
    trial factor and a series of features.
    Required args:
            - mouse_n           : 1D list of binary integers, representing the categorical (feature) variable.
            - sess_n            : Array of trial factors from a TCA ouput.
            - area              : The minimum number of unique values of X needed to calculate MI.
            - plane             : The number of principle components you wish to calculate
            - pre               : the prestimulus time period to use (seconds)
            - post              : the post-stimulus time period to use (seconds)
            - rank              : the number of principle components to output in the TCA
    Features:
            - lick_t            : if the mouse licked or not during the "lick" phase of a trial.
            - corret_t          : if the trial was correct (correct hit, correct rej) or incorrect (too_soon, false_pos, missed)
            - stim_pos          : if the trial was a GO or NO-GO trial.
            - runspeed          : if the average runspeed for the pre/post time period was > 40
            - licks_per_t       : if the mouse licked during the pre/post time period.

    '''
    # INITIALISING SESSAREA OBJECT
    # This defines our sessionarea object and assaigns it to the variable "sess".
    sess = sessarea.SessArea(
        datadir, area, mouse_n=mouse_n, sess_n=sess_n, mouse_df=mouse_df)
    # We will use ranklen when creating for loops for all principle components.
    ranklen = [i for i in range(1, rank + 1)]


    # GENERATING 3RD ORDER TENSORS FOR A PLANE
    plane = sess.get_plane(plane_n)  # Assaigns the current plane to a plane object called "plane"
    sess.set_plane_oi(plane_oi=plane_n)  # This just allows us to reference the plane. It doesn't change the code.

    # This will get all of corrected 2p data (dF/F) and convert it to a 3rd order tensor
    trial_ns = sess.get_trials_by_criteria()
    twop_ref_fr = plane.get_twop_fr_by_trial(trial_ns, first=True)
    frame, data = plane.get_roi_trace_array(twop_ref_fr, pre=pre,
                                            post=post, trace_type="corr")
    tensordata = tf.convert_to_tensor(data)
    # NB: Axis0 = ROI, Axis1 = Trials, Axis2 = Timepoints

    # TENSOR COMPOSITION ANALYSIS
    # We fit the model from 2 initial conditions to ensure the initial guess doesn't have too much weight on the solution.
    # If the models have a poor similarity we can change the random_state seed variable to generate new models.
    tca1 = tt.cp_als(tensordata, rank=rank, random_state=1, max_iter=500, verbose=False)
    tca2 = tt.cp_als(tensordata, rank=rank, random_state=2, max_iter=500, verbose=False)

    # Align the two fits and print a similarity score.
    sim = tt.kruskal_align(tca1.factors, tca2.factors, permute_U=True, permute_V=True)
    if sim < 0.7:
        warnings.warn("Similarity score < 0.7.")
    # Extract the factors and assaign them so we can plot them individually
    trial_fac = tca1.factors[1].T

    # EXTRACTING TRIAL AND BEHAVIOURAL DATA
    stim_pos = sess.trial_df['stim_pos']
    stim_pos_color = ["green" if position == 70 else "yellow" for position in stim_pos]

    # TRIAL TYPE
    trial_outcome = sess.all_trial_outc
    print(f"mouse_n: {mouse_n}, sess_n: {sess_n}, area: {area}, plane = {sess.plane_oi}")
    lick_vs_nolick = ["lick" if outcome == "corr_acc" or
                                outcome == "false_pos" or
                                outcome == "too_soon" else
                                "no lick" for outcome in trial_outcome]

    corr_vs_incorr = ["correct" if outcome == "corr_acc" or
                                outcome == "corr_rej" else
                                "incorrect" for outcome in trial_outcome]

    # RUNNING SPEED
    #Returns a list of trial numbers (1,2,3,4...n)
    trial_ns = sess.get_trials_by_criteria()
    #Returns a list of arrays containing the run frame numbers that correspond to the trial numbers
    twop_ref_fr = plane.get_twop_fr_by_trial(trial_ns, first=True)
    #Returns a frame of 2p numbers and a dataset containing the runspeed for each timepoint in each trial.
    runframe, rundata = plane.get_run_velocity_array(twop_ref_fr, pre=pre, post=post)
    #We will take the time-averaged runspeed for each trial
    run_mean_per_trial = np.mean(rundata, axis = 1)

    # LICKING
    #Returns a frame of 2p numbers and a dataset containing whether each timepoint of each trial contained a lick.
    lickframe, lickdata = plane.get_lick_array(twop_ref_fr, pre=pre, post=post)
    #We will take the average number of licks per frame for each trial
    lick_total_per_trial = np.sum(lickdata, axis = 1)

    # Mutual Information
    # It helps to calculate the discrete variables as numbers.
    stim_pos_int = [0 if value == "yellow" in stim_pos_color else 1 for value in stim_pos_color]
    corr_vs_incorr_int = [0 if value == "correct" in corr_vs_incorr else 1 for value in corr_vs_incorr]
    lick_vs_nolick_int = [0 if value == "lick" in lick_vs_nolick else 1 for value in lick_vs_nolick]
    run_vs_norun_int = [0 if value > 40 else 1 for value in run_mean_per_trial]
    lick_threshold_int = [0 if value > 0 else 1 for value in lick_total_per_trial]

    mi_lick_v_nolick = run_gcmi(lick_vs_nolick_int, trial_fac, 3, rank)
    mi_correct_vs_incorrect = run_gcmi(corr_vs_incorr_int, trial_fac, 3, rank)
    mi_stim_pos = run_gcmi(stim_pos_int, trial_fac, 3, rank)
    mi_runspeed_discrete = run_gcmi(run_vs_norun_int, trial_fac, 3, rank)
    mi_licks_per_t_discrete = run_gcmi(lick_threshold_int, trial_fac, 3, rank)

    mi_df = pd.DataFrame({
        'PC': ranklen,                                      # Principle component n (discrete)
        'lick_t': mi_lick_v_nolick,                         # Whether the mouse licked during the "lick" stage (discrete)
        'correct_t': mi_correct_vs_incorrect,               # Whether it was a correct trial or not (discrete)
        'stim_pos': mi_stim_pos,                            # Whether it was a GO or NO-GO trial (discrete)
        'runspeed (D)': mi_runspeed_discrete,
        'licks_per_t (D)': mi_licks_per_t_discrete
    })
    return mi_df


def get_sorted_stim_pos_mi(mouse_n, sess_n, area, plane_n, pre=1, post=1.5, rank=16):
    '''Returns an array of sorted MI scores (trial factor x stim_pos)'''
    data = run_tca_get_mi(mouse_n, sess_n, area, plane_n, pre=pre, post=post, rank=rank)
    output = sorted(data["stim_pos"])
    return output


def get_mean_highest_mi(x, y, n):
    '''Returns the average of the 'n' highest scores of column 'y' in an array 'x'.'''
    data = sorted(x[y])
    highest_n = data [-n:]
    output = np.mean(highest_n)
    return output


def run_tca_get_variance(mouse_n, sess_n, area, plane_n, pre=1, post=1.5, rank=5):
    '''Runs a tensor component analysis on a sessarea object. Then returns the _____ for each trial component.
    Required args:
            - mouse_n           : 1D list of binary integers, representing the categorical (feature) variable.
            - sess_n            : Array of trial factors from a TCA ouput.
            - area              : The minimum number of unique values of X needed to calculate MI.
            - plane             : The number of principle components you wish to calculate
            - pre               : the prestimulus time period to use (seconds)
            - post              : the post-stimulus time period to use (seconds)
            - rank              : the number of principle components to output in the TCA
    '''
    # INITIALISING SESSAREA OBJECT

    # This defines our sessionarea object and assaigns it to the variable "sess".
    sess = sessarea.SessArea(
        datadir, area, mouse_n=mouse_n, sess_n=sess_n, mouse_df=mouse_df)

    # We will use ranklen when creating for loops for all principle components.
    ranklen = [i for i in range(1, rank + 1)]


    # GENERATING 3RD ORDER TENSORS FOR A PLANE

    plane = sess.get_plane(plane_n)  # Assaigns the current plane to a plane object called "plane"
    sess.set_plane_oi(plane_oi=plane_n)  # This just allows us to reference the plane. It doesn't change the code.

    # This will get all of corrected 2p data (dF/F) and convert it to a 3rd order tensor
    trial_ns = sess.get_trials_by_criteria()
    twop_ref_fr = plane.get_twop_fr_by_trial(trial_ns, first=True)
    frame, data = plane.get_roi_trace_array(twop_ref_fr, pre=pre,
                                            post=post, trace_type="corr")
    tensordata = tf.convert_to_tensor(data)

    # NB: Axis0 = ROI, Axis1 = Trials, Axis2 = Timepoints

    # TENSOR COMPOSITION ANALYSIS

    # We fit the model from 2 initial conditions to ensure the initial guess doesn't have too much weight on the solution.
    # If the models have a poor similarity we can change the random_state seed variable to generate new models.
    tca1 = tt.cp_als(tensordata, rank=rank, random_state=1, max_iter=500, verbose=False)
    tca2 = tt.cp_als(tensordata, rank=rank, random_state=2, max_iter=500, verbose=False)

    # Align the two fits and print a similarity score.
    sim = tt.kruskal_align(tca1.factors, tca2.factors, permute_U=True, permute_V=True)
    if sim < 0.7:
        warnings.warn("Similarity score < 0.7.")

    # Extract the factors and assaign them so we can plot them individually
    neuron_fac = tca1.factors[0].T
    trial_fac = tca1.factors[1].T
    time_fac = tca1.factors[2].T

    return(trial_fac)




