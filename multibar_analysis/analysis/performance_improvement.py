import sys
import os
import copy
import sklearn
import gcmi
import warnings
import matplotlib
import warnings

import tca_functions as tcaf
import numpy as np
import tensorflow as tf
import tensortools as tt
import pandas as pd
import seaborn as sns

sys.path.extend(['.', '../'])
matplotlib.use('Qt5Agg')
from analysis import sessarea
from mult_util import mult_gen_util
from util import math_util
from scipy.stats import pointbiserialr
from collections import Counter
from matplotlib import pyplot as plt
from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


#We will assaign our path for our imaging data and mouse data
datadir = os.path.join(
    'C:/University_Work/colmdowling/multibar_analysis/data')
mouse_df = 'C:/University_Work/colmdowling/multibar_analysis/mouse_df_new.csv'


# Mouse 1
sessions = [i for i in range(1,28)]
areas = [i for i in range(1,4)]

def get_performance(mouse_n, sessions, areas):
    scores = []
    for p in sessions:
        for q in areas:
            try:
                sess = sessarea.SessArea(
                    datadir, q, mouse_n=mouse_n, sess_n=p, mouse_df=mouse_df)
                performance = sess.prop_corr
                scores.append(performance)
            except:
                continue
    return scores

m1 = get_performance(1, sessions, areas)
m2 = get_performance(2, sessions, areas)
m3 = get_performance(3, sessions, areas)
m4 = get_performance(4, sessions, areas)
m5 = get_performance(5, sessions, areas)

m1 = [i for i in m1 if i == i]
m2 = [i for i in m2 if i == i]
m3 = [i for i in m3 if i == i]
m4 = [i for i in m4 if i == i]
m5 = [i for i in m5 if i == i]


fig = plt.figure()
plt.title("Performance over time")
plt.xlabel("Testing session/area")
plt.ylabel("Proportion of correct responses")
plt.plot(m1)
plt.plot(m2)
plt.plot(m3)
plt.plot(m4)
plt.plot(m5)
plt.show()