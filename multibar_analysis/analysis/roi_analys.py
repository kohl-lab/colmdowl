"""
gen_analys.py

This script analyses ROI trace data from the MULTIBAR project 
(Kohl lab, Dr. Mariangela Panniello).

Authors: Colleen Gillon

Date: September, 2019

Note: this code uses python 3.7.

"""


import copy
import os
import multiprocessing

from joblib import Parallel, delayed
import numpy as np
import scipy.stats as st
import scipy.ndimage as sni

from util import file_util, gen_util, math_util
from mult_util import mult_gen_util, mult_ntuple_util, mult_str_util
from plot_fcts import roi_analysis_plots as roi_plots


#############################################
def run_deconvolved_traces(sessions, analysis, seed, sesspar, figpar):
    
    """
    run_deconvolved_traces(sessions, analysis, seed, sesspar, figpar)

    Plots denoised traces for a few ROIs for each session. 

    Saves results and parameters relevant to analysis in a dictionary.

    Required args:
        - sessions (list)  : list of SessArea objects
        - analysis (str)   : analysis type (e.g., 'd')
        - seed (int)       : seed value to use. (-1 treated as None)
        - sesspar (SessPar): named tuple containing session parameters
        - figpar (dict)    : dictionary containing figure parameters
    """

    sessstr_pr = mult_str_util.sess_par_str(
        sesspar.sess_n, sesspar.layer, sesspar.level, sesspar.task, 
        str_type='print')

    print(('\nPlotting denoised traces for specific ROIs for entire '
        'session.\n({}).').format(sessstr_pr))

    seed = gen_util.seed_all(seed, 'cpu', print_seed=False)

    figpar = copy.deepcopy(figpar)
    if figpar['save']['use_dt'] is None:
        figpar['save']['use_dt'] = gen_util.create_time_str()
    
    traces = dict()
    for sess in sessions:
        plane = sess.get_plane(sess.plane_oi)
        plane.denoise_plane()
        plot_n = np.min([plane.nrois, 4]) # get number of ROIs to plot
        roi_ns = np.random.choice(range(plane.nrois), plot_n, replace=False)
        traces['baseline']  = plane.baselines[roi_ns].tolist()
        traces['denoised']  = plane.denoised[roi_ns, :].tolist()
        traces['fluo_corr'] = plane.fluo_corr[roi_ns, :].tolist()
        traces['spikes']    = plane.fluo_corr[roi_ns, :].tolist()

        extrapar = {'analysis': analysis,
                    'roi_ns'  : roi_ns.tolist(),
                    }

        sess_info = mult_gen_util.get_sess_info(sess)

        info = {'sesspar'  : sesspar._asdict(),
                'extrapar' : extrapar,
                'sess_info': sess_info,
                'traces'   : traces,
                }

        fulldir, savename = roi_plots.plot_deconvolved_traces(
            figpar=figpar, **info)

        file_util.saveinfo(info, savename, fulldir, 'json')
    

#############################################
# def run_pca_traces(sessions, analysis, sesspar, figpar):
    
#     """
#     run_pca_traces(sessions, analysis, sesspar, figpar)

#     Plots denoised traces for a few ROIs for each session. 

#     Saves results and parameters relevant to analysis in a dictionary.

#     Required args:
#         - sessions (list)  : list of SessArea objects
#         - analysis (str)       : analysis type (e.g., 'p')
#         - sesspar (SessPar)    : named tuple containing session parameters
#         - figpar (dict)        : dictionary containing figure parameters
#     """

#     sessstr_pr = mult_str_util.sess_par_str(
#         sesspar.sess_n, sesspar.layer, sesspar.level, sesspar.task, 
#         str_type='print')

#     print(('\nPlotting denoised traces for specific ROIs for entire '
#         'session.\n({}).').format(sessstr_pr))

#     figpar = copy.deepcopy(figpar)
#     if figpar['save']['use_dt'] is None:
#         figpar['save']['use_dt'] = gen_util.create_time_str()
    
#     traces = dict()
#     for sess in sessions:
#         plane = sess.get_plane(sess.plane_oi)
#         plane.denoise_plane()
#         roi_ns = np.random.choice(range(plane.nrois), 4)
#         traces['baseline']  = plane.baselines[roi_ns].tolist()
#         traces['denoised']  = plane.denoised[:, roi_ns].tolist()
#         traces['fluo_corr'] = plane.fluo_corr[:, roi_ns].tolist()
#         traces['spikes']    = plane.fluo_corr[:, roi_ns].tolist()

#         extrapar = {'analysis': analysis,
#                     'roi_ns'  : roi_ns.tolist(),
#                     }

#         sess_info = mult_gen_util.get_sess_info(sess, plane.plane_n)

#         info = {'sesspar'  : sesspar._asdict(),
#                 'extrapar' : extrapar,
#                 'sess_info': sess_info,
#                 'traces'   : traces
#                 }

#         fulldir, savename = roi_plots.plot_deconvolved_traces(
#             figpar=figpar, **info)

#         file_util.saveinfo(info, savename, fulldir, 'json')
    

#############################################
def run_nmf_corr(sessions, analysis, sesspar, figpar):
    
    """
    run_nmf_corr(sessions, analysis, sesspar, figpar)

    Plots denoised traces for a few ROIs for each session. 

    Saves results and parameters relevant to analysis in a dictionary.

    Required args:
        - sessions (list)  : list of 2 SessArea objects
        - analysis (str)   : analysis type (e.g., 'c')
        - sesspar (SessPar): named tuple containing session parameters
        - figpar (dict)    : dictionary containing figure parameters
    """

    if sesspar.task == 'sens_stim':
        raise ValueError(
            'Cannot run analysis for sensory stimulation sessions, as the '
            'task is passive.')

    sessstr_pr = mult_str_util.sess_par_str(
        sesspar.sess_n, sesspar.layer, sesspar.level, sesspar.task, 
        str_type='print')

    print(('\nCalculating correlation between NMF components and events early '
        'and late in training.\n({}).').format(sessstr_pr))

    figpar = copy.deepcopy(figpar)
    if figpar['save']['use_dt'] is None:
        figpar['save']['use_dt'] = gen_util.create_time_str()
    
    print('_' * 30)
    for sesses in sessions:
        print('\nMouse {}, sess {} v {} ({}%, {}%):'.format(
            sesses[0].mouse_n, sesses[0].sess_n, sesses[1].sess_n, 
            int(sesses[0].prop_corr * 100), int(sesses[1].prop_corr * 100)))
        all_corrs = []
        for sess in sesses:
            plane = sess.get_plane(sess.plane_oi)
            plane.denoise_plane()
            plane.run_nmf('denoised', 5)
            corr_acc_trials = sess.get_trials_by_criteria(
                outcome='corr_acc', remconsec=False)
            events = {'corr_acc': plane.get_twop_fr_by_trial(
                corr_acc_trials, first=True),
                    #   'motor_start': sess.motor_start,
                    #   'run'        : sess.run,
                    #   'licks'      : sess.licks,
                    #   'water_on'   : sess.water_on,
                    #   'missed'     : sess.get_trial_idx('missed')
                      }

            corrs = dict()
            for key in events.keys():
                if key == 'run':
                    ev_data = events[key]
                else:
                    ev_data = np.zeros_like(
                        plane.nmf_transf[:, 0]).astype(float)
                    ev_data[events[key]] = 1
                    # smooth binary data
                    ev_data = sni.filters.gaussian_filter1d(ev_data, 5)
                for n in range(5):
                    if np.all(ev_data == 0):
                        corrs['{}_{}'.format(key, n)] = np.nan
                    else:
                        corrs['{}_{}'.format(key, n)] = st.pearsonr(
                            ev_data, plane.nmf_transf[:, n])[0]
                    
                    # from matplotlib import pyplot as plt
                    # if n == 0:
                    #     plt.plot(plane.nmf_transf[:, n])
                    #     plt.plot(ev_data)
                    #     plt.show()
            

            all_corrs.append(corrs)
        for key in events.keys():
            for n in range(5):
                comp_key = '{}_{}'.format(key, n)
                sess1 = all_corrs[0][comp_key]
                sess2 = all_corrs[1][comp_key]
                sign = ''
                if (sess2 - sess1) > 0.1:
                    sign = ' +'
                elif (sess1 - sess2) > 0.1:
                    sign = ' -'
                print('    C{}: {:.2f} v {:.2f}{}'.format(
                    n, sess1, sess2, sign))

        extrapar = {'analysis': analysis,
                    }

        sess_info = mult_gen_util.get_sess_info(sess)

        info = {'sesspar'  : sesspar._asdict(),
                'extrapar' : extrapar,
                'sess_info': sess_info,
                }

        # fulldir, savename = roi_plots.plot_nmf_corr(figpar=figpar, **info)

        # file_util.saveinfo(info, savename, fulldir, 'json')


#############################################
def run_sess_svms(plane, analyspar, trialpar, roipar, svmpar, outcomes, 
                  stim_poses, parallel=False):
    """
    run_sess_svms(sess, analyspar, trialpar, svmpar, outcomes, stim_poses)

    Required args:
        - plane (Plane)        : Plane object
        - analyspar (AnalysPar): named tuple containing analysis parameters
        - sesspar (SessPar)    : named tuple containing session parameters
        - trialpar (TrialPar)  : named tuple containing trial parameters
        - roipar (ROIPar)      : named tuple containing ROI parameters
        - svmpar (SVMPar)      : named tuple containing SVM parameters
        - outcomes (list)      : list of outcomes to include in each SVM target 
                                 group
        - stim_poses (list)    : list of stimulus positions to include in each 
                                 SVM target group

    Optional args:
        - parallel (bool) : if True, some of the analysis is parallelized across 
                            CPU cores 
                            default: False

    Returns:
        - scores (1D array): scores for each fold (None if cannot run SVM)
        - shuffs (2D array): statistics (me, err) for each shuffle, structured 
                             as shuff x stats (me, err) (None if cannot run SVM)
    """

    if len(outcomes) != len(stim_poses):
        raise ValueError('Must pass as many outcomes as stim_poses.')

    integ = False
    prepost = [[trialpar.pre, trialpar.post]]
    ch_fl = None
    if trialpar.diff:
        if trialpar.pre == 0 or trialpar.post == 0:
            raise ValueError('Cannot take weighted difference if pre or post '
                'is 0.')
        integ = True
        prepost = [[trialpar.pre, 0], [0, trialpar.post]]
        # check flanks as pre and post are split up!
        ch_fl = [trialpar.pre, trialpar.post]

    # get the data (ROI signal, stimulus, outcome) trial x fr x ROI
    full_input, full_output = [], []
    for i, (o, sp) in enumerate(zip(outcomes, stim_poses)):
        trials = plane.sessarea.get_trials_by_criteria(stim_pos=sp, outcome=o)
        twop_ref_fr = plane.get_twop_fr_by_trial(
            trials, first=True, ch_fl=ch_fl)
        # ROI x trials (x frames if not diff)
        data = []
        for pre, post in prepost:
            data.append(plane.get_roi_trace_array(
                twop_ref_fr, pre, post, trace_type=roipar.trace_type, 
                stats=analyspar.stats, integ=integ, 
                resp_rois=roipar.resp_rois)[1])
        if trialpar.diff:
            data = data[1].T/np.absolute(trialpar.post) - \
                data[0].T/np.absolute(trialpar.pre)
        else:
            data = data[0].transpose([1, 0, 2])
        n_seqs = data.shape[0]
        full_input.append(data.reshape([n_seqs, -1]))
        full_output.append(np.full(n_seqs, i, dtype=int))
    
    n_tr = [len(out) for out in full_output]
    if np.min(n_tr) < 5:
        return None, None
    
    full_input  = np.concatenate(full_input)
    full_output = np.concatenate(full_output)
    
    errors = [analyspar.error, None]
    n_jobs = gen_util.get_n_jobs(svmpar.n_shuff + 1, parallel)
    outs = []
    for n in range(svmpar.n_shuff + 1):
        outs.append(math_util.run_cv_svm(
            full_input, full_output, cv=svmpar.cv, shuffle=(n != 0), 
            stats=analyspar.stats, error=errors[bool(n)], 
            class_weight='balanced', n_jobs=n_jobs))
    scores = outs[0]
    shuffs = np.asarray(outs[1:])
    
    return scores, shuffs


#############################################
def run_svm_across_learn(sessions, analysis, analyspar, sesspar, trialpar, 
                         roipar, svmpar, figpar, parallel=False):
    
    """
    run_svm_across_learn(sessions, analysis, analyspar, sesspar, figpar)

    Plots SVM results on sessions across or pre and post learning. 

    Saves results and parameters relevant to analysis in a dictionary.

    NOTE: EVENTUALLY USE A SCORE ON A COMPLETELY HELD-OUT SET INSTEAD OF THE 
    CROSS-VAL SCORES?

    Required args:
        - sessions (list)      : list of SessArea objects
        - analysis (str)       : analysis type (e.g., 's')
        - analyspar (AnalysPar): named tuple containing analysis parameters
        - sesspar (SessPar)    : named tuple containing session parameters
        - trialpar (TrialPar)  : named tuple containing trial parameters
        - roipar (ROIPar)      : named tuple containing ROI parameters
        - svmpar (SVMPar)      : named tuple containing SVM parameters
        - figpar (dict)        : dictionary containing figure parameters
    
    Optional args:
        - parallel (bool): if True, some of the analysis is parallelized across 
                           CPU cores 
    """

    if svmpar.sub:
        raise NotImplementedError(
            'Analysis not yet implemented for subsampling.')

    if roipar.trace_type in ['nmf', 'pca']:
        raise NotImplementedError(
            'Analysis not yet implemented for use with NMF or PCA traces.')

    diffstr = ''
    if trialpar.diff:
        if trialpar.post == 0 or trialpar.pre == 0:
            print('Setting trialpar to False as pre or post is 0.')
            trialpar = mult_ntuple_util.get_modif_ntuple(
                trialpar, 'diff', False)
        else:
            diffstr = ' (diff)'
    
    task = gen_util.list_if_not(sesspar.task)
    if task == ['pd_2']:
        task = 'pd_2'
        stimstr_pr = ' ({})'.format(mult_str_util.outc_par_str(
            svmpar.stim_cl, case='&', str_type='print')) 
        outstr_pr = ' and behaviour ({})'.format(
            mult_str_util.outc_par_str(svmpar.outc_cl, str_type='print'))
    elif task == ['sens_stim']:
        task = 'sens_stim'
        stimstr_pr, outstr_pr = '', ''
    else:
        raise ValueError('Analysis not implemented for task {}.'.format(
            ', '.join(task)))

    sessstr_pr = mult_str_util.sess_par_str(
        sesspar.sess_n, sesspar.layer, sesspar.level, task, str_type='print')
    
    prepoststr_pr = mult_str_util.prepost_par_str(
        trialpar.pre, trialpar.post, str_type='print')
    roistr_pr = mult_str_util.roi_par_str(
        roipar.trace_type, roipar.resp_rois, str_type='print')

    print(('\nCalculating stimulus{}{} decodability across '
        '{} sessions, {} from trial onset{} on {}.\n({}).').format(
            stimstr_pr, outstr_pr, task, prepoststr_pr, diffstr, roistr_pr, 
            sessstr_pr))

    figpar = copy.deepcopy(figpar)
    if figpar['save']['use_dt'] is None:
        figpar['save']['use_dt'] = gen_util.create_time_str()

    if task == 'pd_2':
        # datatypes: stimulus (e.g., go/nogo) and 
        # beh outcome (e.g., hit/miss) (retrieved later)
        outcomes = [
            [mult_gen_util.get_outcomes(svmpar.stim_cl)] * 2, 
            mult_gen_util.get_outcomes(svmpar.outc_cl)]
    elif task == 'sens_stim':   
        outcomes = [['', '']]

    sess_info = []
    n_mice = len(sessions)
    n_sess = len(sessions[0])
    n_data = len(outcomes)
    stats_len = 2 + (analyspar.stats == 'median' and analyspar.error == 'std')
    
    scores = np.full([n_mice, n_sess, n_data, stats_len], np.nan)
    shuffs = np.full([n_sess, n_data, n_mice * svmpar.n_shuff], np.nan)
    for m, sesses in enumerate(sessions):
        mouse_ns = list(set([str(s.mouse_n) for s in sesses if s is not None]))
        sess_ns  = [str(s.sess_n) for s in sesses if s is not None]
        print('\nMouse {}, sess {}'.format(
            ', '.join(mouse_ns), ', '.join(sess_ns)))
        for s, sess in enumerate(sesses):
            if sess is None:
                continue
            if task == 'sens_stim':
                # all possible positions: [35, 42, 49, 56, 63, 70]
                if svmpar.ctrl:
                    stim_poses = [[63, 42]]
                else:
                    stim_poses = [[70, 49]] # go, nogo
            else:
                stim_poses = [[sess.go_pos, sess.nogo_pos], ['any', 'any']]            
            plane = sess.get_plane(sess.plane_oi)
            plane.denoise_plane()
            for d, (outs, sps) in enumerate(zip(outcomes, stim_poses)):
                st, end = [svmpar.n_shuff * i for i in [m, m+1]]
                scs, shs = run_sess_svms(
                    plane, analyspar, trialpar, roipar, svmpar, outs, sps, 
                    parallel)
                if scs is not None and shs is not None:
                    scores[m, s, d]      = scs
                    shuffs[s, d, st:end] = shs
        # placing after so that responsive ROIs are already assessed
        sess_info.append(mult_gen_util.get_sess_info(sesses, roipar.resp_rois))
        

    perc = [5, 95, 50]
    shuff_extr = np.full([n_sess, n_data, len(perc)], np.nan)
    for p, perc in enumerate(perc):
        shuff_extr[:, :, p] = np.nanpercentile(shuffs, perc, axis=-1)

    scores = {'all_scores': scores.tolist(),
              'shuff_extr': shuff_extr.tolist()}

    extrapar = {'analysis' : analysis,
                }

    info = {'analyspar': analyspar._asdict(),
            'sesspar'  : sesspar._asdict(),
            'trialpar' : trialpar._asdict(),
            'roipar'   : roipar._asdict(),
            'svmpar'   : svmpar._asdict(),
            'extrapar' : extrapar,
            'sess_info': sess_info,
            'scores'   : scores
            }

    fulldir, savename = roi_plots.plot_svm_across_learn(figpar=figpar, **info)

    file_util.saveinfo(info, savename, fulldir, 'json')


#############################################
def run_sess_traces(sess, analyspar, trialpar, roipar, datatype='roi'):
    """
    run_sess_traces(sess, analyspar, trialpar, roipar)

    Returns trace statistics for a session for each stimulus type and outcome 
    type.
    
    Required args:
        - sess (SessArea)      : SessArea object
        - analyspar (AnalysPar): named tuple containing analysis parameters
        - trialpar (TrialPar)  : named tuple containing trial parameters
        - roipar (ROIPar)      : named tuple containing ROI parameters
    
    Optional args:
        - datatype (str)  : type of data (e.g., 'roi', 'run')
                            default: 'roi'

    Returns:
        - sess_traces (list): trace statistics (None if no trials fit criteria
                              and None if a sign has no responsive ROIs), 
                                 structured as 
                                    criteria x val (x sign) x 
                                    stats [me, err] x frames
        - sess_ns (list)    : number of sequences for each trace (None if no 
                              trials fit criteria), 
                                  structured as [stim_pos, outcomes] x vals
        - sess_vals (list)  : criteria value names (None if no trials fit 
                                  criteria), structured as criteria x val
        - rr_ns (list)      : number of responsive ROIs (val lists are empty if
                              resp_rois is False), structured as
                                  criteria x val x sign
        - xran (list)       : time values for the trace statistics
    """


    if trialpar.diff:
        raise NotImplementedError(
            'trialpar.diff does not apply to this analysis.')

    plane = sess.get_plane(sess.plane_oi)
    plane.denoise_plane()

    criteria = ['stim_pos', 'outcome']
    signs = [1, -1]

    sess_traces = [[] for _ in range(len(criteria))]
    sess_ns = [[] for _ in range(len(criteria))]
    sess_vals = [[] for _ in range(len(criteria))]
    rr_ns = [[] for _ in range(len(signs))]

    xran = None
    for c, cri in enumerate(criteria):
        set_none = False
        if cri == 'stim_pos':
            vals = sess.stim_pos
            if roipar.resp_rois:
                plane.set_resp_rois()
                if plane.get_n_resp_rois() == 0:
                    rr_ns[c].append([0, 0])
                    set_none = True
        elif cri == 'outcome':
            if sess.task == 'sens_stim':
                set_none = True
            else:
                vals = sess.trial_outc
                if roipar.resp_rois:
                    plane.set_outc_resp_rois()
                    if plane.get_n_resp_rois(trial_type='outcome') == 0:
                        rr_ns[c].append([0, 0])
                        set_none = True
        if set_none:
            sess_traces[c] = None
            sess_ns[c] = None
            sess_vals[c] = None
            continue

        for v in vals:
            if cri == 'stim_pos':
                trials = plane.sessarea.get_trials_by_criteria(stim_pos=v)
            elif cri == 'outcome':
                trials = plane.sessarea.get_trials_by_criteria(outcome=v)
            twop_ref_fr = plane.get_twop_fr_by_trial(trials, first=True)
            if not roipar.resp_rois:
                # xran, stats, arr
                all_data = plane.get_roi_trace_stats(
                    twop_ref_fr, trialpar.pre, trialpar.post, byroi=False, 
                    trace_type=roipar.trace_type, ret_arr=True, 
                    stats=analyspar.stats, error=analyspar.error, 
                    resp_rois=roipar.resp_rois)
                xran = all_data[0]
                sess_traces[c].append(all_data[1].tolist())
                sess_ns[c].append(all_data[2].shape[1])
            else: # get positive and negative responsive ROIs
                # xran, arr (ROI x seq x fr)
                xran, data = plane.get_roi_trace_array(
                    twop_ref_fr, trialpar.pre, trialpar.post, 
                    trace_type=roipar.trace_type)
                if cri == 'stim_pos':
                    resp_rois_ns = plane.resp_rois
                    resp_roi_sign = plane.resp_roi_sign
                    idx = sess.stim_pos.index(v)
                else:
                    resp_rois_ns = plane.outc_resp_rois
                    resp_roi_sign = plane.outc_resp_roi_sign
                    idx = sess.trial_outc.index(v)
                resp_roi_data = []
                sign_rr_ns = []
                for sign in signs:
                    keep_rois = np.where(
                        resp_rois_ns[:, idx] * resp_roi_sign[:, idx] == sign)
                    sign_rr_ns.append(len(keep_rois[0])) # add n_rois
                    if sign_rr_ns[-1] == 0:
                        resp_roi_data.append(None)
                    else:
                        resp_roi_data.append(
                            plane.get_array_stats(xran, 
                            data[keep_rois], axes=[1, 0], stats=analyspar.stats, 
                            error=analyspar.error)[1].tolist())
                sess_traces[c].append(resp_roi_data)
                sess_ns[c].append(data.shape[1])  
                rr_ns[c].append(sign_rr_ns)
            sess_vals[c].append(v)     

    return sess_traces, sess_ns, sess_vals, rr_ns, xran


#############################################
def run_traces_split(sessions, analysis, analyspar, sesspar, trialpar, roipar, 
                     figpar, datatype='roi', parallel=False):

    """
    run_traces_split(sessions, analysis, analyspar, sesspar, trialpar, roipar, 
                     figpar)

    Retrieves trace statistics by session and plots them for each stimulus type 
    and outcome type, if applicable, with each session in a separate subplot.
    
    Saves results and parameters relevant to analysis in a dictionary.

    Required args:
        - sessions (list)      : list of SessArea objects
        - analysis (str)       : analysis type (e.g., 't')
        - analyspar (AnalysPar): named tuple containing analysis parameters
        - sesspar (SessPar)    : named tuple containing session parameters
        - trialpar (TrialPar)  : named tuple containing trial parameters
        - ROIpar (ROIPar)      : named tuple containing ROI parameters
        - figpar (dict)        : dictionary containing figure parameters
    
    Optional args:
        - datatype (str) : type of data (e.g., 'roi', 'run')
                           default: 'roi'
        - parallel (bool): if True, some of the analysis is parallelized across 
                           CPU cores 

    """

    if trialpar.diff:
        print('Setting trialpar.diff to False for this analysis.')
        trialpar = mult_ntuple_util.get_modif_ntuple(trialpar, 'diff', False)


    laylev_pr = mult_str_util.laylev_par_str(
        sesspar.layer, sesspar.level, str_type='print')

    print('\nAnalysing and plotting stimulus ROI traces \n({}).'.format(
        laylev_pr))

    xran = None
    if parallel:
        n_jobs = gen_util.get_n_jobs(len(sessions))
        returns = Parallel(n_jobs=n_jobs)(delayed(run_sess_traces)
            (sess, analyspar, trialpar, roipar, datatype) for sess in sessions)
        [all_traces, all_ns, all_vals, all_rr_ns, xrans] = \
            [list(ret) for ret in zip(*returns)]
        for val in xrans:
            if val is not None:
                xran = val
    else:
        all_traces, all_ns, all_vals, all_rr_ns = [], [], [], []
        for sess in sessions:
            returns = run_sess_traces(
                sess, analyspar, trialpar, roipar, datatype)
            all_traces.append(returns[0])
            all_ns.append(returns[1])
            all_vals.append(returns[2])
            all_rr_ns.append(returns[3])
            if returns[-1] is not None:
                xran = returns[-1]

    # placing after so that responsive ROIs are already assessed
    sess_info = mult_gen_util.get_sess_info(sessions, roipar.resp_rois)
   
    trace_st = {'all_traces'  : all_traces,
                'all_ns'      : all_ns,
                'all_vals'    : all_vals,
                'xran'        : xran.tolist()
                }

    if roipar.resp_rois:
        trace_st['n_resp_rois'] = all_rr_ns # n resp ROIs per stim/sign
      
    extrapar = {'analysis'  : analysis,
                }

    info = {'analyspar': analyspar._asdict(),
            'sesspar'  : sesspar._asdict(),
            'trialpar' : trialpar._asdict(),
            'roipar'   : roipar._asdict(),
            'extrapar' : extrapar,
            'sess_info': sess_info,
            'tr_stats' : trace_st
            }

    fulldir, savename = roi_plots.plot_traces_split(figpar=figpar, **info)

    file_util.saveinfo(info, savename, fulldir, 'json')


#############################################
def run_peak_extract(sessions, analysis, analyspar, sesspar, trialpar, figpar, 
                       datatype='roi'):
    """
    run_peak_extract(sessions, analysis, analyspar, sesspar, stimpar, figpar)

    Retrieves peaks by session and plots them for each stimulus type 
    and outcome type, if applicable, with each session in a separate subplot.
    
    Saves results and parameters relevant to analysis in a dictionary.

    Required args:
        - sessions (list)      : list of Session objects
        - analysis (str)       : analysis type (e.g., 't')
        - analyspar (AnalysPar): named tuple containing analysis parameters
        - sesspar (SessPar)    : named tuple containing session parameters
        - trialpar (TrialPar)  : named tuple containing trial parameters
        - figpar (dict)        : dictionary containing figure parameters
    
    Optional args:
        - datatype (str): type of data (e.g., 'roi', 'run')
    """

    # laylev_pr = mult_str_util.laylev_par_str(sesspar.layer, sesspar.level, 
    #                                          str_type='print')

    # print(('\nAnalysing and plotting traces peaks '
    #        '\n({}).').format(laylev_pr))

    # resp_rois = True
    # trace_type = 'corr'

    # all_traces, all_ns, all_vals, all_rr_ns = [], [], [], []
    # xran = None
    # for sess in sessions:
    #     returns = run_sess_traces(sess, analyspar, trialpar, datatype, 
    #                               trace_type, resp_rois)
    #     all_traces.append(returns[0])
    #     all_ns.append(returns[1])
    #     all_vals.append(returns[2])
    #     all_rr_ns.append(returns[3])
    #     if returns[-1] is not None:
    #         xran = returns[-1]



# def scan_acc_time(all_data, labels, units="all", kernel="linear"):
#     """ Fits SVC on each time slice from datatensor
    
#     Args:
#     all_data : Expects datatensor with trials x time x unit. 
#     labels : The labels for training the classifier.
#     units : A list of indices of units to include in the analysis or "all" for all (default "all").
#     kernel : The kernel to use for the classifier.
    
#     Returns classification accuracy scores for 5-fold CV of a linear classifer over time.
#     """
#     if units=="all":
#         data_selection = all_data
#     else:
#         data_selection = all_data[:,:,units]
       
#     results = []
#     for t in range(data_selection.shape[1]):
#         partial_data = data_selection[:,t,:]
#         dataset = fuse_dataset(partial_data,labels)
#         clf = SVC(kernel=kernel, C=1)
#         scores = cross_val_score(clf, dataset[0], dataset[1], cv=5)
#         results.append(scores)
#     results = np.array(results)
#     return results



#     def get_acc_all_times(all_data, labels, units="all", kernel="linear"):
#     """ Fits SVC on each time slice from datatensor
    
#     Args:
#     all_data : Expects datatensor with trials x time x unit. 
#     labels : The labels for training the classifier.
#     units : A list of indices of units to include in the analysis or "all" for all (default "all").
#     kernel : The kernel to use for the classifier.
    
#     Returns classification accuracy scores for 5-fold CV of a linear classifer.
#     """
#     if units=="all":
#         data_selection = all_data
#     else:
#         data_selection = all_data[:,:,units]
       
#     dataset = fuse_dataset(data_selection,labels)
#     clf = SVC(kernel=kernel, C=1)
#     scores = cross_val_score(clf, dataset[0], dataset[1], cv=5)
#     return scores


#     def get_acc(dataset):
#     pred=svc.predict(dataset[0])
#     acc=(pred==dataset[1]).mean()
#     return acc

#     def fuse_dataset(data, labels, standardize=True):
#     d = data.reshape((data.shape[0],-1))
#     if standardize:
#         scaler = sklearn.preprocessing.Normalizer()
#         d = scaler.fit_transform(d)
#     return d,labels