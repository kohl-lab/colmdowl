import sys
import os
import copy
import sklearn
import gcmi
import warnings
import matplotlib
import warnings
import tca_functions as tcaf

import numpy as np
import tensorflow as tf
import tensortools as tt
import pandas as pd
import seaborn as sns

sys.path.extend(['.', '../'])
matplotlib.use('Qt5Agg')
from analysis import sessarea
from mult_util import mult_gen_util
from util import math_util
from scipy.stats import pointbiserialr
from collections import Counter
from matplotlib import pyplot as plt
from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


#We will assaign our path for our imaging data and mouse data
datadir = os.path.join(
    'C:/University_Work/colmdowling/multibar_analysis/data')
mouse_df = 'C:/University_Work/colmdowling/multibar_analysis/mouse_df_new.csv'


# Here's a list of areas that looked at DL3 and HSL2. Only pd2 trials where both planes were present are used.
sessions_m1 = [13, 14]                                              #CTBD2.5a
sessions_m2 = [12]                                                  #CTBD7.2a
sessions_m3 = [3, 7]                                                #CTBD7.2e
sessions_m4 = [2, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]           #CTBD7.6e
sessions_m5 = [2, 5, 10, 11, 12, 15, 16, 17, 18, 19, 20, 22]        #CTBD7.6f
sessions_m6 = [5, 10, 11, 12]                                       #CTBD11.3e
sessions_m7 = [3, 6, 12, 13, 14]                                    #CTBD11.3g
sessions_m8 = []                                                    #CTBD13.5c

'''
# Comparing hypsershallow L2 and deep L3 (area 2) for a specific feature
plane_n = 1
top_n = 1  # top n MI scores will be included
feature = "licks_per_t (D)"

# Mouse 1
# Sessions is a list of pd_2 sessions where both planes contained recordings and were not flagged to be excluded
sessions = sessions_m1
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(1, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=1, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)

m1 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})

# Mouse 2
sessions = sessions_m2
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(2, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=2, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)

m2 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})

# Mouse 3
sessions = sessions_m3
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(3, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=3, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)

m3 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})

# Mouse 4
sessions = sessions_m4
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(4, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=4, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)
m4 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})

# Mouse 5
sessions = sessions_m5
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(5, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=5, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)
m5 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})


# Mouse 6
sessions = sessions_m6
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(6, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=6, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)
m6 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})


# Mouse 7
sessions = sessions_m7
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(7, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=7, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)
m7 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})


# Mouse 8
sessions = sessions_m8
learn_cat = []
means = []
for p in sessions:
    data = tcaf.run_tca_get_mi(8, p, 2, plane_n)
    mean_highest_mi = tcaf.get_mean_highest_mi(data, feature, top_n)
    means.append(mean_highest_mi)
    sess = sessarea.SessArea(datadir, area=2, mouse_n=8, sess_n=p, mouse_df=mouse_df)
    prop_corr = sess.prop_corr
    if prop_corr < .55:
        learn_cat.append(1)
    elif prop_corr < .65:
        learn_cat.append(2)
    elif prop_corr < .75:
        learn_cat.append(3)
    elif prop_corr > .75:
        learn_cat.append(4)
m8 = pd.DataFrame({
    'Session': sessions,
    'learn_cat': learn_cat,
    'mean': means})



# We will find the average MI value for each learning catagory. This will be our y value in our plot
m1_learn = m1.groupby('learn_cat', as_index=False).mean()
m2_learn = m2.groupby('learn_cat', as_index=False).mean()
m3_learn = m3.groupby('learn_cat', as_index=False).mean()
m4_learn = m4.groupby('learn_cat', as_index=False).mean()
m5_learn = m5.groupby('learn_cat', as_index=False).mean()
m6_learn = m6.groupby('learn_cat', as_index=False).mean()
m7_learn = m7.groupby('learn_cat', as_index=False).mean()
m8_learn = m8.groupby('learn_cat', as_index=False).mean()

# We can plot the averages for each learning category
fig = plt.figure()
plt.title("Hypershallow L2 - Mutual information between trial factors and licks during trial")
plt.xticks(np.arange(1, 5, 1.0), ("Naieve", "2", "3", "Expert"))
plt.xlabel("Learning stage")
plt.ylabel("Mutual information")
sns.scatterplot(data=m1_learn, x="learn_cat", y="mean", markers=True, legend='full')
sns.lineplot(data=m1_learn, x="learn_cat", y="mean", markers=True)
sns.scatterplot(data=m2_learn, x="learn_cat", y="mean", markers=True)
sns.lineplot(data=m2_learn, x="learn_cat", y="mean", markers=True)
sns.scatterplot(data=m3_learn, x="learn_cat", y="mean", markers=True)
sns.lineplot(data=m3_learn, x="learn_cat", y="mean", markers=True)
sns.scatterplot(data=m4_learn, x="learn_cat", y="mean", markers=True)
sns.lineplot(data=m4_learn, x="learn_cat", y="mean", markers=True)
sns.scatterplot(data=m5_learn, x="learn_cat", y="mean", markers=True)
sns.lineplot(data=m5_learn, x="learn_cat", y="mean", markers=True)
sns.scatterplot(data=m6_learn, x="learn_cat", y="mean", markers=True)
sns.lineplot(data=m6_learn, x="learn_cat", y="mean", markers=True)
sns.scatterplot(data=m7_learn, x="learn_cat", y="mean", markers=True)
sns.lineplot(data=m7_learn, x="learn_cat", y="mean", markers=True)
sns.scatterplot(data=m8_learn, x="learn_cat", y="mean", markers=True)
sns.lineplot(data=m8_learn, x="learn_cat", y="mean", markers=True)
plt.show()

'''

########################################################################################################################

# PLOT ALL COMPONENTS MI SCORES AVERAGE
def plot_all_components_mean_highest_mi(feature, mouse_n, plane_n, sessions, pre=1, post=1.5):
    '''Plots all 16 components MI scores for a given feature. Averages scores within each learning stage:
    Required args:
            - feature           : The variable to calculate MI scores for (stim_pos, lick_t, lpt, ___)
            - mouse_n           : Mouse number
            - plane_n           : Plane of interest
            - sessions          : 1D list of sessions where both planes contained imaging data.
            - pre               : the prestimulus time period to use (seconds)
            - post              : the post-stimulus time period to use (seconds)
    Output:
            - x-axis            : learning stage
            - y-axis            : MI score
            - by                : Output components from TCA analysis'''
    sess_n = []
    learn_cat = []
    PC1 = []
    PC2 = []
    PC3 = []
    PC4 = []
    PC5 = []
    PC6 = []
    PC7 = []
    PC8 = []
    PC9 = []
    PC10 = []
    PC11 = []
    PC12 = []
    PC13 = []
    PC14 = []
    PC15 = []
    PC16 = []
    for p in sessions:
        try:
            data = tcaf.run_tca_get_mi(mouse_n, p, 2, plane_n, pre=pre, post=post)
            data = sorted(data[feature])
            sess_n.append(p)
            PC1.append(data[0])
            PC2.append(data[1])
            PC3.append(data[2])
            PC4.append(data[3])
            PC5.append(data[4])
            PC6.append(data[5])
            PC7.append(data[6])
            PC8.append(data[7])
            PC9.append(data[8])
            PC10.append(data[9])
            PC11.append(data[10])
            PC12.append(data[11])
            PC13.append(data[12])
            PC14.append(data[13])
            PC15.append(data[14])
            PC16.append(data[15])
            sess = sessarea.SessArea(datadir, area=2, mouse_n=mouse_n, sess_n=p, mouse_df=mouse_df)
            plane = sess.get_plane(plane_n)
            prop_corr = sess.prop_corr
            if prop_corr < .55:
                learn_cat.append(1)
            elif prop_corr < .65:
                learn_cat.append(2)
            elif prop_corr < .75:
                learn_cat.append(3)
            elif prop_corr > .75:
                learn_cat.append(4)
        except: continue

    df = pd.DataFrame({
        'Session': sess_n,
        'learn_cat': learn_cat,
        'PC1': PC1,
        'PC2': PC2,
        'PC3': PC3,
        'PC4': PC4,
        'PC5': PC5,
        'PC6': PC6,
        'PC7': PC7,
        'PC8': PC8,
        'PC9': PC9,
        'PC10': PC10,
        'PC11': PC11,
        'PC12': PC12,
        'PC13': PC13,
        'PC14': PC14,
        'PC15': PC15,
        'PC16': PC16})

    df_learn = df.groupby('learn_cat', as_index=False).mean()

    fig = plt.figure()
    plt.xticks(np.arange(1, 5, 1.0), ("Naieve", "2", "3", "Expert"))
    sns.lineplot(data=df_learn, x="learn_cat", y="PC1", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC2", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC3", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC4", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC5", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC6", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC7", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC8", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC9", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC10", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC11", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC12", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC13", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC14", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC15", markers=True)
    sns.lineplot(data=df_learn, x="learn_cat", y="PC16", markers=True)
    plt.xlabel("Learning stage")
    plt.ylabel("Mutual information")
    plt.title(f"Distribution of MI scores over learning {feature} - Mouse {mouse_n} {plane.level} {plane.layer}")
    plt.show()


def plot_all_components_highest_mi(feature, mouse_n, plane_n, sessions, pre=1, post=1.5):
    '''Plots all 16 components MI scores for a given feature:
    Required args:
            - feature           : The variable to calculate MI scores for (stim_pos, lick_t, lpt, ___)
            - mouse_n           : Mouse number
            - plane_n           : Plane of interest
            - sessions          : 1D list of sessions where both planes contained imaging data.
            - pre               : the pre-stimulus time period to use (seconds)
            - post              : the post-stimulus time period to use (seconds)
    Output:
            - x-axis            : session number
            - y-axis            : MI score
            - by                : Output components from TCA analysis'''
    learn_cat = []
    sess_n = []
    PC1 = []
    PC2 = []
    PC3 = []
    PC4 = []
    PC5 = []
    PC6 = []
    PC7 = []
    PC8 = []
    PC9 = []
    PC10 = []
    PC11 = []
    PC12 = []
    PC13 = []
    PC14 = []
    PC15 = []
    PC16 = []
    for p in sessions:
        try:
            data = tcaf.run_tca_get_mi(mouse_n, p, 2, plane_n, pre=pre, post=post)
            data = sorted(data[feature])
            sess_n.append(p)
            PC1.append(data[0])
            PC2.append(data[1])
            PC3.append(data[2])
            PC4.append(data[3])
            PC5.append(data[4])
            PC6.append(data[5])
            PC7.append(data[6])
            PC8.append(data[7])
            PC9.append(data[8])
            PC10.append(data[9])
            PC11.append(data[10])
            PC12.append(data[11])
            PC13.append(data[12])
            PC14.append(data[13])
            PC15.append(data[14])
            PC16.append(data[15])
            sess = sessarea.SessArea(datadir, area=2, mouse_n=mouse_n, sess_n=p, mouse_df=mouse_df)
            plane = sess.get_plane(plane_n)
            prop_corr = sess.prop_corr
            if prop_corr < .55:
                learn_cat.append(1)
            elif prop_corr < .65:
                learn_cat.append(2)
            elif prop_corr < .75:
                learn_cat.append(3)
            elif prop_corr > .75:
                learn_cat.append(4)
        except: continue

    df = pd.DataFrame({
        'Session': sess_n,
        'learn_cat': learn_cat,
        'PC1': PC1,
        'PC2': PC2,
        'PC3': PC3,
        'PC4': PC4,
        'PC5': PC5,
        'PC6': PC6,
        'PC7': PC7,
        'PC8': PC8,
        'PC9': PC9,
        'PC10': PC10,
        'PC11': PC11,
        'PC12': PC12,
        'PC13': PC13,
        'PC14': PC14,
        'PC15': PC15,
        'PC16': PC16})

    df_learn = df.sort_values(by="learn_cat")

    fig = plt.figure()
    plt.xticks(np.arange(1, 30, 1.0))
    sns.lineplot(data=df_learn, x="Session", y="PC1", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC2", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC3", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC4", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC5", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC6", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC7", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC8", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC9", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC10", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC11", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC12", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC13", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC14", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC15", markers=True)
    sns.lineplot(data=df_learn, x="Session", y="PC16", markers=True)
    plt.xlabel("Session")
    plt.ylabel("Mutual information")
    plt.title(f"Distribution of MI scores over learning {feature} - Mouse {mouse_n} {plane.level} {plane.layer}")
    plt.show()





# L2HS corresponds to area 2 plane 1 in CTBD2.5a, CTBD7.2a, CTBD7.2e, CTBD11.3g
# L3D corresponds to area 2 plane 2 in CTBD2.5a, CTBD7.2a, CTBD7.2e, CTBD11.3g
# L3D corresponds to area 3 plane 1 in CTBD11.3e, CTBD13.5c, CTBD7.6e, CTBD7.6f
#                    area 3 plane 2

feature = "stim_pos"                 # runspeed (D)
mouse_n = 5
sessions = sessions_m5
pre=1
post=1.5
plane_n = 1

plot_all_components_mean_highest_mi(feature, mouse_n, plane_n, sessions, pre=pre, post=post)
