'''This script creates a 3rd order tensor from imaging data contained in a sess_area object. It then runs a
CP ALS tensor decomposition and compares the trial factors across a number of behavioural paramaters
using mutual information.'''

import sys
import os
import copy
import sklearn
import gcmi
import warnings
import matplotlib

import numpy as np
import tensorflow as tf
import tensortools as tt
import pandas as pd
import seaborn as sns

sys.path.extend(['.', '../'])
matplotlib.use('Qt5Agg')
from analysis import sessarea
from mult_util import mult_gen_util
from mult_util import mult_mat_util
from util import math_util
from scipy.stats import pointbiserialr
from collections import Counter
from matplotlib import pyplot as plt
from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


#We will assaign our path for our imaging data and mouse data
datadir = os.path.join(
    'C:/University_Work/colmdowling/multibar_analysis/data')
mouse_df = 'C:/University_Work/colmdowling/multibar_analysis/mouse_df_new.csv'

#mult_mat_util.build_mouse_df(datadir)
#mult_gen_util.add_depth_info(df = mouse_df, savename='mouse_df_newest2', fulldir='C:/University_Work/colmdowling/multibar_analysis', overwrite=False)

# We can use the information from our mouse data to create a SessArea object
mouse_n = 6
sess_n = 5
area = 2

# We can define our plane number, and the timeframe (pre/post stimulus) that we are interested in.
plane_n = 1
pre = 1
post = 1.5

# We can specify our rank (the number of components to be computed in the TCA)
rank = 16


########################################################################################################################

# INITIALISING SESSAREA OBJECT
print("__INITIALISING SESSAREA OBJECT__")

# This defines our sessionarea object and assaigns it to the variable "sess".
sess = sessarea.SessArea(
    datadir, area, mouse_n=mouse_n, sess_n=sess_n, mouse_df=mouse_df)

# This will print basic info for each plane object contained within sess.
# For each value (plane) of 1 and 2
print(f'M = {mouse_n},     S = {sess_n},     A = {area}')
for p in [1, 2]:
    plane = sess.get_plane(p)
    layer = plane.layer
    level = plane.level
    nrois = plane.nrois
    print(f'Plane {p}: {level} {layer} (ROIs = {nrois})')
print(f'Proportion correct: {sess.prop_corr}')

# We will use ranklen when creating for loops for all principle components.
ranklen = [i for i in range(1,rank+1)]



########################################################################################################################

# GENERATING 3RD ORDER TENSORS FOR A PLANE
print("__GENERATING 3RD ORDER TENSORS FOR A PLANE__")

plane = sess.get_plane(plane_n)         # Assaigns the current plane to a plane object called "plane"
sess.set_plane_oi(plane_oi=plane_n)     # This just allows us to reference the plane. It doesn't change the code.

# This will get all of corrected 2p data (dF/F) and convert it to a 3rd order tensor
trial_ns = sess.get_trials_by_criteria()
twop_ref_fr = plane.get_twop_fr_by_trial(trial_ns, first=True)
frame, data = plane.get_roi_trace_array(twop_ref_fr, pre=pre,
        post=post, trace_type="corr")
tensordata = tf.convert_to_tensor(data)

# NB: Axis0 = ROI, Axis1 = Trials, Axis2 = Timepoints




########################################################################################################################
'''
# DETERMINE MODEL METHOD AND RANK FOR TCA
print("__DETERMINE MODEL METHOD AND RANK FOR TCA__")

# TCA produces different results depending on the number of components you want to calculate, as well as random initial factors.
# We can run ensembles of tensor decompositions to compare how well different outputs might look.
methods = (
  'cp_als',    # fits unconstrained tensor decomposition.
  'ncp_bcd',   # fits nonnegative tensor decomposition.
  'ncp_hals',  # fits nonnegative tensor decomposition.
)

ensembles = {}
for m in methods:
    ensembles[m] = tt.Ensemble(fit_method=m, fit_options=dict(tol=1e-4))
    ensembles[m].fit(tensordata, ranks=range(1, 50), replicates=3)

# Plotting options for the unconstrained and nonnegative models.
plot_options = {
  'cp_als': {
    'line_kw': {
      'color': 'black',
      'label': 'cp_als',
    },
    'scatter_kw': {
      'color': 'black',
    },
  },
  'ncp_hals': {
    'line_kw': {
      'color': 'blue',
      'alpha': 0.5,
      'label': 'ncp_hals',
    },
    'scatter_kw': {
      'color': 'blue',
      'alpha': 0.5,
    },
  },
  'ncp_bcd': {
    'line_kw': {
      'color': 'red',
      'alpha': 0.5,
      'label': 'ncp_bcd',
    },
    'scatter_kw': {
      'color': 'red',
      'alpha': 0.5,
    },
  },
}

# Plot similarity and error plots.
plt.figure()
for m in methods:
    tt.plot_objective(ensembles[m], **plot_options[m])
plt.legend()

plt.figure()
for m in methods:
    tt.plot_similarity(ensembles[m], **plot_options[m])
plt.legend()
plt.show()
'''


########################################################################################################################

# TENSOR COMPOSITION ANALYSIS
print("__TENSOR COMPOSITION ANALYSIS__")

# We fit the model from 2 initial conditions to ensure the initial guess doesn't have too much weight on the solution.
# If the models have a poor similarity we can change the random_state seed variable to generate new models.
tca1 = tt.cp_als(tensordata, rank=rank, random_state=1, max_iter=500, verbose=False)
tca2 = tt.cp_als(tensordata, rank=rank, random_state=2, max_iter=500, verbose=False)

# Compare the low-dimensional factors from the two fits.
fig, _, _ = tt.plot_factors(tca1.factors, plots=['bar','scatter','line'])
tt.plot_factors(tca2.factors, plots=['bar','scatter','line'], fig=fig)
fig.suptitle("raw models")
fig.tight_layout()

# Align the two fits and print a similarity score.
sim = tt.kruskal_align(tca1.factors, tca2.factors, permute_U=True, permute_V=True)
print(f'Plane {sess.plane_oi} TCA similarity score - {sim}')

# Plot the results again to see alignment.
fig, ax, po = tt.plot_factors(tca1.factors, plots=['bar','scatter','line'])
tt.plot_factors(tca2.factors, plots=['bar','scatter','line'], fig=fig)
fig.suptitle("aligned models")
fig.tight_layout()

# We can uncomment the line below if we want to show the aligned plots.
#plt.show()

# Extract the factors and assaign them so we can plot them individually
neuron_fac = tca1.factors[0].T
trial_fac = tca1.factors[1].T
time_fac = tca1.factors[2].T




########################################################################################################################

# EXTRACTING TRIAL AND BEHAVIOURAL DATA
print("__EXTRACTING TRIAL AND BEHAVIOURAL DATA__")

# STIM POSITION
stim_pos = sess.trial_df['stim_pos']
stim_pos_color = ["green" if position == sess.go_pos else "goldenrod" for position in stim_pos]


# TRIAL TYPE
trial_outcome = sess.all_trial_outc
lick_vs_nolick = ["lick" if outcome == "corr_acc" or
                            outcome == "false_pos" or
                            outcome == "too_soon" else
                            "no lick" for outcome in trial_outcome]
lick_vs_nolick_col = ["brown" if outcome == "no lick" else "pink" for outcome in lick_vs_nolick]

corr_vs_incorr = ["correct" if outcome == "corr_acc" or
                            outcome == "corr_rej" else
                            "incorrect" for outcome in trial_outcome]
corr_vs_incorr_col = ["blue" if outcome == "correct" else "red" for outcome in corr_vs_incorr]


# RUNNING SPEED
#Returns a list of trial numbers (1,2,3,4...n)
trial_ns = sess.get_trials_by_criteria()
#Returns a list of arrays containing the run frame numbers that correspond to the trial numbers
twop_ref_fr = plane.get_twop_fr_by_trial(trial_ns, first=True)
#Returns a frame of 2p numbers and a dataset containing the runspeed for each timepoint in each trial.
runframe, rundata = plane.get_run_velocity_array(twop_ref_fr, pre=pre, post=post)

#We will take the time-averaged runspeed for each trial
run_mean_per_trial = np.mean(rundata, axis = 1)
#We use a threshold of 40 to determine if a trial was a "run" trial or not.
run_vs_norun_col = ["orange" if value > 40 else "cyan" for value in run_mean_per_trial]


# LICKING
#Returns a list of trial numbers (1,2,3,4...n)
trial_ns = sess.get_trials_by_criteria()
#Returns a list of arrays containing the run frame numbers that correspond to the trial numbers
twop_ref_fr = plane.get_twop_fr_by_trial(trial_ns, first=True)
#Returns a frame of 2p numbers and a dataset containing whether each timepoint of each trial contained a lick.
lickframe, lickdata = plane.get_lick_array(twop_ref_fr, pre=pre, post=post)

#We will take the average number of licks per frame for each trial
lick_total_per_trial = np.sum(lickdata, axis = 1)
#We use a threshold of .04 to determine whether a trial contained a lot of licking or not.
lick_threshold = ["black" if value > 0 else "magenta" for value in lick_total_per_trial]




########################################################################################################################

# CORRELATING TRIAL FACTORS WITH BEHAVIOUR
print("__CORRELATING TRIAL FACTORS WITH BEHAVIOUR__")

# It helps to calculate the discrete variables as numbers.
stim_pos_int = [0 if value == "yellow" in stim_pos_color else 1 for value in stim_pos_color]
corr_vs_incorr_int = [0 if value == "correct" in corr_vs_incorr else 1 for value in corr_vs_incorr]
lick_vs_nolick_int = [0 if value == "lick" in lick_vs_nolick else 1 for value in lick_vs_nolick]
run_vs_norun_int = [0 if value > 40 else 1 for value in run_mean_per_trial]
lick_threshold_int = [0 if value > 0 else 1 for value in lick_total_per_trial]


# MUTUAL INFORMATION
def return_more_than_n(list, n):
    '''Returns all numbers in a list which occur more than n times'''
    counts = Counter(list)
    out_list = [i for i in counts if counts[i]>n]
    return out_list

def run_gcmi(X, n, rank):
    '''Checks that there are at least n occurences of each unique value in a list X. If each value occurs at least
    n-times, the output will be a list of MI scores for each trial factor and X. The output will be a list of 0s if
    there are less than n occurances.
    Required args:
            - X                 : 1D list of binary integers, representing the categorical (feature) variable.
            - n                 : The minimum number of unique values of X needed to calculate MI.
            - rank              : The number of principle components you wish to calculate'''
    output = []
    n = n-1
    ranklen = [i for i in range(1,rank+1)]
    if len(return_more_than_n(X, n)) < 2:
        for p in ranklen:
            output.append(0)
    elif len(return_more_than_n(X, n)) >= 2:
        for p in ranklen:
            mi = gcmi.gcmi_model_cd(trial_fac[p - 1], X, 2)
            output.append(mi)
    return output


mi_lick_v_nolick = run_gcmi(lick_vs_nolick_int, 2, rank)
mi_correct_vs_incorrect = run_gcmi(corr_vs_incorr_int, 2, rank)
mi_stim_pos = run_gcmi(stim_pos_int, 2, rank)
mi_runspeed_discrete = run_gcmi(run_vs_norun_int, 2, rank)
mi_licks_per_t_discrete = run_gcmi(lick_threshold_int, 2, rank)

'''
# The continuous variables for runspeed and licks don't MI very well because they have a lot of repeated values of 0
mi_runspeed = []
for p in ranklen:
    mi = gcmi.gcmi_cc(trial_fac[p-1],run_mean_per_trial)
    mi_runspeed.append(mi)

mi_licks_per_t = []
for p in ranklen:
    mi = gcmi.gcmi_cc(trial_fac[p-1],lick_total_per_trial)
    mi_licks_per_t.append(mi)
'''

mi_df = pd.DataFrame({
    'PC': ranklen,                                      # Principle component n (discrete)
    'lick_t': mi_lick_v_nolick,                         # Whether the mouse licked during the "lick" stage (discrete)
    'correct_t': mi_correct_vs_incorrect,               # Whether it was a correct trial or not (discrete)
    'stim_pos': mi_stim_pos,                            # Whether it was a GO or NO-GO trial (discrete)
    'runspeed (D)': mi_runspeed_discrete,
    'licks_per_t (D)': mi_licks_per_t_discrete
#    'runspeed (C)': mi_runspeed,                        # The mouse's average run velocity (continuous)
#    'licks_per_t (C)': mi_licks_per_t                   # The number of licks during the trial (continuous)
})
print("*Gaussian Copula Mutual Information*")
print(mi_df)


'''
# POINT BISERIAL CORRELATION

def pbc_trial_fac(cat_data):
    output = []
    for p in ranklen:
        pbc = pointbiserialr(cat_data,trial_fac[p-1])
        output.append(p)
        output.append(pbc)
    return output

pbc_stim_pos = pbc_trial_fac(stim_pos_bool)
pbc_corr_vs_incorr = pbc_trial_fac(corr_vs_incorr_bool)
pbc_lick_vs_nolick = pbc_trial_fac(lick_vs_nolick_bool)
print(pbc_stim_pos)
'''




########################################################################################################################

# GENERATING PLOTS
print("__GENERATING PLOTS__")
# We will make a new window type with scrollable bars. This will mean we can plot many subplots without squishing.
class ScrollableWindow(QtWidgets.QMainWindow):
    def __init__(self, fig):
        self.qapp = QtWidgets.QApplication([])

        QtWidgets.QMainWindow.__init__(self)
        self.widget = QtWidgets.QWidget()
        self.setCentralWidget(self.widget)
        self.widget.setLayout(QtWidgets.QVBoxLayout())
        self.widget.layout().setContentsMargins(0,0,0,0)
        self.widget.layout().setSpacing(0)

        self.fig = fig
        self.canvas = FigureCanvas(self.fig)
        self.canvas.draw()
        self.scroll = QtWidgets.QScrollArea(self.widget)
        self.scroll.setWidget(self.canvas)

        self.nav = NavigationToolbar(self.canvas, self.widget)
        self.widget.layout().addWidget(self.nav)
        self.widget.layout().addWidget(self.scroll)

        self.show()
        self.qapp.exec_()

warnings.filterwarnings("ignore", message="Adding an axes using the same")
plt.close('all')


# Here we will plot our principle components and colour the trial factor by whether the mouse licked when the pole was presented.
fig_a = plt.figure(figsize=(9,1.5*rank))
fig_a.suptitle("Lick vs no-lick trials", fontsize=14)
fig.legend(["Lick", "No Lick"], loc = 'upper right')
plt.subplots_adjust(wspace=0.3, hspace=0.1)
plt.subplot(rank,3,1)
plt.title("Neuron Factor")
plt.subplot(rank,3,2)
plt.title("Time Factor")
plt.subplot(rank,3,3)
plt.title("Trial Factor")
for p in ranklen:                         # For each of our principle components we generate a graph of the factors
    n=((p-1)*3)+1                         # Each subplot wil have a unique position in an array of numbers (1,2,3,4,5...)
    plt.subplot(rank,3,n)
    plt.bar(range(len(neuron_fac[p-1])),neuron_fac[p-1])

    n=((p-1)*3)+2
    plt.subplot(rank,3,n)
    plt.plot(time_fac[p-1])

    n = ((p-1)*3)+3
    plt.subplot(rank,3,n)
    plt.scatter(range(len(trial_fac[p-1])),trial_fac[p-1],
                c=lick_vs_nolick_col, s=5)
# pass the figure to the custom window
plotfig_a = ScrollableWindow(fig_a)


# Here we will plot our principle components and colour the trial factor by whether the mouse correctly performed.
fig_b = plt.figure(figsize=(9,1.5*rank))
fig_b.suptitle("Correct vs incorrect trials", fontsize=14)
plt.subplots_adjust(wspace=0.3, hspace =0.1)
plt.subplot(rank,3,1)
plt.title("Neuron Factor")
plt.subplot(rank,3,2)
plt.title("Time Factor")
plt.subplot(rank,3,3)
plt.title("Trial Factor")
for p in ranklen:                         # For each of our principle components we generate a graph of the factors
    n=((p-1)*3)+1                         # Each subplot wil have a unique position in an array of numbers (1,2,3,4,5...)
    plt.subplot(rank,3,n)
    plt.bar(range(len(neuron_fac[p-1])),neuron_fac[p-1])

    n=((p-1)*3)+2
    plt.subplot(rank,3,n)
    plt.plot(time_fac[p-1])

    n = ((p-1)*3)+3
    plt.subplot(rank,3,n)
    plt.scatter(range(len(trial_fac[p-1])),trial_fac[p-1],
                c=corr_vs_incorr_col, s=5)
# pass the figure to the custom window
plotfig_b = ScrollableWindow(fig_b)


# Here we will plot our principle components and colour the trial factor by stimulus position.
fig_c = plt.figure(figsize=(9,1.5*rank))
fig_c.suptitle("GO vs NOGO position", fontsize=14)
plt.subplots_adjust(wspace=0.3, hspace=0.1)
plt.subplot(rank,3,1)
plt.title("Neuron Factor")
plt.subplot(rank,3,2)
plt.title("Time Factor")
plt.subplot(rank,3,3)
plt.title("Trial Factor")
for p in ranklen:                         # For each of our principle components we generate a graph of the factors
    n=((p-1)*3)+1                         # Each subplot wil have a unique position in an array of numbers (1,2,3,4,5...)
    plt.subplot(rank,3,n)
    plt.bar(range(len(neuron_fac[p-1])),neuron_fac[p-1])

    n=((p-1)*3)+2
    plt.subplot(rank,3,n)
    plt.plot(time_fac[p-1])

    n = ((p-1)*3)+3
    plt.subplot(rank,3,n)
    plt.scatter(range(len(trial_fac[p-1])),trial_fac[p-1],
                c=stim_pos_color, s=5)
# pass the figure to the custom window
plotfig_c = ScrollableWindow(fig_c)


# Here we will replot our principle components and colour the trial factor by stimulus position.
# We will also change the markers to reflect whether it was a correct or incorrect trial.
fig_d = plt.figure(figsize=(9,1.5*rank))
fig_d.suptitle("GO vs NOGO position x correct vs incorrect trials", fontsize=14)
plt.subplots_adjust(wspace=0.3, hspace=0.1)
plt.subplot(rank,3,1)
plt.title("Neuron Factor")
plt.subplot(rank,3,2)
plt.title("Time Factor")
plt.subplot(rank,3,3)
plt.title("Trial Factor")
for p in ranklen:                         # For each of our principle components we generate a graph of the factors
    n=((p-1)*3)+1                         # Each subplot wil have a unique position in an array of numbers (1,2,3,4,5...)
    plt.subplot(rank,3,n)
    plt.bar(range(len(neuron_fac[p-1])),neuron_fac[p-1])

    n=((p-1)*3)+2
    plt.subplot(rank,3,n)
    plt.plot(time_fac[p-1])

    n=((p-1)*3)+3
    plt.subplot(rank,3,n)
    tupledata = list(zip(range(len(trial_fac[p-1])),trial_fac[p-1], stim_pos, corr_vs_incorr))
    pandasdata = pd.DataFrame(tupledata, columns=["x", "y", "hue", "style"])
    sns.scatterplot(data = pandasdata, x = "x", y = "y", palette = ["green","olive"], hue = "hue", style = "style", s = 10, legend = False)
# pass the figure to the custom window
plotfig_d = ScrollableWindow(fig_d)


# Here we will plot our principle components and colour the trial factor by running speed.
fig_e = plt.figure(figsize=(9,1.5*rank))
fig_e.suptitle("Running Speed", fontsize=14)
plt.subplots_adjust(wspace=0.3, hspace=0.1)
plt.subplot(rank,3,1)
plt.title("Neuron Factor")
plt.subplot(rank,3,2)
plt.title("Time Factor")
plt.subplot(rank,3,3)
plt.title("Trial Factor")
for p in ranklen:                         # For each of our principle components we generate a graph of the factors
    n=((p-1)*3)+1                         # Each subplot wil have a unique position in an array of numbers (1,2,3,4,5...)
    plt.subplot(rank,3,n)
    plt.bar(range(len(neuron_fac[p-1])),neuron_fac[p-1])

    n=((p-1)*3)+2
    plt.subplot(rank,3,n)
    plt.plot(time_fac[p-1])

    n = ((p-1)*3)+3
    plt.subplot(rank,3,n)
    plt.scatter(range(len(trial_fac[p-1])),trial_fac[p-1],
                c=run_mean_per_trial, s=5)
# pass the figure to the custom window
plotfig_e = ScrollableWindow(fig_e)


# Here we will plot our principle components and colour the trial factor by how much licking occured during the trial.
fig_f = plt.figure(figsize=(9,1.5*rank))
fig_f.suptitle("Number of licks during pre/post timeframe", fontsize=14)
plt.subplots_adjust(wspace=0.3, hspace=0.1)
plt.subplot(rank,3,1)
plt.title("Neuron Factor")
plt.subplot(rank,3,2)
plt.title("Time Factor")
plt.subplot(rank,3,3)
plt.title("Trial Factor")
for p in ranklen:                         # For each of our principle components we generate a graph of the factors
    n=((p-1)*3)+1                         # Each subplot wil have a unique position in an array of numbers (1,2,3,4,5...)
    plt.subplot(rank,3,n)
    plt.bar(range(len(neuron_fac[p-1])),neuron_fac[p-1])

    n=((p-1)*3)+2
    plt.subplot(rank,3,n)
    plt.plot(time_fac[p-1])

    n = ((p-1)*3)+3
    plt.subplot(rank,3,n)
    plt.scatter(range(len(trial_fac[p-1])),trial_fac[p-1],
                c=lick_total_per_trial, s=5)
# pass the figure to the custom window
plotfig_f = ScrollableWindow(fig_f)



#for plane in sess.set_plane_oi(plane_oi=plane)
#plane1, plane2 = sess.get_plane(sess.plane_oi)




