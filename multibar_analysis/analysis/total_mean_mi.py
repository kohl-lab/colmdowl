'''This script is for calculating the total MI averages for stim position, licks per trial, lick/no-lick,
correct/incorrect and runspeed. This does so for all session/areas over all learning stages.'''

import sys
import os
import copy
import sklearn
import gcmi
import warnings
import matplotlib
import warnings

import tca_functions as tcaf
import numpy as np
import tensorflow as tf
import tensortools as tt
import pandas as pd
import seaborn as sns

sys.path.extend(['.', '../'])
matplotlib.use('Qt5Agg')
from analysis import sessarea
from mult_util import mult_gen_util
from util import math_util
from scipy.stats import pointbiserialr
from collections import Counter
from matplotlib import pyplot as plt
from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


#We will assaign our path for our imaging data and mouse data
datadir = os.path.join(
    'C:/University_Work/colmdowling/multibar_analysis/data')
mouse_df = 'C:/University_Work/colmdowling/multibar_analysis/mouse_df_new.csv'


top_n = 1  # top n MI scores will be included


sessions = [i for i in range(1,28)]
areas = [i for i in range(1,4)]
planes = [i for i in range(1,3)]


def get_total_mean_mi(mouse, sessions, areas, planes, pre=1, post=5):
    means_lick = []
    means_correct = []
    means_stim = []
    means_run = []
    means_lpt = []
    for p in sessions:
        for q in areas:
            for r in planes:
                try:
                    sess = sessarea.SessArea(datadir, q, mouse_n=mouse, sess_n=p, mouse_df=mouse_df)
                    plane = sess.get_plane(r)
                    if plane.incl != 1:    # Excludes any sessions that have been marked for exclusion
                        print(f"Skipping M{mouse}, S{p}, A{q}, P{r}")
                    else:
                        try:
                            # Run tca one each of the sessions, areas and planes and get the MI scores.
                            # If it generates an error it will skip to the next iteration.
                            data = tcaf.run_tca_get_mi(mouse,p,q,r,pre=pre, post=post)
                            # Gets the average of the highest MI scores for that column (top_n indicates how many scores to average)
                            mean_highest_mi_lick = tcaf.get_mean_highest_mi(data, "lick_t", top_n)
                            means_lick.append(mean_highest_mi_lick)
                        except:
                            continue
                        try:
                            mean_highest_mi_correct = tcaf.get_mean_highest_mi(data, "correct_t", top_n)
                            means_correct.append(mean_highest_mi_correct)
                        except:
                            continue
                        try:
                            mean_highest_mi_stim = tcaf.get_mean_highest_mi(data, "stim_pos", top_n)
                            means_stim.append(mean_highest_mi_stim)
                        except:
                            continue
                        try:
                            mean_highest_mi_run = tcaf.get_mean_highest_mi(data, "runspeed (D)", top_n)
                            means_run.append(mean_highest_mi_run)
                        except:
                            continue
                        try:
                            mean_highest_mi_lpt = tcaf.get_mean_highest_mi(data, "licks_per_t (D)", top_n)
                            means_lpt.append(mean_highest_mi_lpt)
                        except:
                            continue
                except:
                    continue

    # Let's get rid of those pesky 0s. They are generated if there isn't enough variance in the data to calculate MI.
    means_lick = [i for i in means_lick if i != 0]
    means_correct = [i for i in means_correct if i != 0]
    means_stim = [i for i in means_stim if i != 0]
    means_run = [i for i in means_run if i != 0]
    means_lpt = [i for i in means_lpt if i != 0]

    mean_means_lick = np.mean(means_lick)
    mean_means_correct = np.mean(means_correct)
    mean_means_stim = np.mean(means_stim)
    mean_means_run = np.mean(means_run)
    mean_means_lpt = np.mean(means_lpt)

    output = np.array([mean_means_lick, mean_means_correct, mean_means_stim, mean_means_run, mean_means_lpt])

    return output

m1 = get_total_mean_mi(1, sessions=sessions, areas=areas, planes=planes)
m2 = get_total_mean_mi(2, sessions=sessions, areas=areas, planes=planes)
m3 = get_total_mean_mi(3, sessions=sessions, areas=areas, planes=planes)
m4 = get_total_mean_mi(4, sessions=sessions, areas=areas, planes=planes)
m5 = get_total_mean_mi(5, sessions=sessions, areas=areas, planes=planes)
m6 = get_total_mean_mi(6, sessions=sessions, areas=areas, planes=planes)
m7 = get_total_mean_mi(7, sessions=sessions, areas=areas, planes=planes)
m8 = get_total_mean_mi(8, sessions=sessions, areas=areas, planes=planes)
allmice = [m1, m2, m3, m4, m5, m6, m7, m8]

df = pd.DataFrame(allmice, columns = ['decision to lick',
                                      'correct trial',
                                      'stimulus position',
                                      'run velocity',
                                      'n licks during trial'])
df = df.transpose()
df.to_csv(os.path.join(datadir, 'mean_mi_data.csv'))

fig = plt.figure()
plt.title("Mean highest MI scores for trial factors")
plt.ylabel("Mutual information")
sns.barplot(data=df, markers=True)
plt.show()