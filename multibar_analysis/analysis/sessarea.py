"""
sess_area.py

Classes to store, extract, and analyze a session recording for one area for
data from the MULTIBAR project (Kohl lab, Dr. Mariangela Panniello)..

Authors: Colleen Gillon, Colm Dowling

Date: September, 2019

Note: this code uses python 3.7.

"""

import os

import h5py
import numpy as np
import pandas as pd
import scipy.stats as st
from sklearn.decomposition import PCA, NMF

from mult_util import mult_mat_util
from util import file_util, gen_util, math_util
from oasis.functions import deconvolve


#############################################
#############################################
class SessArea(object):
    """
    The SessArea object is the top-level object for analyzing a session
    recording for one area for the Kohl lab MULTIBAR Project. All that needs
    to be provided to create the object is the data directory, mouse ID or
    number, the session date or number and the area number. The SessArea object
    that is created will contain all of the information relevant to a
    session, including stimulus information, behaviour information and
    pointers to the 2p data.
    """

    def __init__(self, datadir, area, mouseid=None, date=None, mouse_n=None,
                 sess_n=None, mouse_df='mouse_df.csv', plane_oi='both'):
        """
        self.__init__(datadir, area)

        Initializes and returns the new SessArea object using the specified
        data directory, mouse ID or number, session date or number and
        area number. Also initializes Plane objects.

        Sets attributes:
            - datapath (str): path to the data file (.mat)
            - home (str)    : path of the master data directory

        and calls self._init_attribs() and
                  self._get_mat_data()

        Required args:
            - datadir (str): full path to the directory where data is stored
            - area (int)   : area

        Optional args:
            - mouseid (str)        : mouse ID (must provide mouse ID or number,
                                     but not both)
                                     default: None
            - date (str or int)    : date, in YYYYMMDD format (must provide date
                                     or session number, but not both)
                                     default: None
            - mouse_n (str or int) : mouse number (must provide mouse ID or
                                     number, but not both)
                                     default: None
            - sess_n (str or int)  : session number (must provide date or
                                     session number, but not both)
                                     default: None
            - mouse_df (str)       : path name of dataframe containing
                                     information on each recorded session.
                                     Dataframe should have the following
                                     columns:
                                         mouse_n, mouseid, sess_n, date, task,
                                         area, prop_go, go_pos, n_trials_rec,
                                         corr_acc, corr_rej, false_pos, missed,
                                         too_soon, n_trials, correct, as well
                                         as columns needed for Plane object
                                     default: 'mouse_df.csv'
            - plane_oi (str or int): plane of interest
                                     default: 'both'
        """

        self.home = datadir

        self._init_attribs(area, mouseid, date, mouse_n, sess_n, mouse_df)

        self.datapath = os.path.join(self.home, '{}.mat'.format(self.mouseid))

        self._get_mat_data()

        self.plane_1 = Plane(self, 1, mouse_df)
        self.plane_2 = Plane(self, 2, mouse_df)
        self.planes = [self.plane_1, self.plane_2]

        self.set_plane_oi(plane_oi)


    #############################################
    def __repr__(self):
        return (f'{self.__class__.__name__} '
            f'(M{self.mouse_n}, S{self.sess_n}, A{self.area})')

    def __str__(self):
        return repr(self)




    #############################################
    def _init_attribs(self, area, mouseid=None, date=None, mouse_n=None,
                      sess_n=None, mouse_df='mouse_df.csv'):
        """
        self._init_attribs(area)

        Sets the attributes for a specific session and area based on the mouse
        dataframe:

        Sets attributes:
            - area (int)        : area
            - date (int)        : session date (in YYYYMMDD format)
            - mouseid (str)     : mouse ID (e.g., CTBD7.2e)
            - mouse_n (int)     : mouse number (corresponds to mouse ID)
            - n_trials_rec (int): number of trials during 2p recording
                                  (-1 if no 2p recording or no ROIs extracted)
            - prop_go (float)   : proportion of GO trials (None if task is
                                  passive)
            - pos_go (int)      : GO bar position (70 if task is passive)
            - sess_n (int)      : session number (corresponds to session date)
            - task (str)        : session task

            number of trials during 2p recording (None if no recording):
            - corr_acc (int)    : correct GO trials
            - corr_rej (int)    : correct NO-GO trials
            - false_pos (int)   : false positive (incorrect GO) trials
            - missed (int)      : missed (incorrect NO-GO) trials
            - prop_corr (float) : proportion of correct trials (too soon trials
                                  ignored)
            - too_soon (int)    : too soon (GO, but too early) trials

        Required args:
            - area (int)   : area

        Optional args:
            - mouseid (str)       : mouse ID (must provide mouse ID or number,
                                    but not both)
                                    default: None
            - date (str or int)   : date, in YYYYMMDD format (must provide date
                                    or session number, but not both)
                                    default: None
            - mouse_n (str or int): mouse number (must provide mouse ID or
                                    number, but not both)
                                    default: None
            - sess_n (str or int) : session number (must provide date or
                                    session number, but not both)
                                    default: None
            - mouse_df (str)      : path name of dataframe containing
                                    information on each recorded session.
                                    Dataframe should have the following
                                    columns:
                                        mouse_n, mouseid, sess_n, date, task,
                                        area, prop_go, go_pos, n_trials_rec,
                                        corr_acc, corr_rej, false_pos, missed,
                                        too_soon, n_trials, correct, as well
                                        as columns needed for Plane object
                                    default: 'mouse_df.csv'
        """

        if isinstance(mouse_df, str):
            mouse_df  = file_util.loadfile(mouse_df)

        # collect mouse and session number information
        info = [[mouseid, mouse_n], [date, sess_n]]
        labs = [['mouseid', 'mouse_n'], ['date', 'sess_n']]

        for s in range(len(info)):
            done = False
            if len(info[s]) != 2:
                raise ValueError(
                    'Code must be modified if the sublists are not of '
                    'length 2.')
            for i in range(len(info[s])):
                if info[s][i] is None:
                    if info[s][1-i] is None:
                        raise ValueError(
                            'Must provide either `{}` or `{}`.'.format(
                                labs[s][i], labs[s][1-i]))
                    elif done:
                        raise ValueError(
                            'Cannot provide both `{}` and `{}`.'.format(
                                labs[s][i], labs[s][1-i]))
                    else:
                        if s == 0:
                            info[s][i] = gen_util.get_df_vals(
                                mouse_df, labs[s][1-i], info[s][1-i],
                                labs[s][i], single=True)
                        else:
                            info[s][i] = gen_util.get_df_vals(
                                mouse_df, [labs[s][1-i], 'mouseid'],
                                [info[s][1-i], info[0][0]],
                                labs[s][i], single=True)
                        done = True

        self.mouseid, self.mouse_n  = info[0]
        self.date,     self.sess_n  = info[1]

        self.area = area
        areas = gen_util.get_df_vals(
            mouse_df, ['mouseid', 'date'], [self.mouseid, self.date], 'area',
            dtype=str)

        if str(self.area) not in areas:
            raise ValueError(
                'No area {} found for mouse {}, {} session.'.format(
                    self.area, self.mouseid, self.date))

        df_line = gen_util.get_df_vals(
            mouse_df, ['mouseid', 'date', 'area'],
            [self.mouseid, self.date, self.area], single=True)

        self.task    = df_line['task'].tolist()[0]
        self.prop_go = float(df_line['prop_go'].tolist()[0])
        self.n_trials_rec = int(df_line['n_trials_rec'].tolist()[0])

        keys_int = ['go_pos', 'corr_acc', 'corr_rej', 'false_pos', 'missed',
            'too_soon']
        vals_int = []
        for key in keys_int:
            val = df_line[key].tolist()[0]
            if np.isnan(val):
                vals_int.append(None)
            else:
                vals_int.append(int(val))

        [self.go_pos, self.corr_acc, self.corr_rej,
            self.false_pos, self.missed, self.too_soon] = vals_int

        if self.go_pos is None:
            self.go_pos = 70

        if np.isnan(self.prop_go):
            self.prop_go = None

        self.prop_corr = df_line['prop_corr'].tolist()[0]


    #############################################
    def _make_trial_df(self, trial_outc=None, stim_pos=None, n_2pfr=None,
                       go_pos=None, nogo_pos=None):
        """
        self._make_trial_df()

        Saves a dataframe containing trial information as an attribute.

        Sets attributes:
            - stim_pos (list)        : sorted list of stim positions
            - trial_outc (list)      : sorted list of trial outcome types (None
                                       if task is sens_stim)
            - trial_df (pd DataFrame): trial alignment dataframe with columns:
                                       'trial_n', 'stim_pos', 'outcome',
                                       'start2pfr', 'end2pfr', 'num2pfr'
        """

        self.trial_outc = trial_outc
        if self.trial_outc is not None:
            self.trial_outc = sorted(set(self.trial_outc))

        self.trial_df = pd.DataFrame(
            columns=['trial_n', 'stim_pos', 'outcome', 'start2pfr', 'end2pfr',
                'num2pfr'])

        if trial_outc is None:
            if stim_pos is None:
                raise ValueError('Must pass trial outcome or stim position.')
            else:
                n_trials = len(stim_pos)
                self.trial_df['stim_pos'] = stim_pos
                self.trial_df['outcome'] = [''] * n_trials
        else:
            n_trials = len(trial_outc)
            self.trial_df['outcome'] = trial_outc
            if stim_pos is None:
                stim_pos = [-1] * n_trials
                # add in the go_pos and nogo_pos as stimulus positions
                if go_pos is None or nogo_pos is None:
                    raise ValueError(
                        'If stim_pos is None, go_pos and nogo_pos must be '
                        'provided.')
                trial_outc = np.asarray(trial_outc)
                stim_pos   = np.asarray(stim_pos)
                go_trials = np.where(
                    (trial_outc == 'corr_acc') | (trial_outc == 'missed') |
                    (trial_outc == 'too_soon'))
                nogo_trials = np.where(
                    (trial_outc == 'corr_rej') | (trial_outc == 'false_pos'))
                stim_pos[go_trials]   = go_pos
                stim_pos[nogo_trials] = nogo_pos
            self.trial_df['stim_pos'] = stim_pos

        self.trial_df['trial_n'] = list(range(n_trials))

        # add the start, end and number of 2p frames
        if len(self.motor_whisk) == len(self.trial_df):
            start2pfr = self.motor_whisk
            end2pfr = (np.asarray(self.motor_whisk)[1:] - 1).tolist()
            num2pfr = \
                (np.asarray(end2pfr) - np.asarray(start2pfr[:-1])).tolist()

            max_num2pfr = np.max(num2pfr).astype(int)
            if n_2pfr is None:
                end2pfr.append(start2pfr + max_num2pfr)
                num2pfr.append(max_num2pfr)
            else:
                end2pfr.append(n_2pfr)
                num2pfr.append(n_2pfr - start2pfr[-1])
                if num2pfr[-1] > max_num2pfr * 1.5:
                    raise ValueError(
                        'Calculation leads to unexpected number '
                        'of frames for the final trial.')
        elif len(self.motor_whisk) > len(self.trial_df):
            n_excess = len(self.motor_whisk) - len(self.trial_df)
            start2pfr = self.motor_whisk[:-n_excess]
            end2pfr = (np.asarray(self.motor_whisk)[1:] - 1).tolist()
            num2pfr = (np.asarray(end2pfr) - np.asarray(start2pfr)).tolist()
            if n_excess > 1:
                print('    WARNING: {} additional trials recorded'.format(
                    n_excess))
        else:
            raise ValueError('All trials were not recorded.')

        self.trial_df['start2pfr'] = start2pfr
        self.trial_df['end2pfr'] = end2pfr
        self.trial_df['num2pfr'] = num2pfr

        self.stim_pos = sorted(self.trial_df['stim_pos'].unique().tolist())


    #############################################
    def _get_mat_data(self):
        """
        self._get_mat_data()

        Sets the attributes for a specific session area based on the mouse data
        file.

        Also calls self._make_df()

        Sets attributes:
            - licks (1D array)         : 2p frames at which each lick started
                                        (None if task is passive)
            - motor_origin (1D array)  : 2p frames at which motor arrived at
                                         origin
            - motor_lin_back (1D array): 2p frames at which motor went back to
                                         origin
            - motor_start (1D array)   : 2p frames at which motor started
                                         toward whisk position
            - motor_whisk (1D array)   : 2p frames at which motor arrived at
                                         whisk position
            - n_trials (int)           : number of trials
            - nogo_pos (int)           : NO-GO bar position  (49 if task is
                                         passive)
            - run (1D array)           : run velocity for each 2p frame
            - trial_outc (list)        : outcome of each trial ('correct',
                                         'correct_rejection', 'false_positive',
                                         'missed', 'too_soon')  (None if task
                                         is passive)
            - water_on (1D array)      : 2p frames at which each water onset
                                         occurred (None if task is passive)
        """

        date  = mult_mat_util.make_date_key(self.date)
        area  = mult_mat_util.make_area_key(self.area)

        with h5py.File(self.datapath, 'r') as f:
            [self.motor_origin, _, self.motor_start, self.motor_whisk,
                self.motor_lin_back] = mult_mat_util.get_motor_arr(
                    f, date, area)
            self.run = mult_mat_util.get_run_velocity(f, date, area)

            if self.prop_go is None:
                self.nogo_pos = None
                self.licks    = None
                self.water_on = None
                stim_pos = mult_mat_util.get_stim_pos(f, date, area)
                self.n_trials = np.min([len(self.motor_whisk), len(stim_pos)])
                stim_pos = stim_pos[: self.n_trials]
                self.all_trial_outc = None
            else:
                self.nogo_pos = mult_mat_util.get_nogo_pos(f, date, area)
                self.licks    = mult_mat_util.get_licks(f, date, area)
                self.water_on = mult_mat_util.get_water_on(f, date, area)
                self.all_trial_outc = np.asarray(
                    mult_mat_util.get_trial_outc(f, date, area))
                self.n_trials = np.min(
                    [len(self.motor_whisk), len(self.all_trial_outc)])
                self.all_trial_outc = self.all_trial_outc[: self.n_trials]
                stim_pos = None
                # replace names
                orig = ['correct', 'correct_rejection', 'false_positive']
                new  = ['corr_acc', 'corr_rej', 'false_pos']
                for o, n in zip(orig, new):
                    self.all_trial_outc[np.where(self.all_trial_outc == o)] = n

        self._make_trial_df(
            trial_outc=self.all_trial_outc, stim_pos=stim_pos,
            n_2pfr=len(self.run), go_pos=self.go_pos, nogo_pos=self.nogo_pos)


    #############################################
    def _format_trial_criteria(self, trial_n='any', stim_pos='any',
                               outcome='any', start2pfr='any', end2pfr='any',
                               num2pfr='any'):
        """
        self._format_trial_criteria()

        Returns a list of trial parameters formatted correctly to use
        as criteria when searching through the trial dataframe.

        Will strip criteria not related to the current sessarea object.

        Optional args:
            - trial_n (str, int or list) : trial number value(s) of interest
                                           default: 'any'
            - stim_pos (str, int or list): stim_pos value(s) of interest
                                           default: 'any'
            - outcome (str, int or list) : outcome value(s) of interest,
                                           e.g., 'corr_acc', 'corr_rej',
                                                 'missed', 'false_pos',
                                                 'too_soon'
                                           default: 'any'
            - start2pfr (str or list)    : 2p start frames range of interest
                                           [min, max (excl)]
                                           default: 'any'
            - end2pfr (str or list)      : 2p end frames range of interest
                                           [min, max (excl)]
                                           default: 'any'
            - num2pfr (str or list)      : 2p num frames range of interest
                                           [min, max (excl)]
                                           default: 'any'

        Returns:
            - trial_n (list)     : trial number value(s) of interest
            - stim_pos (list)    : stim_pos value(s) of interest
            - outcome (list)     : outcome value(s) of interest,
            - start2pfr_min (int): minimum of 2p start2pfr range of interest
            - start2pfr_max (int): maximum of 2p start2pfr range of interest
                                   (excl)
            - end2pfr_min (int)  : minimum of 2p end2pfr range of interest
            - end2pfr_max (int)  : maximum of 2p end2pfr range of interest
                                   (excl)
            - num2pfr_min (int)  : minimum of num2pfr range of interest
            - num2pfr_max (int)  : maximum of num2pfr range of interest
                                   (excl)
        """

        # remove brick criteria for gabors and vv
        if self.task == 'sens_stim':
            outcome = 'any'

        if outcome == 'correct':
            outcome = ['corr_acc', 'corr_rej']
        elif outcome == 'wrong':
            outcome = ['missed', 'false_pos']

        # converts values to lists or gets all possible values, if 'any'
        stim_pos = gen_util.get_df_label_vals(
            self.trial_df, 'stim_pos', stim_pos)
        outcome  = gen_util.get_df_label_vals(
            self.trial_df, 'outcome', outcome)
        trial_n  = gen_util.get_df_label_vals(
            self.trial_df, 'trial_n', trial_n)

        if start2pfr in ['any', None]:
            start2pfr_min = int(self.trial_df['start2pfr'].min())
            start2pfr_max = int(self.trial_df['start2pfr'].max()+1)
        if end2pfr in ['any', None]:
            end2pfr_min = int(self.trial_df['end2pfr'].min())
            end2pfr_max = int(self.trial_df['end2pfr'].max()+1)
        if num2pfr in ['any', None]:
            num2pfr_min = int(self.trial_df['num2pfr'].min())
            num2pfr_max = int(self.trial_df['num2pfr'].max()+1)

        return [trial_n, stim_pos, outcome, start2pfr_min, start2pfr_max,
            end2pfr_min, end2pfr_max, num2pfr_min, num2pfr_max]


    #############################################
    def get_go_nogo(self, pos='go'):
        """
        self.get_go_nogo()

        Returns the request position (go/nogo).

        Optional args:
            - pos (str): requested position ('go' or 'nogo')
                         default: 'go'

        Returns:
            - pos (int): requested position
        """

        if pos == 'go':
            pos = self.go_pos
        elif pos == 'nogo':
            pos = self.nogo_pos
        else:
            gen_util.accepted_values_error('pos', pos, ['go', 'nogo'])

        return pos


    #############################################
    def get_plane(self, plane=1):
        """
        self.get_plane()

        Returns the requested plane.

        Optional args:
            - plane (int or str): plane

        Returns:
            - ret_pl (Plane): requested plane
        """

        if int(plane) == 1:
            ret_pl = self.plane_1
        elif int(plane) == 2:
            ret_pl = self.plane_2
        else:
            raise ValueError('No plane {}.'.format(plane))

        return ret_pl


    #############################################
    def set_plane_oi(self, plane_oi='both'):
        """
        self.set_plane_oi()

        Sets attributes:
            - plane_oi (int or str): number of the plane to be analysed (for
                                     user to keep track. Not used internally.)
                                     default: 'both'

        Optional args:
            - plane_oi (int): plane
        """

        if plane_oi == 'both':
            self.plane_oi = plane_oi
        elif int(plane_oi) in [1, 2]:
            self.plane_oi = int(plane_oi)
        else:
            raise ValueError('No plane {}.'.format(plane_oi))


    ############################################
    def get_trials_by_criteria(self, trial_n='any', stim_pos='any',
                               outcome='any', start2pfr='any', end2pfr='any',
                               num2pfr='any', remconsec=False):
        """
        self.trials_by_criteria()

        Returns a list of trial numbers that have the specified values
        in specified columns in the trial dataframe.

        Optional args:
            - trial_n (str, int or list)   : trial number value(s) of interest
                                             default: 'any'
            - stim_pos (str, int or list)  : stim_pos value(s) of interest
                                             default: 'any'
            - outcome (str, int or list)   : outcome value(s) of interest,
                                             e.g., 'corr_acc', 'corr_rej',
                                                   'missed', 'false_pos',
                                                   'too_soon'
                                             default: 'any'
            - start2pfr (str or list)      : 2p start frames range of interest
                                             [min, max (excl)]
                                             default: 'any'
            - end2pfr (str or list)        : 2p end frames range of interest
                                             [min, max (excl)]
                                             default: 'any'
            - num2pfr (str or list)        : 2p num frames range of interest
                                             [min, max (excl)]
                                             default: 'any'
            - remconsec (bool)             : if True, consecutive segments are
                                             removed within a block
                                             default: False

        Returns:
            - trial_ns (list): list of trial numbers that obey the criteria
        """

        pars = self._format_trial_criteria(
            trial_n, stim_pos, outcome, start2pfr, end2pfr, num2pfr)

        [trial_n, stim_pos, outcome, start2pfr_min, start2pfr_max,
            end2pfr_min, end2pfr_max, num2pfr_min, num2pfr_max] = pars

        trial_ns = self.trial_df.loc[
            (self.trial_df['trial_n'].isin(trial_n))      &
            (self.trial_df['stim_pos'].isin(stim_pos))    &
            (self.trial_df['outcome'].isin(outcome))      &
            (self.trial_df['start2pfr'] >= start2pfr_min) &
            (self.trial_df['start2pfr'] < start2pfr_max)  &
            (self.trial_df['end2pfr'] >= end2pfr_min)     &
            (self.trial_df['end2pfr'] < end2pfr_max)      &
            (self.trial_df['num2pfr'] >= num2pfr_min)     &
            (self.trial_df['num2pfr'] < num2pfr_max)]['trial_n'].tolist()

        # if removing consecutive values
        if remconsec:
            trial_ns_new = []
            for k, val in enumerate(trial_ns):
                if k == 0 or val != trial_ns[k-1]+1:
                    trial_ns_new.extend([val])
            trial_ns = trial_ns_new
        # check for empty
        if len(trial_ns) == 0:
             raise ValueError('No trials fit these criteria.')

        return trial_ns


#############################################
#############################################
class Plane(object):
    """
    The Plane object is a higher level class for describing plane properties.
    """

    def __init__(self, sessarea, n, mouse_df='mouse_df.csv'):
        """
        self.__init__(sessarea, n)

        Initializes and returns the new Plane object for the specified session
        area.

        Also calls:
            self._get_mat_data()

        Sets attributes:
            - depth (int)         : depth aimed at (None if no 2p
                                    recording)
            - depth_act (int)     : actual recording depth (None if no 2p
                                    recording)
            - incl (int)          : whether plane is to be used (0 or 1)
            - info (str)          : information recorded on the plane
            - layer (str)         : cortical layer (e.g., 'L2', 'L3') (None if
                                    no 2p recording)
            - level (str)         : level within layer (e.g., 'shallow', 'mid',
                                    'deep') (None if no 2p recording)
            - nrois (int)         : number of ROIs extracted (None if no 2p
                                    recording)
            - plane_n (int)       : plane number (1 or 2)
            - sessarea (SessArea) : SessArea object
            - usable (int)        : whether plane is usable (0 or 1)

        Required args:
            - sessarea (SessArea): SessArea object
            - n (int)            : plane number (1 or 2)

        Optional args:
            - mouse_df (str)     : path name of dataframe containing
                                   information on each recorded session.
                                   Dataframe should have the following
                                   columns:
                                        mouseid, date, plane_n, depth_n,
                                        layer_n, level_n, nrois_n, info_n,
                                        usable_n, use_n
        """

        if isinstance(mouse_df, str):
            mouse_df  = file_util.loadfile(mouse_df)

        df_line = gen_util.get_df_vals(
            mouse_df, ['mouseid', 'sess_n', 'area'],
            [sessarea.mouseid, sessarea.sess_n, sessarea.area], single=True)

        self.sessarea = sessarea
        self.plane_n  = n

        plane  = df_line['plane_{}'.format(n)].tolist()[0]
        if not np.isnan(plane):
            self.depth_act = int(plane)
            self.depth     = int(df_line['depth_{}'.format(n)].tolist()[0])
            nrois = df_line['nrois_{}'.format(n)].tolist()[0]
            if not np.isnan(nrois):
                self.nrois = int(nrois)
            else:
                self.nrois = None
            self.layer     = df_line['layer_{}'.format(n)].tolist()[0]
            self.level     = df_line['level_{}'.format(n)].tolist()[0]

        else:
            self.depth     = None
            self.depth_act = None
            self.nrois     = None
            self.layer     = None
            self.level     = None

        self.usable = int(df_line['usable_{}'.format(n)].tolist()[0])
        self.incl   = int(df_line['incl_{}'.format(n)].tolist()[0])

        self.info   = df_line['info_{}'.format(n)].tolist()[0]

        self._get_mat_data()


    #############################################
    def _get_mat_data(self):
        """
        self._get_mat_data()

        Sets the attributes for a specific plane based on the mouse data file
        and calls self._set_stim_leng()

        Sets attributes:
            - twop_fps (float)       : 2p frames per second
            - roipos (2D array)      : ROI pixel positions, structured as
                                       coordinates (x, y?) x ROI
                                       (None if no 2p recording)
            - fluo_raw (2D array)    : raw fluorescence values for each
                                       frame, structured as ROI x frame
                                       (None if no 2p recording)
            - fluo_corr (2D array)   : corrected fluorescence values for each
                                       frame, structured as ROI x frame
                                       (None if no 2p recording)
            - neuropil_raw (2D array): raw neuropil values for each
                                       frame, structured as ROI x frame
                                       (None if no 2p recording)
            - fluo_trbytr (list)     : fluorescence values for each trial,
                                       each structured as ~200 x 35 (???)
                                       (None if no 2p recording)
        """

        if self.nrois is None:
            self.twop_fps     = None
            self.roipos       = None
            self.fluo_raw     = None
            self.fluo_corr    = None
            self.neuropil_raw = None
            self.fluo_trbytr  = None
            self.tot_twop_fr = None

        else:
            date  = mult_mat_util.make_date_key(self.sessarea.date)
            area  = mult_mat_util.make_area_key(self.sessarea.area)
            plane = mult_mat_util.make_plane_key(self.plane_n)

            with h5py.File(self.sessarea.datapath, 'r') as f:
                self.twop_fps     = mult_mat_util.get_fps(f, date, area, plane)
                self.roipos       = mult_mat_util.get_roipos(
                                        f, date, area, plane)
                self.fluo_raw     = mult_mat_util.get_fluo_raw(
                                        f, date, area, plane).T
                self.fluo_corr    = mult_mat_util.get_fluo_corr(
                                        f, date, area, plane).T
                self.neuropil_raw = mult_mat_util.get_neuropil_raw(
                                        f, date, area, plane).T
                self.fluo_trbytr  = mult_mat_util.get_fluo_trialbytrial(
                                        f, date, area, plane)
                self.tot_twop_fr  = self.fluo_corr.shape[1]
        self._set_stim_leng()


    #############################################
    def _set_stim_leng(self):
        """
        self._set_stim_leng()

        Sets the following attribute:
        - stim_leng (1D array):  average length of stimulus presentations
                                 (None if self.twop_fps is None)
        """

        if self.twop_fps is None:
            self.stim_leng = None

        else:
            n_fr = min(
                [len(self.sessarea.motor_lin_back),
                len(self.sessarea.motor_whisk)])

            self.stim_leng = np.mean((self.sessarea.motor_lin_back[ : n_fr] - \
                self.sessarea.motor_whisk[: n_fr])/self.twop_fps)
        return


    #############################################
    def _check_rois(self):
        """
        self._check_rois()

        Throws an error if the plane has no ROIs.
        """

        if self.nrois is None:
            raise ValueError('Plane does not have ROIs.')


    ##############################################
    def set_outc_resp_rois(self, pre=1.0, post=1.0, p_thr=0.01,
                           ginit='single_fit', replace=False):
        """
        self.set_outc_resp_rois()

        Identifies ROIs that are responsive to specific outcomes, based on
        denoised traces.

        Sets attribute:
            - outc_resp_rois (2D array)    : bool array indicating which ROIs
                                             are responsive to which stimuli,
                                             structured as
                                                ROI x stim positions
            - outc_resp_roi_sign (2D array): array indicating sign (-1/1) of
                                             change in activity locked to
                                             stimulus onset for each ROI
                                             (0 if no trials),
                                             structured as
                                                 ROI x stim positions

        Optional args:
            - pre (num)     : number of seconds to take before trial onset
                              (None will set to default)
                              default: 1.0
            - post (num)    : number of seconds to take after trial onset
                              (None will set to default)
                              default: 1.0
            - p_thr (float) : p value threshold for paired t-test (multiple
                              comparisons not done, 2-tailed)
                              default: 0.01
            - ginit (str)   : OASIS fit parameter
                              default: 'single_fit'
            - replace (bool): if replace is True, existing outcome responsive
                              ROIs are replaced
        """

        if pre is None:
            pre = 1
        if post is None:
            post = 1

        ch_fl = [pre, post]

        if self.sessarea.task == 'sens_stim':
            raise ValueError('No outcomes in a sensory stimulation session.')

        if hasattr(self, 'outc_resp_rois'):
            if replace:
                print(('    Replacing outcome responsive ROI attribute for '
                    'plane {}.').format(self.plane_n))
            else:
                return

        self.denoise_plane(ginit=ginit)

        self.outc_resp_rois = np.zeros(
            [self.nrois, len(self.sessarea.trial_outc)], dtype=bool)
        self.outc_resp_roi_sign = np.zeros(
            [self.nrois, len(self.sessarea.trial_outc)], dtype=int)
        for t, outc in enumerate(self.sessarea.trial_outc):
            trials = self.sessarea.get_trials_by_criteria(outcome=outc)
            twop_ref_fr = self.get_twop_fr_by_trial(
                trials, first=True, ch_fl=ch_fl)
            if len(twop_ref_fr) == 0:
                continue
            # ROI x trials
            pre_data  = self.get_roi_trace_array(
                twop_ref_fr, pre, 0, integ=True,
                trace_type='denoised')[1]/float(pre)
            post_data = self.get_roi_trace_array(
                twop_ref_fr, 0, post, integ=True,
                trace_type='denoised')[1]/float(post)
            for r in range(len(pre_data)): # 2-tailed
                self.outc_resp_roi_sign[r, t] = np.sign(
                    np.mean(post_data[r] - pre_data[r]))
                if len(trials) > 1:
                    _, p_val = st.ttest_rel(pre_data[r], post_data[r])
                    self.outc_resp_rois[r, t]     = p_val < p_thr


    ##############################################
    def set_resp_rois(self, pre=1, post=1, p_thr=0.01, ginit='single_fit',
                      replace=False):
        """
        self.set_resp_rois()

        Identifies ROIs that are responsive to specific stimulus positions,
        based on denoised traces.

        Sets attribute:
            - resp_rois (2D array)    : bool array indicating which ROIs are
                                        responsive to which stimuli,
                                        structured as
                                            ROI x stim positions
            - resp_roi_sign (2D array): array indicating sign (-1/1) of change
                                        in activity locked to stimulus onset
                                        for each ROI, (0 if no trials),
                                        structured as
                                            ROI x stim positions

        Optional args:
            - pre (num)     : number of seconds to take before trial onset
                              (None will set to default)
                              default: 1
            - post (num)    : number of seconds to take after trial onset
                              (None will set to default)
                              default: 1
            - p_thr (float) : p value threshold for paired t-test (multiple
                              comparisons not done, 2-tailed)
                              default: 0.01
            - ginit (str)   : OASIS fit parameter
                              default: 'single_fit'
            - replace (bool): if replace is True, existing responsive ROIs are
                              replaced (e.g., if using new parameters)
                              default: False
        """

        if pre is None:
            pre = 1
        if post is None:
            post = 1

        ch_fl = [pre, post]

        if hasattr(self, 'resp_rois'):
            if replace:
                print(('    Replacing responsive ROI attribute for '
                    'plane {}.').format(self.plane_n))
            else:
                return

        self.denoise_plane(ginit=ginit)

        self.resp_rois = np.zeros(
            [self.nrois, len(self.sessarea.stim_pos)], dtype=bool)
        self.resp_roi_sign = np.zeros(
            [self.nrois, len(self.sessarea.stim_pos)], dtype=int)
        for s, sp in enumerate(self.sessarea.stim_pos):
            trials = self.sessarea.get_trials_by_criteria(stim_pos=sp)
            twop_ref_fr = self.get_twop_fr_by_trial(
                trials, first=True, ch_fl=ch_fl)
            if len(twop_ref_fr) == 0:
                continue
            # ROI x trials
            pre_data  = self.get_roi_trace_array(
                twop_ref_fr, pre, 0, integ=True,
                trace_type='denoised')[1]/float(pre)
            post_data = self.get_roi_trace_array(
                twop_ref_fr, 0, post, integ=True,
                trace_type='denoised')[1]/float(post)
            for r in range(len(pre_data)): # 2-tailed
                self.resp_roi_sign[r, s] = np.sign(np.mean(
                    post_data[r] - pre_data[r]))
                if len(trials) > 1:
                    _, p_val = st.ttest_rel(pre_data[r], post_data[r])
                    self.resp_rois[r, s]     = p_val < p_thr


    ##############################################
    def get_n_resp_rois(self, pre=None, post=None, p_thr=0.01,
                        trial_type='stim_pos', replace=False):
        """
        self.get_n_resp_rois()

        Returns number of ROIs responsive to any stimulus position or
        any trial outcome.

        Calls:
            - self.set_resp_rois()
            or
            - self.set_outc_resp_rois()

        Optional args:
            - pre (num)       : number of seconds to take before trial onset
                                if None, uses the downstream default
                                default: None
            - post (num)      : number of seconds to take after trial onset
                                if None, uses the downstream default
                                default: None
            - p_thr (float)   : p value threshold for paired t-test (multiple
                                comparisons not done)
                                default: 0.01
            - trial_type (str): type of trial to get responsive ROIs for (stim
                                positions or outcomes)
                                default: 'stim_pos'
            - replace (bool)  : if replace is True, existing responsive ROIs
                                are replaced (e.g., if using new parameters)
                                default: False
        """

        if trial_type == 'stim_pos':
            self.set_resp_rois(pre, post, p_thr, replace)
            resp_rois = self.resp_rois

        elif trial_type == 'outcome':
            self.set_outc_resp_rois(pre, post, p_thr, replace)
            resp_rois = self.outc_resp_rois

        else:
            gen_util.accepted_values_error(
                'trial_type', trial_type, ['stim_pos', 'outcome'])

        n_resp_rois = len(np.where(np.sum(resp_rois, axis=1))[0])

        return n_resp_rois


    ##############################################
    def denoise_plane(self, ginit='single_fit'):
        """
        self.denoise_plane()

        Denoise the corrected fluorescence traces for the plane. Denoised
        traces are replaced if `ginit` parameter changes.

        Sets attributes:
            - baselines (1D array): baseline values for each ROI
            - gvalues (list)      : autoregressive model estimated parameters
                                    for each ROI
            - lambdas (1D array)  : optimal La Grange multipliers for each ROI
            - spikes (2D array)   : array of discretized spikes for each frame
                                    (not binary), structured as ROI x frame
            - tau_d (float)       : decay time constant (None if none used to
                                    estimate autocovariance)
            - tau_r (float)       : rising time constant (None if none used to
                                    estimate autocovariance)
            - traces (2D array)   : denoised fluorescence traces, structured as
                                    ROI x frame

        Optional args:
            - ginit (str): OASIS fit parameter
                           default: 'single_fit'
        """

        self._check_rois()

        if ginit == 'double_fix':
            self.tau_r = 45e-3
            self.tau_d = 140e-3
            g1 = np.exp(-1/(self.tau_d * self.twop_fps)) + \
                np.exp(-1/(self.tau_r * self.twop_fps))
            g2 = -np.exp(-1/(self.tau_d * self.twop_fps)) * \
                np.exp(-1/(self.tau_r * self.twop_fps))
        else:
            self.tau_r = None
            self.tau_d = None
            g1, g2 = None, None

        ginits = {'double_fix': (g1, g2),
                  'double_fit': (None, None),
                  'single_fix': (0.95, ),
                  'single_fit': (None, )
                  }

        if not hasattr(self, 'ginit'):
            self.ginit = None
        else:
            if self.ginit != ginit:
                print(('    Replacing {} with {} denoised spikes for '
                    'plane {}').format(self.ginit, ginit, self.plane_n))
            else:
                return

        self.ginit = ginit
        if self.ginit not in ginits.keys():
            gen_util.accepted_values_error('ginit', ginit, ginits.keys())

        denoised  = np.empty_like(self.fluo_corr) # ROI x frames
        spikes    = np.empty_like(self.fluo_corr)
        baselines = np.empty(self.nrois)
        gvalues   = [] # may be tuples for each ROI
        lambdas   = np.empty(self.nrois)
        for roi_n in range(self.nrois): # each ROI
            c, s, b, g, l = deconvolve(
                self.fluo_corr[roi_n, :].astype(np.double),
                g=ginits[self.ginit], penalty=1)
            denoised[roi_n, :] = c
            spikes[roi_n, :]   = s
            baselines[roi_n]   = b
            gvalues.append(g)
            lambdas[roi_n]     = l

        self.denoised  = denoised
        self.spikes    = spikes
        self.baselines = baselines
        self.gvalues   = gvalues
        self.lambdas   = lambdas


    #############################################
    def check_flanks(self, frs, ch_fl, fr_type='twop'):
        """
        self.check_flanks(self, frs, ch_fl)

        Required args:
            - frs (arraylike): list of frames values
            - ch_fl (list)   : flanks in sec [pre sec, post sec] around frames
                               to check for removal if out of bounds

        Optional args:
            - fr_type (str): time of frames ('twop', 'stim')
                             default: 'twop'

        """

        if not isinstance(ch_fl, list) or len(ch_fl) != 2:
            raise ValueError('`ch_fl` must be a list of length 2.')

        fps = self.twop_fps
        max_val = self.tot_twop_fr

        ran_fr = [np.around(x * fps) for x in [-ch_fl[0], ch_fl[1]]]
        frs = np.asarray(frs)

        neg_idx  = np.where((frs + ran_fr[0]) < 0)[0].tolist()
        over_idx = np.where((frs + ran_fr[1]) >= max_val)[0].tolist()
        all_idx = set(range(len(frs))) - set(neg_idx + over_idx)

        if len(all_idx) == 0:
            frs = np.asarray([])
        else:
            frs = frs[np.asarray(sorted(all_idx))]

        return frs


    #############################################
    def get_twop_fr_by_trial(self, triallist, first=False, last=False,
                             ch_fl=None):
        """
        self.get_twop_fr_by_seg(triallist)

        Returns a list of arrays containing the 2-photon frame numbers that
        correspond to a given set of trial numbers provided in a list
        for a specific stimulus.

        Required args:
            - triallist (list of ints): the trial number for which to get
                                        2p frames

        Optional args:
            - first (bool): instead, return first frame for each trial
                            default: False
            - last (bool) : instead return last frame for each trial
                            default: False
            - ch_fl (list): if provided, flanks in sec [pre sec, post sec]
                            around frames to check for removal if out of bounds
                            default: None

        Returns:
            if first and last are True:
                - frames (nested list): list of the first and last 2p frames
                                        numbers for each segment, structured
                                        as [first, last]
            if first or last is True, but not both:
                - frames (list)       : a list of first or last 2p frames
                                        numbers for each segment
            else:
                - frames (list of int arrays): a list (one entry per segment)
                                               of arrays containing the 2p
                                               frame
        """

        # initialize the frames list
        frames = []

        # get the rows in the alignment dataframe that correspond to the segments
        rows = self.sessarea.trial_df.loc[
            (self.sessarea.trial_df['trial_n'].isin(triallist))]

        # get the start frames and end frames from each row
        start2pfrs = rows['start2pfr'].values

        if ch_fl is not None:
            start2pfrs = self.check_flanks(start2pfrs, ch_fl)

        if not first or last:
            end2pfrs = rows['end2pfr'].values

        if not first and not last:
            # build arrays for each segment
            for r in range(start2pfrs.shape[0]):
                frames.append(np.arange(start2pfrs[r], end2pfrs[r]))
        else:
            if first:
                frames.append(start2pfrs)
            if last:
                frames.append(end2pfrs)
            frames = gen_util.delist_if_not(frames)

        return frames


    #############################################
    def get_n_twop_fr_by_trial(self, trials):
        """
        self.get_n_twop_fr_by_trial(trials)

        Returns a list with the number of twop frames for each trial number
        passed.

        Required args:
            - trials (list): list of trial numbers

        Returns:
            - n_fr_sorted (list): list of number of frames in each trial
        """

        trials = gen_util.list_if_not(trials)

        trials_unique = sorted(set(trials))

        # number of frames will be returned in ascending order of trial number
        n_fr = self.sessarea.trial_df.loc[
            (self.sessarea.trial_df['trial_n'].isin(trials_unique))]['num2pfr'].tolist()

        # resort based on order in which trials were passed and include any
        # duplicates
        n_fr_sorted = [n_fr[trials_unique.index(trial)] for trial in trials]

        return n_fr_sorted


    #############################################
    def get_traces(self, trace_type='corr'):
        """
        self.get_traces()

        Returns the requested traces.

        Optional args:
            - trace_type (str): type of traces
                                ('corr' for corrected fluorescence,
                                 'raw' for raw fluorescence,
                                 'denoised' for denoised corrected
                                 fluorescence)
                                default: 'corr'

        Returns:
            - traces (2D array): traces, structured as ROI x frame
        """

        self._check_rois()

        if trace_type == 'corr':
            traces = self.fluo_corr
        elif trace_type == 'raw':
            traces = self.fluo_raw
        elif trace_type == 'denoised':
            if not hasattr(self, 'denoised'):
                raise ValueError('If `trace_type` is `denoised`, first call '
                    '`self.denoise_plane()` on the plane.')
            else:
                traces = self.denoised
        else:
            gen_util.accepted_values_error(
                'trace_type', trace_type, ['corr', 'raw', 'denoised'])

        return traces


    #############################################
    def run_pca(self, trace_type='corr'):
        """
        self.run_pca()

        Runs Principle Component Analysis (PCA) on the specified traces
        and sets the following attributes:
            - pca (PCA) : sklearn PCA on specified traces
            - pca_transf: PCA transformed traces, structured as frames x comp
            - pca_type  : trace type on which the PCA was performed

        Optional args:
            - trace_type (str): type of traces
                                ('corr' for corrected fluorescence,
                                 'raw' for raw fluorescence,
                                 'denoised' for denoised corrected
                                 fluorescence)
                                default: 'corr'
        """

        self._check_rois()

        # frames x ROI
        traces = self.get_traces(trace_type).T

        self.pca_type = trace_type

        self.pca = PCA()
        self.pca.fit(traces)
        self.pca_transf = self.pca.transform(traces)


    #############################################
    def run_nmf(self, trace_type='corr', n_comp=20, rand_st=None, alpha=0.1,
                l1_ratio=0.5):
        """
        self.run_nmf()

        Runs Non Negative Matrix Factorization (NMF) on the specified traces
        and sets the following attributes:
            - nmf (NMF) : sklearn NMF on specified traces
            - nmf_transf: NMF transformed traces, structured as frames x comp
            - nmf_type  : trace type on which the NMF was performed

        Optional args:
            - trace_type (str) : type of traces
                                 ('corr' for corrected fluorescence,
                                  'raw' for raw fluorescence,
                                  'denoised' for denoised corrected
                                  fluorescence)
                                 default: 'corr'
            - n_comp (int)     : number of components to retain. If None,
                                 all are retained
                                 default: 20
            - rand_st (int)    : random state instance or seed
                                 default: None
            - alpha (double)   : regularization parameter
                                 default: 0.1
            - l1_ratio (double): L1 to L2 regularization mixing parameter
                                 default: 0.5
        """

        self._check_rois()

        # frames x ROI
        traces = self.get_traces(trace_type).T

        self.nmf_type = trace_type

        self.nmf = NMF(
            n_components=n_comp, random_state=rand_st, alpha=alpha,
            l1_ratio=l1_ratio)
        self.nmf.fit(traces)
        self.nmf_transf = self.nmf.transform(traces)


    #############################################
    def get_twop_fr_ran(self, twop_ref_fr, pre, post):
        """
        self.get_twop_fr_ran(twop_ref_fr, pre, post)

        Returns an array of 2p frame numbers, where each row is a sequence and
        each sequence ranges from pre to post around the specified reference
        2p frame numbers.

        Required args:
            - twop_ref_fr (list): 1D list of 2p frame numbers
                                  (e.g., all 1st seg frames)
            - pre (num)         : range of frames to include before each
                                  reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)

        Returns:
            - num_ran (2D array): array of frame numbers, structured as:
                                      sequence x frames
            - xran (1D array)   : time values for the 2p frames

        """

        self._check_rois()

        ran_fr = [np.around(x*self.twop_fps) for x in [-pre, post]]
        xran   = np.linspace(-pre, post, int(np.diff(ran_fr)[0]))

        if len(twop_ref_fr) == 0:
            raise ValueError(
                'No frames: frames list must include at least 1 frame.')

        if isinstance(twop_ref_fr[0], (list, np.ndarray)):
            raise ValueError(
                'Frames must be passed as a 1D list, not by block.')

        # get sequences x frames
        fr_idx = gen_util.num_ranges(
            twop_ref_fr, pre=-ran_fr[0], leng=len(xran))

        # remove sequences with negatives or values above total number of stim
        # frames
        neg_idx  = np.where(fr_idx[:,0] < 0)[0].tolist()
        over_idx = np.where(fr_idx[:,-1] >= self.tot_twop_fr)[0].tolist()

        num_ran = gen_util.remove_idx(fr_idx, neg_idx + over_idx, axis=0)

        if len(num_ran) == 0:
            raise ValueError('No frames: All frames were removed from list.')

        return num_ran, xran


    #############################################
    def get_frame_array(self, twop_fr_seqs, padding=(0, 0)):
        """
        self.get_frame_array(twop_fr_seqs)

        Returns the padded frame array for the given twop frame sequences.
        Frames around the start and end of the sequences can be requested by
        setting the padding argument.

        If the sequences are different lengths the array is nan padded

        Required args:
            - twop_fr_seqs (list of arrays): list of arrays of 2p frames,
                                             structured as sequences x frames.
                                             If any frames are out of range,
                                             then NaNs returned.

        Optional args:
            - padding (2-tuple of ints): number of additional 2p frames to
                                         include from start and end of
                                         sequences
                                         default: (0, 0)

        Returns:
            - frames (2D array)  : array of frame numbers for the specified
                                   frame sequences, padded with NaNs,
                                   structured as: sequences x frames
        """

        # extend values with padding
        if padding[0] != 0:
            min_fr = np.asarray([min(x) for x in twop_fr_seqs])
            st_padd = np.tile(
                np.arange(-padding[0], 0), (len(twop_fr_seqs), 1)) + \
                min_fr[:, None]
            twop_fr_seqs = [np.concatenate((st_padd[i], x))
                for i, x in enumerate(twop_fr_seqs)]
        if padding[1] != 0:
            max_fr = np.asarray([max(x) for x in twop_fr_seqs])
            end_padd = np.tile(
                np.arange(1, padding[1] + 1),
                (len(twop_fr_seqs), 1)) + max_fr[:, None]
            twop_fr_seqs = [np.concatenate((x, end_padd[i]))
                for i, x in enumerate(twop_fr_seqs)]
        if padding[0] < 0 or padding[1] < 0:
            raise ValueError('Negative padding not supported.')
        # get length of each padded sequence
        pad_seql = np.array([len(s) for s in twop_fr_seqs])
        # flatten the sequences into one list of frames, removing any sequences
        # with unacceptable frame values (< 0 or > tot_fr)
        frames_flat = np.empty([sum(pad_seql)]) * np.nan
        last_idx = 0
        seq_rem = []
        seq_rem_l = []
        for i in range(len(twop_fr_seqs)):
            if (max(twop_fr_seqs[i]) >= self.tot_twop_fr or
                min(twop_fr_seqs[i]) < 0):
                seq_rem.extend([i])
                seq_rem_l.extend([pad_seql[i]])
            else:
                frames_flat[last_idx: last_idx + pad_seql[i]] = twop_fr_seqs[i]
                last_idx += pad_seql[i]

        # Warn about removed sequences and update pad_seql and twop_fr_seqs
        # to remove these sequences
        if len(seq_rem) != 0:
            print(f'\nSome of the specified frames for sequences {seq_rem} '
                'are out of range so the sequence will not be included.')
            pad_seql = np.delete(pad_seql, seq_rem)
            twop_fr_seqs = np.delete(twop_fr_seqs, seq_rem).tolist()
        # sanity check that the list is as long as expected
        if last_idx != len(frames_flat):
            if last_idx != len(frames_flat) - sum(seq_rem_l):
                raise ValueError((
                    f'Concatenated frame array is {last_idx} long instead of '
                    'expected {}.').format(
                    len(frames_flat - sum(seq_rem_l))))
            else:
                frames_flat = frames_flat[: last_idx]
        # convert to int
        frames = frames_flat.astype(int).reshape(
            len(twop_fr_seqs), max(pad_seql))

        return frames


    #############################################
    def get_roi_seqs(self, twop_fr_seqs, padding=(0,0), trace_type='corr',
                     basewin=1000):
        """
        self.get_roi_seqs(twop_fr_seqs)

        Returns the processed ROI traces for the given frames.
        Frames around the start and end of the sequences can be requested by
        setting the padding argument.

        If the sequences are different lengths the array is nan padded

        Required args:
            - twop_fr_seqs (list of arrays): list of arrays of 2p frames,
                                             structured as sequences x frames.
                                             If any frames are out of range,
                                             then NaNs returned.


        Optional args:
            - padding (2-tuple of ints): number of additional 2p frames to
                                         include from start and end of
                                         sequences
                                         default: (0, 0)
            - trace_type (str)         : type of traces
                                         ('corr' for corrected fluorescence,
                                          'raw' for raw fluorescence,
                                          'denoised' for denoised corrected
                                           fluorescence)
                                          default: 'corr'
            - basewin (int)            : window length for calculating baseline
                                         fluorescence
                                         default: 1000

        Returns:
            - traces (3D array): array of traces for the specified
                                 ROIs and sequences, structured as:
                                 ROIs x sequences x frames
        """

        self._check_rois()

        frames = self.get_frame_array(twop_fr_seqs, padding=padding)

        # load the traces
        full_traces = self.get_traces(trace_type)
        traces_flat = full_traces[:, frames.reshape(-1)]

        # chop back up into sequences padded with Nans
        traces = np.full((self.nrois, *frames.shape), np.nan)

        last_idx = 0
        pad_seql = np.sum(np.isfinite(frames), axis=1)
        for i in range(frames.shape[0]):
            traces[:, i, :pad_seql[i]] = \
                traces_flat[:, last_idx:last_idx + pad_seql[i]]
            last_idx += pad_seql[i]

        return traces


    #############################################
    def get_lick_seqs(self, twop_fr_seqs, padding=(0, 0)):
        """
        self.get_lick_seqs(twop_fr_seqs)

        Returns the lick traces for the given frames.
        Frames around the start and end of the sequences can be requested by
        setting the padding argument.
        If the sequences are different lengths the array is nan padded

        Required args:
            - twop_fr_seqs (list of arrays): list of arrays of 2p frames,
                                             structured as sequences x frames.
                                             If any frames are out of range,
                                             then NaNs returned.
        Optional args:
            - padding (2-tuple of ints): number of additional 2p frames to
                                         include from start and end of
                                         sequences
                                         default: (0, 0)
        Returns:
            - traces (2D array): array of lick traces for the specified
                                 sequences, structured as:
                                 sequences x frames
        """

        frames = self.get_frame_array(twop_fr_seqs, padding=padding)

        # create the traces
        lick_binary = np.zeros(self.tot_twop_fr)
        lick_binary[self.sessarea.licks] = 1

        traces_flat = lick_binary[frames.reshape(-1)]

        # chop back up into sequences padded with Nans
        traces = np.full(frames.shape, np.nan)

        last_idx = 0
        pad_seql = np.sum(np.isfinite(frames), axis=1)
        for i in range(len(twop_fr_seqs)):
            traces[i, :pad_seql[i]] = \
                traces_flat[last_idx:last_idx + pad_seql[i]]
            last_idx += pad_seql[i]

        return traces


    #############################################
    def get_run_velocity_seqs(self, twop_fr_seqs, padding=(0, 0)):
        """
        self.get_run_velocity_seqs(twop_fr_seqs)

        Returns the run velocity traces for the given frames.
        Frames around the start and end of the sequences can be requested by
        setting the padding argument.
        If the sequences are different lengths the array is nan padded.

        Required args:
            - twop_fr_seqs (list of arrays): list of arrays of 2p frames,
                                             structured as sequences x frames.
                                             If any frames are out of range,
                                             then NaNs returned.

        Optional args:
            - padding (2-tuple of ints): number of additional 2p frames to
                                         include from start and end of
                                         sequences
                                         default: (0, 0)
        Returns:
            - traces (2D array): array of running traces for the specified
                                 sequences, structured as:
                                 sequences x frames
        """

        frames = self.get_frame_array(twop_fr_seqs, padding=padding)

        # create the traces
        traces_flat = self.sessarea.run[frames.reshape(-1)]

        # chop back up into sequences padded with Nans
        traces = np.full(frames.shape, np.nan)

        last_idx = 0
        pad_seql = np.sum(np.isfinite(frames), axis=1)
        for i in range(len(twop_fr_seqs)):
            traces[i, :pad_seql[i]] = \
                traces_flat[last_idx:last_idx + pad_seql[i]]
            last_idx += pad_seql[i]

        return traces


    #############################################
    def get_array_stats(self, xran, data, ret_arr=False, axes=0, stats='mean',
                        error='std', integ=False):
        """
        self.get_array_stats(xran, data)

        Returns stats (mean and std or median and quartiles) for arrays of
        running or roi traces. If sequences of unequal length are passed, they
        are cut down to the same length, including xran.

        Required args:
            - xran (array-like)  : x values for the frames
            - data (nested lists): list of datapoints, (no more than 3 dim),
                                   structured as
                                       dim x (dim x) (frames if not integ)

        Optional args:
            - ret_arr (bool)    : also return data array, not just statistics
                                  default: False
            - axes (int or list): axes along which to  take statistics. If a
                                  list is passed.
                                  If None, axes are ordered reverse
                                  sequentially (-1 to 1).
                                  default: 0
            - stats (str)       : return mean ('mean') or median ('median')
                                  default: 'mean'
            - error (str)       : return std dev/quartiles ('std') or SEM/MAD
                                  ('sem')
                                  default: 'sem'
            - integ (bool)      : if True, data is integrated across frames

        Returns:
            - xran (array-like)         : x values for the frames
                                          (length is equal to last data
                                          dimension)
            - data_stats (2 to 3D array): array of data statistics, structured
                                          as: stats [me, err] (x dim) x frames
            if ret_arr, also:
            - data_array (2 to 3D array): data array, structured as:
                                              dim x (dim x) frames
        """

        if integ:
            data_array = np.asarray(data)

        # find minimum sequence length and cut all sequences down
        else:
            all_leng = []
            for subdata in data:
                if isinstance(subdata[0], (list, np.ndarray)):
                    for subsub in subdata:
                        all_leng.append(len(subsub))
                else:
                    all_leng.append(len(subdata))

            len_fr = np.min(all_leng + [len(xran)])
            xran   = xran[:len_fr]

            if np.all(all_leng == len_fr):
                data_array = np.asarray(data)
            else:
                # cut data down to minimum sequence length
                data_array = []
                for subdata in data:
                    if isinstance(subdata[0], (list, np.ndarray)):
                        sub_array = []
                        for subsub in subdata:
                            sub_array.append(subsub[:len_fr])
                        data_array.append(sub_array)
                    else:
                        data_array.append(subdata[:len_fr])
                data_array = np.asarray(data_array)

        data_stats = math_util.get_stats(data_array, stats, error, axes=axes)

        if ret_arr:
            return xran, data_stats, data_array
        else:
            return xran, data_stats


    #############################################
    def get_roi_trace_array(self, twop_ref_fr, pre, post, trace_type='corr',
                            integ=False, baseline=None, stats='mean',
                            resp_rois=False):
        """
        self.get_roi_trace_array(twop_ref_fr, pre, post)

        Returns an array of 2p trace data around specific 2p frame numbers.

        Required args:
            - twop_ref_fr (list): 1D list of 2p frame numbers
                                  (e.g., all 1st Gabor A frames)
            - pre (num)         : range of frames to include before each
                                  reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)

        Optional args:
            - trace_type (str) : type of traces
                                 ('corr' for corrected fluorescence,
                                  'raw' for raw fluorescence,
                                  'denoised' for denoised corrected
                                  fluorescence)
                                 default: 'corr'
            - integ (bool)     : if True, dF/F is integrated over frames
                                 default: False
            - baseline (num)   : number of seconds from beginning of
                                 sequences to use as baseline. If None, data
                                 is not baselined.
                                 default: None
            - stats (str)      : statistic to use for baseline, mean ('mean')
                                 or median ('median')
                                 default: 'mean'
            - resp_rois (bool) : if True, only responsive ROIs are retained
                                 (responsive to any stim position)
                                 default: False

        Returns:
            - xran (1D array)           : time values for the 2p frames
            - data_array (2 or 3D array): roi trace data, structured as
                                          ROI x sequences (x frames)
        """

        self._check_rois()

        fr_idx, xran = self.get_twop_fr_ran(twop_ref_fr, pre, post)

        # get dF/F: ROI x seq x fr
        data_array = self.get_roi_seqs(fr_idx, trace_type=trace_type)

        if resp_rois:
            self.set_resp_rois()
            keep_rois = np.where(np.sum(self.resp_rois, axis=1))
            data_array = data_array[keep_rois]

        if baseline is not None: # calculate baseline and subtract
            if baseline > pre + post:
                raise ValueError('Baseline greater than sequence length.')
            baseline_fr = int(np.around(baseline * self.twop_fps))
            baseline_data = data_array[:, :, : baseline_fr]
            data_array_base = math_util.mean_med(
                baseline_data, stats=stats, axis=-1)[:, :, np.newaxis]
            data_array = data_array - data_array_base

        if integ:
            data_array = math_util.integ(data_array, 1./self.twop_fps, axis=2)

        return xran, data_array


    #############################################
    def get_roi_trace_stats(self, twop_ref_fr, pre, post, byroi=True,
                            trace_type='corr', integ=False, ret_arr=False,
                            stats='mean', error='std', baseline=None,
                            resp_rois=False):
        """
        self.get_roi_trace_stats(twop_ref_fr, pre, post)

        Returns stats (mean and std or median and quartiles) for sequences of
        roi traces centered around specific 2p frame numbers.

        Required args:
            - twop_ref_fr (list): 1D list of 2p frame numbers
                                  (e.g., all 1st Gabor A frames)
            - pre (num)         : range of frames to include before each
                                  reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)

        Optional args:
            - byroi (bool)     : if True, returns statistics for each ROI. If
                                 False, returns statistics across ROIs
                                 default: True
            - trace_type (str) : type of traces
                                 ('corr' for corrected fluorescence,
                                  'raw' for raw fluorescence,
                                  'denoised' for denoised corrected
                                  fluorescence)
                                 default: 'corr'
            - integ (bool)     : if True, dF/F is integrated over sequences
                                 default: False
            - ret_arr (bool)   : also return ROI trace data array, not just
                                 statistics.
            - stats (str)      : return mean ('mean') or median ('median')
                                 default: 'mean'
            - error (str)      : return std dev/quartiles ('std') or SEM/MAD
                                 ('sem')
                                 default: 'sem'
            - baseline (num)   : number of seconds from beginning of
                                 sequences to use as baseline. If None, data
                                 is not baselined.
                                 default: None
            - resp_rois (bool) : if True, only responsive ROIs are retained
                                 (responsive to any stim position)
                                 default: False

        Returns:
            - xran (1D array)           : time values for the 2p frames
                                          (length is equal to last data array
                                          dimension)
            - data_stats (1 to 3D array): array of trace data statistics,
                                          structured as:
                                              stats [me, err] (x ROI) (x frames)
            if ret_arr, also:
            - data_array (2 to 3D array): roi trace data array, structured as:
                                            ROIs x sequences (x frames)
        """

        self._check_rois()

        # array is ROI x seq (x fr)
        xran, data_array = self.get_roi_trace_array(
            twop_ref_fr, pre, post, trace_type, integ, baseline=baseline,
            stats=stats, resp_rois=resp_rois)

        # order in which to take statistics on data
        axes = [1, 0]
        if byroi:
            axes = 1

        all_data = self.get_array_stats(
            xran, data_array, ret_arr, axes=axes, stats=stats, error=error,
            integ=integ)

        if ret_arr:
            xran, data_stats, data_array = all_data
            return xran, data_stats, data_array

        else:
            xran, data_stats = all_data
            return xran, data_stats


    #############################################
    def get_lick_array(self, twop_ref_fr, pre, post, integ=False,
                       baseline=None, stats='mean'):
        """
        self.get_lick_array(twop_ref_fr, pre, post)

        Returns an array of boolean values for each specific 2p frame numbers.
        True indicates a lick began at that frame.

        Required args:
            - twop_ref_fr (list): 1D list of 2p frame numbers
                                  (e.g., all 1st Gabor A frames)
            - pre (num)         : range of frames to include before each
                                  reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)
            - integ (bool)      : if True, dF/F is integrated over frames
                                  default: False
            - baseline (num)    : number of seconds from beginning of
                                  sequences to use as baseline. If None, data
                                  is not baselined.
                                  default: None
            - stats (str)       : statistic to use for baseline, mean ('mean')
                                  or median ('median')
                                  default: 'mean'

        Returns:
            - xran (1D array)           : time values for the 2p frames
            - data_array (1 or 2D array): binary lick data, structured as
                                          sequences [x frames]
        """

        fr_idx, xran = self.get_twop_fr_ran(twop_ref_fr, pre, post)
        # get run velocity x seq
        data_array = self.get_lick_seqs(fr_idx)

        if baseline is not None: # calculate baseline and subtract
            if baseline > pre + post:
                raise ValueError('Baseline greater than sequence length.')
            baseline_fr = int(np.around(baseline * self.twop_fps))
            baseline_data = data_array[:, : baseline_fr]
            data_array_base = math_util.mean_med(
                baseline_data, stats=stats, axis=-1)[:, np.newaxis]
            data_array = data_array - data_array_base

        if integ:
            data_array = math_util.integ(data_array, 1./self.twop_fps, axis=1)

        return xran, data_array


    #############################################
    def get_run_velocity_array(self, twop_ref_fr, pre, post, integ=False,
                               baseline=None, stats='mean'):
        """
        self.get_run_velocity_array(twop_ref_fr, pre, post)

        Returns an array of run velocity data around specific 2p frame numbers.

        Required args:
            - twop_ref_fr (list): 1D list of 2p frame numbers
                                  (e.g., all 1st Gabor A frames)
            - pre (num)         : range of frames to include before each
                                   reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)
            - integ (bool)      : if True, run velocity is integrated over
                                  frames
                                  default: False
            - baseline (num)    : number of seconds from beginning of
                                  sequences to use as baseline. If None, data
                                  is not baselined.
                                  default: None
            - stats (str)       : statistic to use for baseline, mean ('mean')
                                  or median ('median')
                                  default: 'mean'

        Returns:
            - xran (1D array)           : time values for the 2p frames
            - data_array (1 or 2D array): run velocity data, structured as
                                          sequences [x frames]
        """

        fr_idx, xran = self.get_twop_fr_ran(twop_ref_fr, pre, post)
        # get run velocity x seq
        data_array = self.get_run_velocity_seqs(fr_idx)

        if baseline is not None: # calculate baseline and subtract
            if baseline > pre + post:
                raise ValueError('Baseline greater than sequence length.')
            baseline_fr = int(np.around(baseline * self.twop_fps))
            baseline_data = data_array[:, : baseline_fr]
            data_array_base = math_util.mean_med(
                baseline_data, stats=stats, axis=-1)[:, np.newaxis]
            data_array = data_array - data_array_base

        if integ:
            data_array = math_util.integ(data_array, 1./self.twop_fps, axis=1)

        return xran, data_array


    #############################################
    def get_roi_fano(self, pre, post, trace_type="corr", rois="all", trial_type = "all"):
        """
        self.get_roi_fano(pre, post, trace_type)

        Returns a list of fano factors for each ROI.

        Required args:
            - pre (num)         : range of frames to include before each
                                   reference frame number (in s)
            - post (num)        : range of frames to include after each
                                  reference frame number (in s)

        Optional args:
            - rois              : a 1d list of ROIs to include. If set to "all", all ROIs will be included.
            - trial_type        : will only look at trials from that type. Can be set to "go", "nogo" and "all".

        Returns:
            - roi_fanos         : a list containing the fano factors for each ROI (starting at ROI=1 and ending at ROI=n)
        """
        # This will get all of corrected 2p data (dF/F) as a 3d array (ROI x trial x time)
        trial_ns = self.sessarea.get_trials_by_criteria()
        twop_ref_fr = self.get_twop_fr_by_trial(trial_ns, first=True)
        frame, data = self.get_roi_trace_array(twop_ref_fr, pre=pre,
                                                post=post, trace_type=trace_type)

        stim_pos = self.sessarea.trial_df['stim_pos']
        stim_pos = ["go" if position == self.sessarea.go_pos else "nogo" for position in stim_pos]
        go_trials = [i for i, pos in enumerate(stim_pos) if pos == "go"]
        nogo_trials = [i for i, pos in enumerate(stim_pos) if pos == "nogo"]
        if trial_type == "go":
            data = np.delete(data, nogo_trials, axis=1)
        elif trial_type == "nogo":
            data = np.delete(data, go_trials, axis=1)
        elif trial_type == "all":
            data = data
        else:
            print(f"Unknown argument - trial_type={trial_type}. Showing results for trial_type='all'")
            data=data

        # NB: Axis0 = ROI, Axis1 = Trials, Axis2 = Timepoints
        if rois == "all":
            nrois = [i for i in range(1, data.shape[0])]
        elif isinstance(rois, (np.ndarray, list)):
            nrois = rois
        roi_fanos = []
        for p in nrois:
            roi = data[p, :, :]  # 2d array of dF/F trial x frame
            roi_mean_over_time = np.mean(roi, axis=1)  # 1d list of mean df/F at each trial
            fano = np.var(roi_mean_over_time) / np.mean(roi_mean_over_time)
            roi_fanos.append(fano)

        return roi_fanos