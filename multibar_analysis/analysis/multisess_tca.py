import sys
import os
import copy
import sklearn
import gcmi
import warnings
import matplotlib

import numpy as np
import tensorflow as tf
import tensortools as tt
import pandas as pd
import seaborn as sns

sys.path.extend(['.', '../'])
matplotlib.use('Qt5Agg')
from analysis import sessarea
from mult_util import mult_gen_util
from util import math_util
from scipy.stats import pointbiserialr
from matplotlib import pyplot as plt
from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


#We will assaign our path for our imaging data and mouse data
datadir = os.path.join(
    'C:/University_Work/colmdowling/multibar_analysis/data')
mouse_df = 'C:/University_Work/colmdowling/multibar_analysis/mouse_df_new.csv'

n_blocks = 2            # The number of blocks we want to compare (max 4)
rank = 16               # The number of components to be computed in the TCA

# We can define our plane number, and the timeframe (pre/post stimulus) that we are interested in.
plane_n = 1
pre = 1
post = 1.5

# We can use the info from out mouse data file to select session/area blocks to analyse (up to 4)
# m = mouse_n           s = session           a = area
m1 = 3
s1 = 15
a1 = 1

m2 = 3
s2 = 22
a2 = 1

m2 = 0
s2 = 0
a2 = 0

m2 = 0
s2 = 0
a2 = 0




########################################################################################################################

# COMPARING MULTIPLE TESTING BLOCKS
print("__COMPARING MULTIPLE TESTING BLOCKS__")


ranklen = [i for i in range(1,rank+1)]     # This returns us a sequence of the number of output components from the TCA

# We will combine data across two stages and run a CP ALS Tensor Decomp on the whole data.
# Hopefully this will reveal dynamics between testing blocks (e.g. does a mouse lose attention over time)
def multi_sess_tensor_2(m1,s1,a1,m2,s2,a2,plane_n=1, pre=1, post=1.5):
    sess1 = sessarea.SessArea(datadir, area=a1, mouse_n=m1, sess_n=s1, mouse_df=mouse_df)
    sess2 = sessarea.SessArea(datadir, area=a2, mouse_n=m2, sess_n=s2, mouse_df=mouse_df)

    plane1 = sess1.get_plane(plane_n)
    trial_ns = sess1.get_trials_by_criteria()
    twop_ref_fr = plane1.get_twop_fr_by_trial(trial_ns, first=True)
    frame1, data1 = plane1.get_roi_trace_array(twop_ref_fr, pre=pre,
                                            post=post, trace_type="corr")

    plane2 = sess2.get_plane(plane_n)
    trial_ns = sess2.get_trials_by_criteria()
    twop_ref_fr = plane2.get_twop_fr_by_trial(trial_ns, first=True)
    frame2, data2 = plane2.get_roi_trace_array(twop_ref_fr, pre=pre,
                                               post=post, trace_type="corr")

    multi_sess_data = np.concatenate((data1,data2),axis=1)
    multi_sess_tensordata = tf.convert_to_tensor(multi_sess_data)
    return(multi_sess_tensordata)


tensordata = multi_sess_tensor_2(3,15,1,3,22,1)
print(tensordata.shape)
# We fit the model from 2 initial conditions to ensure the initial guess doesn't have too much weight on the solution.
# If the models have a poor similarity we can change the random_state seed variable to generate new models.
tca1 = tt.cp_als(tensordata, rank=rank, random_state=1, max_iter=500, verbose=False)
tca2 = tt.cp_als(tensordata, rank=rank, random_state=2, max_iter=500, verbose=False)

# Compare the low-dimensional factors from the two fits.
fig, _, _ = tt.plot_factors(tca1.factors, plots=['bar','scatter','line'])
tt.plot_factors(tca2.factors, plots=['bar','scatter','line'], fig=fig)
fig.suptitle("raw models")
fig.tight_layout()

# Align the two fits and print a similarity score.
sim = tt.kruskal_align(tca1.factors, tca2.factors, permute_U=True, permute_V=True)
#print(f'Plane {sess.plane_oi} TCA similarity score - {sim}')

# Plot the results again to see alignment.
fig, ax, po = tt.plot_factors(tca1.factors, plots=['bar','scatter','line'])
tt.plot_factors(tca2.factors, plots=['bar','scatter','line'], fig=fig)
fig.suptitle("aligned models")
fig.tight_layout()

# We can uncomment the line below if we want to show the aligned plots.
#plt.show()

# Extract the factors and assaign them so we can plot them individually
neuron_fac = tca1.factors[0].T
trial_fac = tca1.factors[1].T
time_fac = tca1.factors[2].T




########################################################################################################################

# GENERATING PLOTS
print("__GENERATING PLOTS__")
# We will make a new window type with scrollable bars. This will mean we can plot many subplots without squishing.
class ScrollableWindow(QtWidgets.QMainWindow):
    def __init__(self, fig):
        self.qapp = QtWidgets.QApplication([])

        QtWidgets.QMainWindow.__init__(self)
        self.widget = QtWidgets.QWidget()
        self.setCentralWidget(self.widget)
        self.widget.setLayout(QtWidgets.QVBoxLayout())
        self.widget.layout().setContentsMargins(0,0,0,0)
        self.widget.layout().setSpacing(0)

        self.fig = fig
        self.canvas = FigureCanvas(self.fig)
        self.canvas.draw()
        self.scroll = QtWidgets.QScrollArea(self.widget)
        self.scroll.setWidget(self.canvas)

        self.nav = NavigationToolbar(self.canvas, self.widget)
        self.widget.layout().addWidget(self.nav)
        self.widget.layout().addWidget(self.scroll)

        self.show()
        self.qapp.exec_()

warnings.filterwarnings("ignore", message="Adding an axes using the same")
plt.close('all')


# Here we will plot our principle components
fig_a = plt.figure(figsize=(9,1.5*rank))
fig_a.suptitle("Multiple Session TCA", fontsize=14)
plt.subplots_adjust(wspace=0.3, hspace=0.1)
plt.subplot(rank,3,1)
plt.title("Neuron Factor")
plt.subplot(rank,3,2)
plt.title("Time Factor")
plt.subplot(rank,3,3)
plt.title("Trial Factor")
for p in ranklen:                         # For each of our principle components we generate a graph of the factors
    n=((p-1)*3)+1                         # Each subplot wil have a unique position in an array of numbers (1,2,3,4,5...)
    plt.subplot(rank,3,n)
    plt.bar(range(len(neuron_fac[p-1])),neuron_fac[p-1])

    n=((p-1)*3)+2
    plt.subplot(rank,3,n)
    plt.plot(time_fac[p-1])

    n = ((p-1)*3)+3
    plt.subplot(rank,3,n)
    plt.scatter(range(len(trial_fac[p-1])),trial_fac[p-1], s=5)
# pass the figure to the custom window
plotfig_a = ScrollableWindow(fig_a)




'''
def multi_sess_tensor_4(m1,s1,a1,m2,s2,a2,m3,s3,a3,m4,s4,a4, plane_n=1, pre=1, post=1.5):
    sess1 = sessarea.SessArea(datadir, area=a1, mouse_n=m1, sess_n=s1, mouse_df=mouse_df)
    sess2 = sessarea.SessArea(datadir, area=a2, mouse_n=m2, sess_n=s2, mouse_df=mouse_df)
    sess3 = sessarea.SessArea(datadir, area=a3, mouse_n=m3, sess_n=s3, mouse_df=mouse_df)
    sess4 = sessarea.SessArea(datadir, area=a4, mouse_n=m4, sess_n=s4, mouse_df=mouse_df)

    plane1 = sess1.get_plane(plane_n)
    trial_ns = sess1.get_trials_by_criteria()
    twop_ref_fr = plane1.get_twop_fr_by_trial(trial_ns, first=True)
    frame1, data1 = plane1.get_roi_trace_array(twop_ref_fr, pre=pre,
                                            post=post, trace_type="corr")

    plane2 = sess2.get_plane(plane_n)
    trial_ns = sess2.get_trials_by_criteria()
    twop_ref_fr = plane2.get_twop_fr_by_trial(trial_ns, first=True)
    frame2, data2 = plane2.get_roi_trace_array(twop_ref_fr, pre=pre,
                                               post=post, trace_type="corr")

    plane3 = sess3.get_plane(plane_n)
    trial_ns = sess3.get_trials_by_criteria()
    twop_ref_fr = plane3.get_twop_fr_by_trial(trial_ns, first=True)
    frame3, data3 = plane1.get_roi_trace_array(twop_ref_fr, pre=pre,
                                               post=post, trace_type="corr")

    plane4 = sess4.get_plane(plane_n)
    trial_ns = sess4.get_trials_by_criteria()
    twop_ref_fr = plane4.get_twop_fr_by_trial(trial_ns, first=True)
    frame4, data4 = plane4.get_roi_trace_array(twop_ref_fr, pre=pre,
                                               post=post, trace_type="corr")

    multi_sess_data = np.concatenate((data1,data2,data3,data4),axis=1)
    multi_sess_tensordata = tf.convert_to_tensor(multi_sess_data)
    return(multi_sess_tensordata)


#multi_sess_tensor(1,3,2,1,7,2,1,11,2,1,12,2)
#tca1 = tt.cp_als(multi_sess_tensordata, rank=rank, random_state=1, max_iter=500, verbose=False)


#ax.set_xticks([50, 104, 157])           Sets the ticks at trial_ns 50, 104 and 157 to mark different trial blocks
#ax.set_xticklabels(['Day 1', 'Day 2', 'Day 3'])
'''