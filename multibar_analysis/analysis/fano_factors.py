import sys
import os
import copy
import sklearn
import gcmi
import warnings
import matplotlib
import warnings

import tca_functions as tcaf
import numpy as np
import tensorflow as tf
import tensortools as tt
import pandas as pd
import seaborn as sns
import statsmodels.api as sm
import scipy.stats as stats
import scikit_posthocs as sp


sys.path.extend(['.', '../'])
matplotlib.use('Qt5Agg')
from analysis import sessarea
from mult_util import mult_gen_util
from util import math_util
from scipy.stats import zscore
from collections import Counter
from statsmodels.formula.api import ols
from matplotlib import pyplot as plt
from pingouin import pairwise_tukey
from PyQt5 import QtWidgets
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar


datadir = os.path.join(
    'C:/University_Work/colmdowling/multibar_analysis/data')
mouse_df = 'C:/University_Work/colmdowling/multibar_analysis/mouse_df_new.csv'


"""
# We can use the information from our mouse data to create a SessArea object
mouse_n = 2
sess_n = 3
area = 1

# We can define our plane number, and the timeframe (pre/post stimulus) that we are interested in.
plane_n = 1
pre = 1
post = 1.5


# This defines our sessionarea object and assaigns it to the variable "sess".
sess = sessarea.SessArea(
    datadir, area, mouse_n=mouse_n, sess_n=sess_n, mouse_df=mouse_df)

# This will print basic info for each plane object contained within sess.
# For each value (plane) of 1 and 2
print(f'M = {mouse_n},     S = {sess_n},     A = {area}')
for p in [1, 2]:
    plane = sess.get_plane(p)
    layer = plane.layer
    level = plane.level
    nrois = plane.nrois
    print(f'Plane {p}: {level} {layer} (ROIs = {nrois})')
print(f'Proportion correct: {sess.prop_corr}')

plane = sess.get_plane(plane_n)

roi_list = []
roi_list_by_stim = []
plane.set_resp_rois(pre=1, post=1.5, p_thr=0.01)
for p, pos in enumerate(sess.stim_pos):
    print(f'\nStim position: {pos}')
    resp_roi_ns = np.where(plane.resp_rois[:, p])[0]
    signif_sign = plane.resp_roi_sign[resp_roi_ns, p]
    signif_sign_str = ['-' if sign == -1 else '+'
                       for sign in signif_sign]
    roi_strs = [f'{roi}{sign}' for roi, sign in zip(resp_roi_ns, signif_sign_str)]
    print('Responsive ROIs: {}'.format(', '.join(roi_strs)))
    roi_list.extend(resp_roi_ns)
    roi_list_by_stim.append(resp_roi_ns)
roi_list.sort()

go = roi_list_by_stim[0]
nogo = roi_list_by_stim[1]

#returns the proportion of neurons which were stim-responsive.
prop_stim_resp_roi = len(roi_list)/plane.nrois
print(f"Proportion of ROIs which responded to stimulus: {prop_stim_resp_roi}")


#calculates fano factor for the 1.5 post period only
fano_go = plane.get_roi_fano(pre=0,post=1.5, rois=go, trial_type="go")
fano_nogo = plane.get_roi_fano(pre=0,post=1.5,rois=nogo, trial_type="nogo")

fig = plt.figure()
fig.suptitle("Fano factors for stimulus-responsive neurons", fontsize=14)
plt.subplot(1,2,1)
plt.title("GO ROIs on GO trials")
plt.bar(range(len(fano_go)), fano_go)
plt.xlabel("GO responsive ROIs")
plt.xticks([i for i in range(0,len(roi_list_by_stim[0]))],roi_list_by_stim[0])
plt.ylabel("Fano factor")
plt.subplot(1,2,2)
plt.title("NOGO ROIs on NOGO trials")
plt.bar(range(len(fano_nogo)), fano_nogo)
plt.xlabel("NO-GO responsive ROIs")
plt.xticks([i for i in range(0,len(roi_list_by_stim[1]))],roi_list_by_stim[1])
plt.ylabel("Fano factor")
plt.show()
"""



def get_roi_fanos(mouse_n, sess_n, area_n, plane_n, pre=1, post=1.5):
    '''Returns a list of fano factors for each stimulus responsive ROI in a given plane:
            - mouse_n           : Mouse number
            - plane_n           : Plane of interest
            - sess_n            : Session number
            - area_n            : Area number
            - pre               : the prestimulus time period to use (seconds)
            - post              : the post-stimulus time period to use (seconds)
    Output:
            - rois              : a _______ of ROIs that are responsive to either the go or no-go position
            - fanos             : a _______ of the fano factors for each responsive ROI'''

    sess = sessarea.SessArea(
        datadir, area=area_n, mouse_n=mouse_n, sess_n=sess_n, mouse_df=mouse_df)
    plane = sess.get_plane(plane_n)

    roi_list = []
    roi_list_by_stim = []
    plane.set_resp_rois(pre=pre, post=post, p_thr=0.01)
    for p, pos in enumerate(sess.stim_pos):
        resp_roi_ns = np.where(plane.resp_rois[:, p])[0]
        roi_list.extend(resp_roi_ns)
        roi_list_by_stim.append(resp_roi_ns)
    roi_list.sort()

    go = roi_list_by_stim[0]
    nogo = roi_list_by_stim[1]

    #calculates fano factor for the post period only
    fano_go = plane.get_roi_fano(pre=0,post=post, rois=go, trial_type="go")
    fano_nogo = plane.get_roi_fano(pre=0,post=post,rois=nogo, trial_type="nogo")

    fano = []
    fano.extend(fano_go)
    fano.extend(fano_nogo)
    rois = np.append(go,nogo)

    return rois, fano



def get_prop_stim_resp_roi(mouse_n, sess_n, area_n, plane_n, pre=1, post=1.5):
    '''Calculates the percentage of ROIs which are stimulus responsive for a given plane:
    Required args:
            - mouse_n           : Mouse number
            - plane_n           : Plane of interest
            - sess_n            : Session number
            - area_n            : Area number
            - pre               : the prestimulus time period to use (seconds)
            - post              : the post-stimulus time period to use (seconds)
    Output:
            - prop_stim_resp_roi: n stimulus responsive ROIs / total n ROIs'''

    sess = sessarea.SessArea(
        datadir, area=area_n, mouse_n=mouse_n, sess_n=sess_n, mouse_df=mouse_df)
    plane = sess.get_plane(plane_n)

    roi_list = []
    roi_list_by_stim = []
    plane.set_resp_rois(pre=pre, post=post, p_thr=0.01)
    for p, pos in enumerate(sess.stim_pos):
#        print(f'\nStim position: {pos}')
        resp_roi_ns = np.where(plane.resp_rois[:, p])[0]
        signif_sign = plane.resp_roi_sign[resp_roi_ns, p]
        signif_sign_str = ['-' if sign == -1 else '+'
                           for sign in signif_sign]
        roi_strs = [f'{roi}{sign}' for roi, sign in zip(resp_roi_ns, signif_sign_str)]
#       print('Responsive ROIs: {}'.format(', '.join(roi_strs)))
        roi_list.extend(resp_roi_ns)
        roi_list_by_stim.append(resp_roi_ns)
    roi_list = list(dict.fromkeys(roi_list))

    # returns the proportion of neurons which were stim-responsive.
    prop_stim_resp_roi = len(roi_list) / plane.nrois
    return prop_stim_resp_roi


def build_prop_responsive_roi_df(datadir, mouse_df):
    '''Creates a df with the proportion of stimulus-responsive rois for each plane. Saves it to the directory:
        Required args:
            - datadir           : The specified path for mouse datafiles (also the output path)
            - mouse_df          : The mouse csv file'''
    # Let's make a pandas df with the proprition of responsive ROIs for each plane in each mouse, session and area.
    mouse_n = [i for i in range (1,9)]
    planes = [i for i in range (1,3)]
    sessions = [i for i in range(1,30)]
    areas = [i for i in range(1,4)]

    resp_roi_scores = pd.DataFrame(data= [], columns=["mouse", "session", "area", "plane", "prop_resp",
                                                      "layer", "level", "correct", "task", "include"])
    for p in mouse_n:
        for q in sessions:
            for r in areas:
                for s in planes:
                    try:
                        print(f"M:{p}, S:{q}, A:{r}, P:{s}")
                        prop_resp = get_prop_stim_resp_roi(p, q, r, s, pre=1, post=1.5)
                        sess = sessarea.SessArea(datadir, area=r, mouse_n=p, sess_n=q, mouse_df=mouse_df)
                        plane = sess.get_plane(s)
                        task = sess.task  # Whether the task was pd2, passive stim or switched go pos.
                        layer = plane.layer  # Layer of interest
                        level = plane.level  # Level within the layer (shallow, mid, deep)
                        include = bool(plane.incl)  # Reccomended for inclusion by Mari
                        correct = sess.prop_corr  # Percentage of correct trials
                        new_row = pd.Series({"mouse": p, "session": q, "area": r, "plane": s, "prop_resp": prop_resp,
                                                "layer": layer, "level": level, "correct": correct, "task": task,
                                                "include": include}, name = "x")
                        resp_roi_scores = resp_roi_scores.append(new_row, ignore_index=False)
                    except:
                        continue
    resp_roi_scores.to_csv(os.path.join(datadir, 'prop_resp_roi.csv'))



def build_fano_df(datadir, mouse_df):
    '''Creates a df with the fano factor for each mouse, session, area, plane and ROI. Saves it to the directory:
        Required args:
            - datadir           : The specified path for mouse datafiles (also the output path)
            - mouse_df          : The mouse csv file'''
    # Let's make a pandas df with the fano factor for each mouse, session, area, plane and ROI.
    mouse_n = [i for i in range (1,10)]
    planes = [i for i in range (1,3)]
    sessions = [i for i in range(1,30)]
    areas = [i for i in range(1,4)]

    allscores = pd.DataFrame(data= [], columns=["mouse", "session", "area", "plane", "ROI", "fano"])
    for p in mouse_n:
        for q in sessions:
            for r in areas:
                for s in planes:
                    try:
                        roi ,fano = get_roi_fanos(p, q, r, s)
                        sess = sessarea.SessArea(
                            datadir, area=r, mouse_n=p, sess_n=q, mouse_df=mouse_df)
                        plane = sess.get_plane(s)
                        task = sess.task                       # Whether the task was pd2, passive stim or switched go pos.
                        layer = plane.layer                    # Layer of interest
                        level = plane.level                    # Level within the layer (shallow, mid, deep)
                        include = bool(plane.incl)             # Reccomended for inclusion by Mari
                        correct = sess.prop_corr               # Percentage of correct trials
                        new_row = pd.DataFrame({"mouse": p, "session": q, "area": r, "plane": s, "ROI": roi, "fano": fano,
                                                "layer": layer, "level": level, "correct": correct, "task": task,
                                                "include": include})
                        allscores = pd.concat([allscores, new_row], axis=0, ignore_index=True)
                    except:
                        continue
    allscores.to_csv(os.path.join(datadir, 'fano_factors.csv'))



def gen_layer_fano_graph(data, layer, level):
    """plots a grpah for the specified layer with fano on Y axis and learning stage on x axis. different lines represent mice
        Required args:
            - data             :
            - layer            : The mouse csv file
            - level            : The mouse csv file"""
    layer_oi = data.loc[(data["level"] == level) & (data["layer"] == layer)]
    dataframe = pd.DataFrame(data= [], columns=["mouse", "tier_1", "tier_2", "tier_3", "tier_4", "switch"])
    for mouse in [i for i in range(1, max(layer_oi["mouse"]) + 1)]:
        mousescores = layer_oi.loc[(data["mouse"] == mouse) & (data["task"] == "pd_2")]
        tier_1 = mousescores.loc[data["learn_cat"] == "tier_1"]["fano"].mean()
        tier_2 = mousescores.loc[data["learn_cat"] == "tier_2"]["fano"].mean()
        tier_3 = mousescores.loc[data["learn_cat"] == "tier_3"]["fano"].mean()
        tier_4 = mousescores.loc[data["learn_cat"] == "tier_4"]["fano"].mean()
        switch = data.loc[(data["mouse"] == mouse) & (data["task"] == "switch")]["fano"].mean()
        newrow = pd.DataFrame({"mouse": [mouse],
                                "tier_1": [tier_1],
                                "tier_2": [tier_2],
                                "tier_3": [tier_3],
                                "tier_4": [tier_4],
                                "switch": [switch]
                                })
        dataframe = pd.concat([dataframe, newrow], axis=0, ignore_index=True)
    fig = plt.figure()
    plt.xticks(np.arange(0, 6, 1.0), ("Naieve", "2", "3", "Expert", "Switch"))
    plt.plot(dataframe.iloc[0, 1:6], marker='o')
    plt.plot(dataframe.iloc[1, 1:6], marker='o')
    plt.plot(dataframe.iloc[2, 1:6], marker='o')
    plt.plot(dataframe.iloc[3, 1:6], marker='o')
    plt.plot(dataframe.iloc[4, 1:6], marker='o')
    plt.plot(dataframe.iloc[5, 1:6], marker='o')
    plt.plot(dataframe.iloc[6, 1:6], marker='o')
    plt.plot(dataframe.iloc[7, 1:6], marker='o')
    plt.title(f"Mean fano factors in stimulus-responsive ROIs ({layer} {level})")

    return fig, dataframe






# Let's import the csv so it doens't have to run the code chunck each time
allscores = pd.read_csv(r'C:/University_Work/colmdowling/multibar_analysis/data/fano_factors.csv')
allscores = allscores.loc[allscores["include"] == True]

# We're going to make a learning stage column. First we will define our different learning categories.
conditions = [
    (allscores["correct"] >= .75),
    (allscores["correct"] >= .65) & (allscores["correct"] < .75),
    (allscores["correct"] >= .55) & (allscores["correct"] < .65),
    (allscores["correct"] < .55)
    ]

# Now we create a list of the values we want to assign for each condition and add the new column.
category = ['tier_4', 'tier_3', 'tier_2', 'tier_1']
allscores['learn_cat'] = np.select(conditions, category)
allscores['depth'] = allscores["layer"] + allscores["level"]

allscores = allscores.loc[allscores["layer"] != "L4"]                        # Remove L4 recordings
#dummy_depth = pd.get_dummies(allscores["depth"]).iloc[:,1:]                 # dummy code depth
#dummy_learn_cat = pd.get_dummies(allscores["learn_cat"]).iloc[:,1:]         # dummy code learning category
#allscores = pd.concat([allscores, dummy_depth, dummy_learn_cat], axis = 1)  # add the new dummy cols to the df

allscores = allscores.loc[allscores["task"] == "pd_2"]

allscores["z"] = np.abs(zscore(allscores["fano"]))
#only keep rows in dataframe with all z-scores less than absolute value of 3
allscores = allscores.loc[allscores["z"]<3]




# Save our data
#allscores.to_csv(os.path.join(datadir, 'allscores_anova.csv'))

# Plot some violins
fig, ax = plt.subplots(1,1)
ax.set_yscale('symlog')
sns.violinplot("learn_cat","fano",data=allscores, order =("tier_1","tier_2","tier_3","tier_4"))
ax.set_xticklabels(["<55%","55-65%","65-75%",">75%"])
ax.set_xlabel("Learning category (% correct responses)")
ax.set_ylabel("SNR (Fano factor)")
plt.show()

fig, ax = plt.subplots(1,1)
ax.set_yscale('symlog')
sns.violinplot("depth","fano",data=allscores, order =("L2mid","L3hypershallow","L3shallow","L3deep"))
ax.set_xticklabels(["L2","Hypershallow L3","Shallow L3","Deep L3"])
ax.set_xlabel("Cortical depth")
ax.set_ylabel("SNR (Fano factor)")
plt.show()

fig, ax = plt.subplots(1,1)
sns.violinplot("mouse","fano",data=allscores)
ax.set_yscale('symlog')
ax.set_xlabel("Mouse")
ax.set_ylabel("SNR (Fano factor)")
plt.show()

# Plot some means and medians for each mouse over learning


# Fit a lm
lm = ols('fano ~ C(learn_cat)*C(depth)*C(mouse)', data=allscores).fit()
fig=plt.figure()
plt.scatter(x=range(1, (len(lm.resid) +1)), y=sorted(lm.resid), s=1)
plt.show()

# Shapiro-Wilk - Test for equal distribution of residuals
w, pvalue = stats.shapiro(lm.resid)
print(f"Shapiro-Wilk: W={w}, p={pvalue}")

# Levene test - Test for homogeneity of variance (non-normal)
levene_depth = stats.levene(allscores.loc[allscores["depth"]=="L3hypershallow"]["fano"],
                            allscores.loc[allscores["depth"]=="L2mid"]["fano"],
                            allscores.loc[allscores["depth"]=="L3deep"]["fano"],
                            allscores.loc[allscores["depth"]=="L3shallow"]["fano"])
levene_learn = stats.levene(allscores.loc[allscores["learn_cat"]=="tier_1"]["fano"],
                            allscores.loc[allscores["learn_cat"]=="tier_2"]["fano"],
                            allscores.loc[allscores["learn_cat"]=="tier_3"]["fano"],
                            allscores.loc[allscores["learn_cat"]=="tier_4"]["fano"])
levene_mouse = stats.levene(allscores.loc[allscores["mouse"]==1]["fano"],
                            allscores.loc[allscores["mouse"]==2]["fano"],
                            allscores.loc[allscores["mouse"]==3]["fano"],
                            allscores.loc[allscores["mouse"]==4]["fano"],
                            allscores.loc[allscores["mouse"]==5]["fano"],
                            allscores.loc[allscores["mouse"]==6]["fano"],
                            allscores.loc[allscores["mouse"]==7]["fano"],
                            allscores.loc[allscores["mouse"]==8]["fano"])
print(f"Levene (depth): {levene_depth}")
print(f"Levene (learn): {levene_learn}")
print(f"Levene (mouse): {levene_mouse}")

# ANOVA
aov = sm.stats.anova_lm(lm, typ=2)
print(aov)

# Kruskall-Wallis (non-parametric ANOVA)
kruskall_depth = stats.kruskal(allscores.loc[allscores["depth"]=="L3hypershallow"]["fano"],
                            allscores.loc[allscores["depth"]=="L2mid"]["fano"],
                            allscores.loc[allscores["depth"]=="L3deep"]["fano"],
                            allscores.loc[allscores["depth"]=="L3shallow"]["fano"])
kruskall_learn = stats.kruskal(allscores.loc[allscores["learn_cat"]=="tier_1"]["fano"],
                            allscores.loc[allscores["learn_cat"]=="tier_2"]["fano"],
                            allscores.loc[allscores["learn_cat"]=="tier_3"]["fano"],
                            allscores.loc[allscores["learn_cat"]=="tier_4"]["fano"])
kruskall_mouse = stats.kruskal(allscores.loc[allscores["mouse"]==1]["fano"],
                            allscores.loc[allscores["mouse"]==2]["fano"],
                            allscores.loc[allscores["mouse"]==3]["fano"],
                            allscores.loc[allscores["mouse"]==4]["fano"],
                            allscores.loc[allscores["mouse"]==5]["fano"],
                            allscores.loc[allscores["mouse"]==6]["fano"],
                            allscores.loc[allscores["mouse"]==7]["fano"],
                            allscores.loc[allscores["mouse"]==8]["fano"])
print(f"Kruskall (depth)")
print(kruskall_depth)
print(f"Kruskall (learn)")
print(kruskall_learn)
print(f"Kruskall (mouse)")
print(kruskall_mouse)


# Post-hoc Conover
cono_depth = sp.posthoc_conover(allscores, val_col='fano', group_col='depth', p_adjust = 'holm')
cono_learn = sp.posthoc_conover(allscores, val_col='fano', group_col='learn_cat', p_adjust = 'holm')
cono_mouse = sp.posthoc_conover(allscores, val_col='fano', group_col='mouse', p_adjust = 'holm')
print("--POST-HOC CONOVER--")
print(round(cono_depth,3))
print(round(cono_learn, 3))
print(round(cono_mouse, 3))

# Post-hoc Tukeys
tukey_learn = pairwise_tukey(data=allscores, dv='fano', between='learn_cat')
tukey_depth = pairwise_tukey(data=allscores, dv='fano', between='depth')
tukey_mouse = pairwise_tukey(data=allscores, dv='fano', between='mouse')
print(tukey_learn)
print(tukey_depth)
print(tukey_mouse)




a, b = gen_layer_fano_graph(allscores, "L2", "mid")
gen_layer_fano_graph(allscores, "L3", "hypershallow")
gen_layer_fano_graph(allscores, "L3", "shallow")
gen_layer_fano_graph(allscores, "L3", "deep")


#        fano_mean = mousescores.groupby("learn_cat")["fano"].mean()
"""
a= gen_layer_fano_graph()
a.fillna(0)
print(a)
b = a.iloc[1,1:6]
fig = plt.figure()
plt.xticks(np.arange(0, 6, 1.0), ("Naieve", "2", "3", "Expert", "Switch"))
#sns.lineplot(data = b)
#sns.lineplot(y = b, x = ["tier_1", "tier_2", "tier_3", "tier_4", "switch"])
#plt.scatter(x = ["tier_1", "tier_2", "tier_3", "tier_4", "switch"], y = b)
plt.plot(a.iloc[0,1:6], linestyle='-', marker='o')
plt.plot(a.iloc[1,1:6], linestyle='-', marker='o')
plt.plot(a.iloc[2,1:6], linestyle='-', marker='o')
plt.plot(a.iloc[3,1:6], linestyle='-', marker='o')
plt.plot(a.iloc[4,1:6], linestyle='-', marker='o')
plt.plot(a.iloc[5,1:6], linestyle='-', marker='o')
plt.plot(a.iloc[6,1:6], linestyle='-', marker='o')
plt.plot(a.iloc[7,1:6], linestyle='-', marker='o')
plt.show()
"""



#        tier_1 = fano_mean.loc[fano_mean["count"]=="tier_1"]
#        tier_2 = fano_mean.loc[fano_mean["count"]=="tier_2"]
#        tier_3 = fano_mean.loc[fano_mean["count"]=="tier_3"]
#        tier_4 = fano_mean.loc[fano_mean["count"]=="tier_4"]

'''
for mouse in [i for i in range(1,max(allscores["mouse"])+1)]:
    mousescores = allscores.loc[(allscores["mouse"] == mouse) & (allscores["task"] == "pd_2")]

    midL2 = mousescores.loc[(mousescores["level"] == "mid") & (mousescores["layer"] == "L2")]
    hypershallowL3 = mousescores.loc[(mousescores["level"] == "hypershallow") & (mousescores["layer"] == "L3")]
    shallowL3 = mousescores.loc[(mousescores["level"] == "shallow") & (mousescores["layer"] == "L3")]
    deepL3 = mousescores.loc[(mousescores["level"] == "deep") & (mousescores["layer"] == "L3")]

    L2_means = []
    L3HS_means = []
    L3S_means = []
    L3D_means = []
#    for session in [i for i in range(1,max(mousescores["session"])+1)]:
#        for area in [1,2,3]:
#            deepL2sess = deepL2.loc[(deepL2["session"] == session) & (deepL2["area"] == area)]
#            deepL3sess = deepL3.loc[(deepL3["session"] == session) & (deepL3["area"] == area)]       # FEFAEFA
#            deepL2mean = deepL2sess["fano"].describe()[1]
#            deepL3mean = deepL3sess["fano"].describe()[1]
    for category in [1:5]:
        deepL3sess = deepL3.loc[deepL3["learn_cat"] == 1]

        L2means.append(deepL2mean)
        L3means.append(deepL3mean)

    fig = plt.figure()
    plt.subplot(2,2,1)
    plt.scatter(x=range(1,len(L2means)+1),y=L2means)
    plt.subplot(2,2,2)
    plt.scatter(x=range(1,len(L3means)+1),y=L3means)
plt.show()


#mean fano factor for stimulus-responsive ROIs per session
#each mouse has different graph. lines for each layer fano and one for performance. x axis = session

pd2_scores = allscores.loc[allscores["task"] == "pd_2"]
beginner_passstim_scores = allscores.loc[(allscores["task"] == "sens_stim") & (allscores["session"] == 1)]
expert_passtim_scores = allscores.loc[(allscores["task"] == "sens_stim") & (allscores["session"] != 1)]
switch_scores = allscores.loc[allscores["task"] == "switch"]

# Compare across layers (ANOVA)
expert = pd2_scores.loc[pd2_scores["correct"] > .75]
pro = pd2_scores.loc[(pd2_scores["correct"] > .65) & (pd2_scores["correct"] < .75)]
novice = pd2_scores.loc[(pd2_scores["correct"] > .55) & (pd2_scores["correct"] < .65)]
beginner = pd2_scores.loc[(pd2_scores["correct"] > .0) & (pd2_scores["correct"] < .55)]

expert_stats = expert["fano"].describe()
pro_stats = pro["fano"].describe()
novice_stats = novice["fano"].describe()
beginner_stats = beginner["fano"].describe()
beginner_passstim_stats = beginner_passstim_scores["fano"].describe()
expert_passtim_stats = expert_passtim_scores["fano"].describe()
switch_stats = switch_scores["fano"].describe()

print(f"Passive stim (beginner): {beginner_passstim_stats[1]}, Passive stim (expert): {expert_passtim_stats[1]}, Switch: {switch_stats[1]}, Expert: {expert_stats[1]}, Pro: {pro_stats[1]}, Novice: {novice_stats[1]}, Beginner: {beginner_stats[1]}")

deepL2 = allscores.loc[(allscores["level"] == "deep") & (allscores["layer"] == "L2")]
deepL3 = allscores.loc[(allscores["level"] == "deep") & (allscores["layer"] == "L3")]

deepL2_stats = deepL2["fano"].describe()
deepL3_stats = deepL3["fano"].describe()

print(f"Deep L2: {deepL2_stats[1]}, Deep L3: {deepL3_stats[1]}")



# Learning stage while controlling for layer

'''