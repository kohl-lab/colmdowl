# Import:
from mult_util import mult_mat_util

# Running multi_mat_util functions to check if applicable to rscassoc

# Define datadir, mouse_id and filename
datadir = "C:/University_Work/multibar_analysis/data"
mouse_id = "CTBD7.2e"
filename = datadir+mouse_id+".mat"

#loads the mat file
mat_data = mult_mat_util.load_mat_file(filename)

# does something (ask ana)
mult_mat_util.get_hdf5group_keys(mat_data)

# prints .mat keys (not values, only keys)
mult_mat_util.print_file_content(mat_data)

# saves as pickle
mult_mat_util.save_zipped_pickle(filename, 'new')

# formats date to match the .mat structure
date = '20190202'
print(mult_mat_util.make_date_key(date))

# formats area to match the .mat structure
area = 1
print(mult_mat_util.make_area_key(area))

# formats plane to match the .mat structure
plane = 1
print(mult_mat_util.make_plane_key(plane))

# gets mat structure
# mult_mat_util.get_mat_str()

# gets all the date keys from the .mat file
print(mult_mat_util.get_date_keys(mat_data))

# gets areas corresponding to the date from the .mat file
print(mult_mat_util.get_area_keys(mat_data,'date_2019_01_30'))

# gets plane corresponding to the date and area
print(mult_mat_util.get_plane_keys(mat_data,'date_2019_01_30', 'area2'))

# gets sess_beh corresponding to date and area
print(mult_mat_util.get_sess_beh_keys(mat_data,'date_2019_01_30', 'area1'))

# gets planes corresponding to sess_bah, date and area
print(mult_mat_util.get_planes_sess_beh_keys(mat_data, 'date_2019_01_30', 'area1'))

# gets raw florescence data # todo: what are these data points?
print(mult_mat_util.get_fluo_raw(mat_data,'date_2019_01_30', 'area1','plane1'))

# corrected raw florescence data
print(mult_mat_util.get_fluo_corr(mat_data,'date_2019_01_30', 'area1','plane1'))

# gets raw neuropil todo: what is neruopil?
print(mult_mat_util.get_neuropil_raw(mat_data,'date_2019_01_30', 'area1','plane1'))

# gets trial by trial florescence data
print(mult_mat_util.get_fluo_trialbytrial(mat_data,'date_2019_01_30', 'area1','plane1'))

# gets number of rois identified
print(mult_mat_util.get_nrois(mat_data,'date_2019_01_30', 'area1','plane1'))

# gets number of corresponding trials and rois in the given date, area and plane
print(mult_mat_util.get_info(mat_data,'date_2019_01_30', 'area1','plane1'))

# gets number of trials
print(mult_mat_util.get_ntrials(mat_data,'date_2019_01_30', 'area1'))

# todo: task does not exist? 
print(mult_mat_util.get_task(mat_data,'date_2019_02_01', 'area1'))
